<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class AuthenticationController extends Controller
{

    /**
     * @OA\Post(
     *     path="/plti_smart_data/dataapi/public/api/auth/register",
     *     tags={"Authentication"},
     *     summary="Register user baru",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 example={"email": "kurnianto@gmail.com", "name": "Achmad Kurnianto", "password": "secret123"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function register(Request $request)
    {
        $email = $request->email;
        $password = Hash::make($request->password);
        $nama = $request->name;
        $token = Str::random(32);

        DB::insert('insert into users (email, nama, password, token) values (?,?,?,?)', [$email, $nama, $password, $token]);

        return ["message" => "success"];
    }

    /**
     * @OA\Post(
     *     path="/plti_smart_data/dataapi/public/api/auth/login",
     *     tags={"Authentication"},
     *     summary="Login untuk mendapatkan token",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 example={"email": "kurnianto@gmail.com", "password": "secret123"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function login(Request $request)
    {

        $email = $request->email;
        $results = DB::select('select * from users where email = ?', [$email]);

        if (count($results) == 0) {
            return ["massage" => "email or password tidak cocok"];
        }

        if (Hash::check($request->password, $results[0]->password)) {
            return [
                "data" => ["name" => $results[0]->nama, "token" => $results[0]->token]
            ];
        } else {
            return ["massage" => "email or password tidak cocok"];
        }
    }
}
