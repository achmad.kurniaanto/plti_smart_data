<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use GuzzleHttp\Client;



class BlocksDataController extends Controller
{


    /**
     * @OA\Get(
     *     path="/plti_smart_data/dataapi/public/api/blockchain/data",
     *     tags={"Data Blockchain"},
     *     summary="Return list of type data",
     *     description="An API to get list of type data",
     *     security={{ "authentication": {} }},
     *     @OA\Response(
     *         response="default",
     *         description="List of data types"
     *     ),
     * )
     */
    public function index()
    {
        return "Masuk";
    }


    /**
     * @OA\Get(
     *     path="/plti_smart_data/dataapi/public/api/blockchain/data/title",
     *     tags={"Data Blockchain"},
     *     summary="Return judul data",
     *     description="API untuk mendapatkan judul data",
     *     security={{ "authentication": {} }},
     *     @OA\Parameter(
     *          name="search",
     *          description="Judul data",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="List judul data"
     *     ),
     * )
     */
    public function title(Request $request)
    {
        $query = "%" . $request->search . "%";
        $results = DB::select('select * from data_title where id in (select id_title from blocks) and (short_title_id like ? OR long_title_id like ?) limit 100', array($query, $query));
        return $results;
    }

    /**
     * @OA\Get(
     *     path="/plti_smart_data/dataapi/public/api/blockchain/data/title/detail/{id_title}",
     *     tags={"Data Blockchain"},
     *     summary="Return judul data",
     *     description="API untuk mendapatkan judul data",
     *     security={{ "authentication": {} }},
     *     @OA\Parameter(
     *          name="id_title",
     *          description="id title",
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Detail judul data"
     *     ),
     * )
     */
    public function titledetail($id)
    {
        $results = DB::table('data')
            ->select("data.*", "var.name_id as var_name", "dtp.name_id as period_name", "du.name_id as unit_name")
            // ->join('data', 'data_title.id', '=', 'data.id_data_title')
            ->join('data_variable as var', 'data.id_variable', '=', 'var.id')
            ->join('data_time_period as dtp', 'data.id_time_period', '=', 'dtp.id')
            ->join('data_unit as du', 'data.id_unit', '=', 'du.id')
            ->where('data.id_data_title', '=', $id)
            ->whereRaw("data.id in (SELECT id_data from blocks)")
            ->get();

        return $results;
    }

    /**
     * @OA\Get(
     *     path="/plti_smart_data/dataapi/public/api/blockchain/data/series/{id_data}",
     *     tags={"Data Blockchain"},
     *     summary="Return data",
     *     description="API untuk mendapatkan data",
     *     security={{ "authentication": {} }},
     *     @OA\Parameter(
     *          name="id_data",
     *          description="Id data",
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="List series data"
     *     ),
     * )
     */
    public function data_series($id)
    {
        $results = DB::table('data_series')
            ->select("data_series.*", "dtpv.name_id as periode_value")
            // ->join('data', 'data_title.id', '=', 'data.id_data_title')
            ->join('data_time_period_value as dtpv', 'data_series.id_time_period_value', '=', 'dtpv.id')
            ->where('data_series.id_data', '=', $id)
            ->whereRaw("data_series.id in (SELECT id_data_series from blocks)")
            ->get();

        return $results;
    }


    /**
     * @OA\Get(
     *     path="/plti_smart_data/dataapi/public/api/blockchain/data/cell/{id_series}",
     *     tags={"Data Blockchain"},
     *     summary="Return data",
     *     description="API untuk mendapatkan data cell",
     *     security={{ "authentication": {} }},
     *     @OA\Parameter(
     *          name="id_series",
     *          description="Id data",
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="List data cell "
     *     ),
     * )
     */
    public function data_cell($id_series)
    {
        $id_data = "";
        // $id_series = "";
        // $data_series = DB::select('select * from data_series where id_data=?', array($id_series));
        // $res_series = [];
        // foreach ($data_series as $s) {
        //     $data_cells =  DB::table('data_cell')
        //         ->where('data_cell.id_data_series', '=', $s->id)
        //         ->get();

        //     array_push($res_series, $data_cells);
        // }

        $data_cells =  DB::table('blocks')
            ->where('blocks.id_data_series', '=', $id_series)
            ->get();
        if (count($data_cells) > 0) {
            return json_decode($data_cells[0]->data);
        } else
            return [];
    }

    /**
     * @OA\Post(
     *     path="/plti_smart_data/dataapi/public/api/blockchain/data",
     *     tags={"Data Blockchain"},
     *     summary="Return data",
     *     description="API untuk memasukkan data blockchain dengan format data_cell json array dengan key row_key dan data_value",
     *     security={{ "authentication": {} }},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={"id_title":"1","id_data_series":"1","id_time_period_value":"2","id_data":"1","id_series":"1","id_issuer":"23","data_cell":{{"row_key":11,"data_value":"1"},{"row_key":11,"data_value":"2"}}}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="List data cell "
     *     ),
     * )
     */
    public function addblockdata(Request $request)
    {
        $client = new Client();
        $host = request()->getHost();
        $response = $client->request('POST', 'http://' . $host . ':3001/mine', ['json' => ['data' => request()->json()->all()], 'verify' => false]);

        return json_decode($response->getBody());
    }
}
