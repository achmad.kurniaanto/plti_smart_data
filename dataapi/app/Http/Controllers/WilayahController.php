<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class WilayahController extends Controller
{


    /**
     * @OA\Get(
     *     path="/api/master/wilayah/provinsi",
     *     tags={"Master Wilayah"},
     *     summary="Return list provinsi",
     *     description="API untuk mendapatkan daftar provinsi",
     *     security={{ "authentication": {} }},
     *     @OA\Response(
     *         response="default",
     *         description="List master provinsi"
     *     ),
     * )
     */
    public function provinsi()
    {
        $results = DB::select('select * from master_provinsi');

        return $results;
    }

    /**
     * @OA\Get(
     *     path="/api/master/wilayah/kabkot/{idparent}",
     *     tags={"Master Wilayah"},
     *     summary="Return list kabupaten/kota",
     *     description="API untuk mendapatkan daftar kabupaten/kota",
     *     security={{ "authentication": {} }},
     *     @OA\Parameter(
     *         description="Id Parent wilayah (Id provinsi)",
     *         in="path",
     *         name="idparent",
     *         @OA\Schema(type="int"),
     *         @OA\Examples(example="int", value="11", summary="Provinsi Aceh"),
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="List master kabupaten kota"
     *     ),
     * )
     */
    public function kabkot($idparent)
    {
        $results = DB::select('select * from master_kabupaten_kota where kode_parent = ?', array($idparent));

        return $results;
    }
}
