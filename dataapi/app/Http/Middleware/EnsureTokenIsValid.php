<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class EnsureTokenIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $token = $request->bearerToken();
        $results = DB::select('select * from users where token = ?', [$token]);
        if (count($results) <= 0) {
            abort(response()->json(
                [
                    'api_status' => '401',
                    'message' => 'authenticated',
                ],
                401
            ));
        }

        return $next($request);
    }
}
