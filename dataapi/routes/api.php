<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\DataController;
use App\Http\Controllers\BlocksDataController;
use App\Http\Controllers\WilayahController;



Route::post('/auth/register', [AuthenticationController::class, 'register']);
Route::post('/auth/login', [AuthenticationController::class, 'login']);


Route::middleware(['auth.token'])->group(function () {

    Route::get('/data', [DataController::class, 'index']);
    Route::get('/data/series/{id}', [DataController::class, 'data_series']);
    Route::get('/data/cell/{id_series}', [DataController::class, 'data_cell']);
    Route::get('/data/title', [DataController::class, 'title']);
    Route::get('/data/title/detail/{id}', [DataController::class, 'titledetail']);

    Route::get('/blockchain/data', [BlocksDataController::class, 'index']);
    Route::post('/blockchain/data', [BlocksDataController::class, 'addblockdata']);
    Route::get('/blockchain/data/series/{id}', [BlocksDataController::class, 'data_series']);
    Route::get('/blockchain/data/cell/{id_series}', [BlocksDataController::class, 'data_cell']);
    Route::get('/blockchain/data/title', [BlocksDataController::class, 'title']);
    Route::get('/blockchain/data/title/detail/{id}', [BlocksDataController::class, 'titledetail']);

    Route::get('/master/wilayah/provinsi', [WilayahController::class, 'provinsi']);
    Route::get('/master/wilayah/kabkot/{idparent}', [WilayahController::class, 'kabkot']);
});
