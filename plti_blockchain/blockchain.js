const Block = require('./block');

class Blockchain {
    constructor() {
        this.chain = [Block.genesis()]
    }

    addBlock(data) {
        const hashChain = Block.hashChain(data)
        Block.getBlockByHashChain(hashChain, (b) => {
            let block;
            if (b != null) {
                console.log("NOT NULL");
                console.log(b);
                block = Block.mineBlock(b, data)
            } else {
                console.log("NULL");
                block = Block.mineBlock(this.chain[this.chain.length - 1], data)
            }
            if (block.hash != "f1r57-h45h")
                block.persistentBlock();
            // this.chain.push(block);
        });
        // return block;
    }

    getBlocks(callback) {
        Block.getBlocks((bs) => {
            callback(bs)
        });
    }

    isValidChain(chain) {
        if
            (JSON.stringify(chain[0]) !== JSON.stringify(Block.genesis())) return false;
        for
            (let i = 1; i < chain.length; i++) {
            const block = chain[i];
            const lastBlock = chain[i - 1];
            if
                (
                block.lastHash !== lastBlock.hash ||
                block.hash !== Block.blockHash(block)
            ) {
                return false;
            }
        }
        return true;
    }
}
module.exports = Blockchain;