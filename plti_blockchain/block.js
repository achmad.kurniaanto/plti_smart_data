const SHA256 = require('crypto-js/sha256');
const { DIFFICULTY, MINE_RATE } = require('./config');
const ChainUtil = require('./chain-util');
var db = require("./db_config");


class Block {
    constructor(timestamp, lastHash, hash, hashChain, nonce, difficulty, id_issuer, id_title, id_data_series, id_data, id_time_period_value, data_cell) {
        this.timestamp = timestamp;
        this.lastHash = lastHash;
        this.hash = hash;
        this.hashChain = hashChain;
        this.nonce = nonce;
        this.difficulty = difficulty || DIFFICULTY;

        this.id_title = id_title;
        this.id_data_series = id_data_series;
        this.id_data = id_data;
        this.id_time_period_value = id_time_period_value;
        this.data_cell = data_cell;
        this.id_issuer = id_issuer;

    }
    toString() {
        return `Block -
    Timestamp : ${this.timestamp}
    Last Hash : ${this.lastHash.substring(0, 10)}
    Hash : ${this.hash.substring(0, 10)}
    Nonce : ${this.nonce}
    Difficulty: ${this.difficulty}
    Data Cell: ${this.data_cell}`;

    }

    static genesis() {
        return new this('Genesis time', '-----', 'f1r57-h45h', 0, DIFFICULTY);
    }

    static mineBlock(lastBlock, data) {
        let hash, timestamp;
        const lastHash = lastBlock.hash;
        let { difficulty } = lastBlock;
        let nonce = 0;
        do {
            nonce++;
            timestamp = Date.now();
            difficulty = Block.adjustDifficulty(lastBlock, timestamp);
            hash = Block.hash(timestamp, lastHash, nonce, difficulty, data);
        }
        while (hash.substring(0, difficulty) !== '0'.repeat(difficulty));

        return new this(timestamp, lastHash, hash, Block.hashChain(data), nonce, difficulty, data.id_issuer, data.id_title, data.id_data_series, data.id_data, data.id_time_period_value, data.data_cell);
    }


    static hash(timestamp, lastHash, nonce, difficulty, data) {
        return ChainUtil.hash(`${timestamp}${lastHash}${data}${nonce}${difficulty}`);
    }

    static hashChain(data) {
        return ChainUtil.hash(`${data.id_title}${data.id_data_series}${data.id_data}`);
    }

    static blockHash(block) {
        const { timestamp, lastHash, nonce, difficulty, data } = block;
        return Block.hash(timestamp, lastHash, nonce, difficulty, data);
    }

    static adjustDifficulty(lastBlock, currentTime) {
        let { difficulty } = lastBlock;
        difficulty = lastBlock.timestamp + MINE_RATE > currentTime ?
            difficulty + 1 : difficulty - 1;
        return difficulty;
    }

    static getBlockByHashChain(hashChain, callback) {
        let sql = "SELECT * FROM blocks WHERE hash_chain = ? order by timestamp DESC LIMIT 1"
        var values = [
            hashChain
        ];
        db.query(sql, [values], (err, res) => {
            if (err) throw err;
            if (res.length == 1) {
                const r = res[0];
                callback(new Block(r.timestamp, r.lasthash, r.hash, r.hash_chain, r.nonce, r.difficulty, r.id_issuer, r.id_title, r.id_data_series, r.id_data, r.id_time_period_value, r.data))
            } else {
                callback(null);
            }
        });
    }


    static getBlocks(callback) {
        let sql = "SELECT * FROM blocks order by timestamp ASC"

        db.query(sql, [], (err, res) => {
            if (err) throw err;
            let blocks = [];
            blocks.push(Block.genesis());
            for (let i = 0; i < res.length; i++) {
                const r = res[i];
                blocks.push(new Block(r.timestamp, r.lasthash, r.hash, r.hash_chain, r.nonce, r.difficulty, r.id_issuer, r.id_title, r.id_data_series, r.id_data, r.id_time_period_value, JSON.parse(r.data)))
            }
            callback(blocks);
        });
    }

    persistentBlock() {
        let sql = "INSERT INTO blocks (hash, lasthash, hash_chain, timestamp, nonce, difficulty, id_title, id_data_series, id_data, id_time_period_value, data, id_issuer) VALUES ?";
        var values = [
            [this.hash, this.lastHash, this.hashChain, this.timestamp, this.nonce, this.difficulty, this.id_title, this.id_data_series, this.id_data, this.id_time_period_value, JSON.stringify(this.data_cell), this.id_issuer],
        ];
        db.query(sql, [values], function (err, result) {
            if (err) throw err;
            console.log("Number of records inserted: " + result.affectedRows);
            // result.json(result);
        });
    }
}
module.exports = Block;