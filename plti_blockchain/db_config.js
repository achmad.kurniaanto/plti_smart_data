var mysql = require('mysql');

var db = mysql.createConnection({
    host: "localhost",
    user: "root",
    port: 3306,
    password: "root",
    database: "plti_smart_vis"
});

module.exports = db;