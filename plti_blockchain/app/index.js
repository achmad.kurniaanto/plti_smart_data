const express = require(`express`);
const bodyParser = require(`body-parser`);
const Blockchain = require(`../blockchain`);
const P2pServer = require('../p2p-server');
const Wallet = require('../wallet');
const TransactionPool = require('../wallet/transaction-pool');
const HTTP_PORT = process.env.HTTP_PORT || 3001;
const wallet = new Wallet();
const tp = new TransactionPool();
const app = express();
const bc = new Blockchain();
const p2pServer = new P2pServer(bc, tp);
app.use(bodyParser.json());
app.get('/blocks', (req, res) => {
    bc.getBlocks(bs => {
        res.json(bs);
    });
});
app.post('/mine', (req, res) => {
    bc.addBlock(req.body.data);
    // const block = bc.addBlock(req.body.data);
    // console.log(`New block added: ${block.toString()}`);
    res.redirect('/blocks');
});
app.get('/transactions', (req, res) => {
    res.json(tp.transactions);
});
app.post('/transact', (req, res) => {
    const { recipient, amount } = req.body;
    const transaction = wallet.createTransaction(recipient, amount, tp);
    p2pServer.broadcastTransaction(transaction);
    res.redirect('/transactions');
});

var db = require("../db_config");
app.get('/test', (req, res) => {
    db.connect(function (err) {
        if (err) throw err;

        let sql = "INSERT INTO blocks (hash, lasthash, timestamp, nonce, difficulty, data) VALUES ?";
        var values = [
            ['hash', 'last_hash', 1234213213, 1, 1, "data"],
        ];
        db.query(sql, [values], function (err, result) {
            if (err) throw err;
            console.log("Number of records inserted: " + result.affectedRows);
            res.json(result);
        });

    });
});



app.listen(HTTP_PORT, () => console.log(`Listening on port: ${HTTP_PORT}`));
p2pServer.listen();