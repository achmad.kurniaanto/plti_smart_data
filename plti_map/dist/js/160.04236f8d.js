"use strict";(self["webpackChunkplti_map"]=self["webpackChunkplti_map"]||[]).push([[160],{45584:function(e,t,i){i.d(t,{D:function(){return D},b:function(){return L}});var r=i(65684),o=i(95650),n=i(57218),a=i(25714),s=i(5885),l=i(4731),c=i(99163),d=i(27125),u=i(91636),h=i(45658),f=i(82082),m=i(6502),p=i(51592),v=i(78183),g=i(52372),x=i(6665),T=i(3417),_=i(30786),b=i(40566),A=i(73393),S=i(2833),C=i(74839),O=i(3864),M=i(20105),E=i(12664),y=i(41272),w=i(78115),R=i(42727),P=i(23410),I=i(3961),N=i(21414);function L(e){const t=new I.kG,i=t.vertex.code,L=t.fragment.code;t.include(w.a,{name:"Default Material Shader",output:e.output}),t.vertex.uniforms.add("proj","mat4").add("view","mat4").add("cameraPosition","vec3").add("localOrigin","vec3");const D=e.hasModelTransformation;return D&&t.vertex.uniforms.add("model","mat4"),t.include(u.f),t.varyings.add("vpos","vec3"),t.include(E.kl,e),t.include(c.fQ,e),t.include(v.LC,e),e.output!==a.H.Color&&e.output!==a.H.Alpha||(t.include(d.O,e),t.include(l.w,{linearDepth:!1,hasModelTransformation:D}),e.normalType===d.h.Attribute&&e.offsetBackfaces&&t.include(n.w),t.include(T.Q,e),t.include(p.B,e),e.instancedColor&&t.attributes.add(N.T.INSTANCECOLOR,"vec4"),t.varyings.add("localvpos","vec3"),t.include(f.D,e),t.include(o.q,e),t.include(h.R,e),t.include(m.c,e),t.vertex.uniforms.add("externalColor","vec4"),t.varyings.add("vcolorExt","vec4"),e.multipassTerrainEnabled&&t.varyings.add("depth","float"),i.add(P.H`
      void main(void) {
        forwardNormalizedVertexColor();
        vcolorExt = externalColor;
        ${e.instancedColor?"vcolorExt *= instanceColor;":""}
        vcolorExt *= vvColor();
        vcolorExt *= getSymbolColor();
        forwardColorMixMode();

        if (vcolorExt.a < ${P.H.float(y.bf)}) {
          gl_Position = vec4(1e38, 1e38, 1e38, 1.0);
        }
        else {
          vpos = calculateVPos();
          localvpos = vpos - view[3].xyz;
          vpos = subtractOrigin(vpos);
          ${e.normalType===d.h.Attribute?P.H`
          vNormalWorld = dpNormal(vvLocalNormal(normalModel()));`:""}
          vpos = addVerticalOffset(vpos, localOrigin);
          ${e.vertexTangents?"vTangent = dpTransformVertexTangent(tangent);":""}
          gl_Position = transformPosition(proj, view, ${D?"model,":""} vpos);
          ${e.normalType===d.h.Attribute&&e.offsetBackfaces?"gl_Position = offsetBackfacingClipPosition(gl_Position, vpos, vNormalWorld, cameraPosition);":""}
        }

        ${e.multipassTerrainEnabled?"depth = (view * vec4(vpos, 1.0)).z;":""}
        forwardLinearDepth();
        forwardTextureCoordinates();
      }
    `)),e.output===a.H.Alpha&&(t.include(s.p2,e),t.include(y.sj,e),e.multipassTerrainEnabled&&(t.fragment.include(x.S),t.include(A.l,e)),t.fragment.uniforms.add("cameraPosition","vec3").add("localOrigin","vec3").add("opacity","float").add("layerOpacity","float"),e.hasColorTexture&&t.fragment.uniforms.add("tex","sampler2D"),t.fragment.include(R.y),L.add(P.H`
      void main() {
        discardBySlice(vpos);
        ${e.multipassTerrainEnabled?"terrainDepthTest(gl_FragCoord, depth);":""}
        ${e.hasColorTexture?P.H`
        vec4 texColor = texture2D(tex, vuv0);
        ${e.textureAlphaPremultiplied?"texColor.rgb /= texColor.a;":""}
        discardOrAdjustAlpha(texColor);`:P.H`vec4 texColor = vec4(1.0);`}
        ${e.attributeColor?P.H`
        float opacity_ = layerOpacity * mixExternalOpacity(vColor.a * opacity, texColor.a, vcolorExt.a, int(colorMixMode));`:P.H`
        float opacity_ = layerOpacity * mixExternalOpacity(opacity, texColor.a, vcolorExt.a, int(colorMixMode));
        `}
        gl_FragColor = vec4(opacity_);
      }
    `)),e.output===a.H.Color&&(t.include(s.p2,e),t.include(b.X,e),t.include(_.K,e),t.include(y.sj,e),e.receiveShadows&&t.include(M.hX,e),e.multipassTerrainEnabled&&(t.fragment.include(x.S),t.include(A.l,e)),t.fragment.uniforms.add("cameraPosition","vec3").add("localOrigin","vec3").add("ambient","vec3").add("diffuse","vec3").add("opacity","float").add("layerOpacity","float"),e.hasColorTexture&&t.fragment.uniforms.add("tex","sampler2D"),t.include(O.jV,e),t.include(C.T,e),t.fragment.include(R.y),t.include(S.k,e),L.add(P.H`
      void main() {
        discardBySlice(vpos);
        ${e.multipassTerrainEnabled?"terrainDepthTest(gl_FragCoord, depth);":""}
        ${e.hasColorTexture?P.H`
        vec4 texColor = texture2D(tex, vuv0);
        ${e.textureAlphaPremultiplied?"texColor.rgb /= texColor.a;":""}
        discardOrAdjustAlpha(texColor);`:P.H`vec4 texColor = vec4(1.0);`}
        shadingParams.viewDirection = normalize(vpos - cameraPosition);
        ${e.normalType===d.h.ScreenDerivative?P.H`
        vec3 normal = screenDerivativeNormal(localvpos);`:P.H`
        shadingParams.normalView = vNormalWorld;
        vec3 normal = shadingNormal(shadingParams);`}
        ${e.pbrMode===O.f7.Normal?"applyPBRFactors();":""}
        float ssao = evaluateAmbientOcclusionInverse();
        ssao *= getBakedOcclusion();

        float additionalAmbientScale = additionalDirectedAmbientLight(vpos + localOrigin);
        vec3 additionalLight = ssao * lightingMainIntensity * additionalAmbientScale * ambientBoostFactor * lightingGlobalFactor;
        ${e.receiveShadows?"float shadow = readShadowMap(vpos, linearDepth);":e.viewingMode===r.JY.Global?"float shadow = lightingGlobalFactor * (1.0 - additionalAmbientScale);":"float shadow = 0.0;"}
        vec3 matColor = max(ambient, diffuse);
        ${e.attributeColor?P.H`
        vec3 albedo_ = mixExternalColor(vColor.rgb * matColor, texColor.rgb, vcolorExt.rgb, int(colorMixMode));
        float opacity_ = layerOpacity * mixExternalOpacity(vColor.a * opacity, texColor.a, vcolorExt.a, int(colorMixMode));`:P.H`
        vec3 albedo_ = mixExternalColor(matColor, texColor.rgb, vcolorExt.rgb, int(colorMixMode));
        float opacity_ = layerOpacity * mixExternalOpacity(opacity, texColor.a, vcolorExt.a, int(colorMixMode));
        `}
        ${e.hasNormalTexture?P.H`
              mat3 tangentSpace = ${e.vertexTangents?"computeTangentSpace(normal);":"computeTangentSpace(normal, vpos, vuv0);"}
              vec3 shadedNormal = computeTextureNormal(tangentSpace, vuv0);`:"vec3 shadedNormal = normal;"}
        ${e.pbrMode===O.f7.Normal||e.pbrMode===O.f7.Schematic?e.viewingMode===r.JY.Global?P.H`vec3 normalGround = normalize(vpos + localOrigin);`:P.H`vec3 normalGround = vec3(0.0, 0.0, 1.0);`:P.H``}
        ${e.pbrMode===O.f7.Normal||e.pbrMode===O.f7.Schematic?P.H`
            float additionalAmbientIrradiance = additionalAmbientIrradianceFactor * lightingMainIntensity[2];
            vec3 shadedColor = evaluateSceneLightingPBR(shadedNormal, albedo_, shadow, 1.0 - ssao, additionalLight, shadingParams.viewDirection, normalGround, mrr, emission, additionalAmbientIrradiance);`:"vec3 shadedColor = evaluateSceneLighting(shadedNormal, albedo_, shadow, 1.0 - ssao, additionalLight);"}
        gl_FragColor = highlightSlice(vec4(shadedColor, opacity_), vpos);
        ${e.oitEnabled?"gl_FragColor = premultiplyAlpha(gl_FragColor);":""}
      }
    `)),t.include(g.s,e),t}const D=Object.freeze({__proto__:null,build:L})},60926:function(e,t,i){i.d(t,{R:function(){return P},b:function(){return R}});var r=i(65684),o=i(95650),n=i(57218),a=i(25714),s=i(5885),l=i(4731),c=i(99163),d=i(27125),u=i(91636),h=i(45658),f=i(82082),m=i(6502),p=i(78183),v=i(52372),g=i(6665),x=i(30786),T=i(40566),_=i(73393),b=i(74839),A=i(3864),S=i(20105),C=i(12664),O=i(41272),M=i(42727),E=i(23410),y=i(3961),w=i(21414);function R(e){const t=new y.kG,i=t.vertex.code,R=t.fragment.code;return t.vertex.uniforms.add("proj","mat4").add("view","mat4").add("cameraPosition","vec3").add("localOrigin","vec3"),t.include(u.f),t.varyings.add("vpos","vec3"),t.include(C.kl,e),t.include(c.fQ,e),t.include(p.LC,e),e.output!==a.H.Color&&e.output!==a.H.Alpha||(t.include(d.O,e),t.include(l.w,{linearDepth:!1}),e.offsetBackfaces&&t.include(n.w),e.instancedColor&&t.attributes.add(w.T.INSTANCECOLOR,"vec4"),t.varyings.add("vNormalWorld","vec3"),t.varyings.add("localvpos","vec3"),e.multipassTerrainEnabled&&t.varyings.add("depth","float"),t.include(f.D,e),t.include(o.q,e),t.include(h.R,e),t.include(m.c,e),t.vertex.uniforms.add("externalColor","vec4"),t.varyings.add("vcolorExt","vec4"),i.add(E.H`
        void main(void) {
          forwardNormalizedVertexColor();
          vcolorExt = externalColor;
          ${e.instancedColor?"vcolorExt *= instanceColor;":""}
          vcolorExt *= vvColor();
          vcolorExt *= getSymbolColor();
          forwardColorMixMode();

          if (vcolorExt.a < ${E.H.float(O.bf)}) {
            gl_Position = vec4(1e38, 1e38, 1e38, 1.0);
          }
          else {
            vpos = calculateVPos();
            localvpos = vpos - view[3].xyz;
            vpos = subtractOrigin(vpos);
            vNormalWorld = dpNormal(vvLocalNormal(normalModel()));
            vpos = addVerticalOffset(vpos, localOrigin);
            gl_Position = transformPosition(proj, view, vpos);
            ${e.offsetBackfaces?"gl_Position = offsetBackfacingClipPosition(gl_Position, vpos, vNormalWorld, cameraPosition);":""}
          }
          ${e.multipassTerrainEnabled?E.H`depth = (view * vec4(vpos, 1.0)).z;`:""}
          forwardLinearDepth();
          forwardTextureCoordinates();
        }
      `)),e.output===a.H.Alpha&&(t.include(s.p2,e),t.include(O.sj,e),e.multipassTerrainEnabled&&(t.fragment.include(g.S),t.include(_.l,e)),t.fragment.uniforms.add("cameraPosition","vec3").add("localOrigin","vec3").add("opacity","float").add("layerOpacity","float"),t.fragment.uniforms.add("view","mat4"),e.hasColorTexture&&t.fragment.uniforms.add("tex","sampler2D"),t.fragment.include(M.y),R.add(E.H`
      void main() {
        discardBySlice(vpos);
        ${e.multipassTerrainEnabled?E.H`terrainDepthTest(gl_FragCoord, depth);`:""}
        ${e.hasColorTexture?E.H`
        vec4 texColor = texture2D(tex, vuv0);
        ${e.textureAlphaPremultiplied?"texColor.rgb /= texColor.a;":""}
        discardOrAdjustAlpha(texColor);`:E.H`vec4 texColor = vec4(1.0);`}
        ${e.attributeColor?E.H`
        float opacity_ = layerOpacity * mixExternalOpacity(vColor.a * opacity, texColor.a, vcolorExt.a, int(colorMixMode));`:E.H`
        float opacity_ = layerOpacity * mixExternalOpacity(opacity, texColor.a, vcolorExt.a, int(colorMixMode));
        `}

        gl_FragColor = vec4(opacity_);
      }
    `)),e.output===a.H.Color&&(t.include(s.p2,e),t.include(T.X,e),t.include(x.K,e),t.include(O.sj,e),e.receiveShadows&&t.include(S.hX,e),e.multipassTerrainEnabled&&(t.fragment.include(g.S),t.include(_.l,e)),t.fragment.uniforms.add("cameraPosition","vec3").add("localOrigin","vec3").add("ambient","vec3").add("diffuse","vec3").add("opacity","float").add("layerOpacity","float"),t.fragment.uniforms.add("view","mat4"),e.hasColorTexture&&t.fragment.uniforms.add("tex","sampler2D"),t.include(A.jV,e),t.include(b.T,e),t.fragment.include(M.y),R.add(E.H`
      void main() {
        discardBySlice(vpos);
        ${e.multipassTerrainEnabled?E.H`terrainDepthTest(gl_FragCoord, depth);`:""}
        ${e.hasColorTexture?E.H`
        vec4 texColor = texture2D(tex, vuv0);
        ${e.textureAlphaPremultiplied?"texColor.rgb /= texColor.a;":""}
        discardOrAdjustAlpha(texColor);`:E.H`vec4 texColor = vec4(1.0);`}
        vec3 viewDirection = normalize(vpos - cameraPosition);
        ${e.pbrMode===A.f7.Normal?"applyPBRFactors();":""}
        float ssao = evaluateAmbientOcclusionInverse();
        ssao *= getBakedOcclusion();

        float additionalAmbientScale = additionalDirectedAmbientLight(vpos + localOrigin);
        vec3 additionalLight = ssao * lightingMainIntensity * additionalAmbientScale * ambientBoostFactor * lightingGlobalFactor;
        ${e.receiveShadows?"float shadow = readShadowMap(vpos, linearDepth);":e.viewingMode===r.JY.Global?"float shadow = lightingGlobalFactor * (1.0 - additionalAmbientScale);":"float shadow = 0.0;"}
        vec3 matColor = max(ambient, diffuse);
        ${e.attributeColor?E.H`
        vec3 albedo_ = mixExternalColor(vColor.rgb * matColor, texColor.rgb, vcolorExt.rgb, int(colorMixMode));
        float opacity_ = layerOpacity * mixExternalOpacity(vColor.a * opacity, texColor.a, vcolorExt.a, int(colorMixMode));`:E.H`
        vec3 albedo_ = mixExternalColor(matColor, texColor.rgb, vcolorExt.rgb, int(colorMixMode));
        float opacity_ = layerOpacity * mixExternalOpacity(opacity, texColor.a, vcolorExt.a, int(colorMixMode));
        `}
        ${E.H`
        vec3 shadedNormal = normalize(vNormalWorld);
        albedo_ *= 1.2;
        vec3 viewForward = vec3(view[0][2], view[1][2], view[2][2]);
        float alignmentLightView = clamp(dot(viewForward, -lightingMainDirection), 0.0, 1.0);
        float transmittance = 1.0 - clamp(dot(viewForward, shadedNormal), 0.0, 1.0);
        float treeRadialFalloff = vColor.r;
        float backLightFactor = 0.5 * treeRadialFalloff * alignmentLightView * transmittance * (1.0 - shadow);
        additionalLight += backLightFactor * lightingMainIntensity;`}
        ${e.pbrMode===A.f7.Normal||e.pbrMode===A.f7.Schematic?e.viewingMode===r.JY.Global?E.H`vec3 normalGround = normalize(vpos + localOrigin);`:E.H`vec3 normalGround = vec3(0.0, 0.0, 1.0);`:E.H``}
        ${e.pbrMode===A.f7.Normal||e.pbrMode===A.f7.Schematic?E.H`
            float additionalAmbientIrradiance = additionalAmbientIrradianceFactor * lightingMainIntensity[2];
            vec3 shadedColor = evaluateSceneLightingPBR(shadedNormal, albedo_, shadow, 1.0 - ssao, additionalLight, viewDirection, normalGround, mrr, emission, additionalAmbientIrradiance);`:"vec3 shadedColor = evaluateSceneLighting(shadedNormal, albedo_, shadow, 1.0 - ssao, additionalLight);"}
        gl_FragColor = highlightSlice(vec4(shadedColor, opacity_), vpos);
        ${e.oitEnabled?"gl_FragColor = premultiplyAlpha(gl_FragColor);":""}
      }
    `)),t.include(v.s,e),t}const P=Object.freeze({__proto__:null,build:R})},79912:function(e,t,i){function r(){return new Float32Array(3)}function o(e){const t=new Float32Array(3);return t[0]=e[0],t[1]=e[1],t[2]=e[2],t}function n(e,t,i){const r=new Float32Array(3);return r[0]=e,r[1]=t,r[2]=i,r}function a(e,t){return new Float32Array(e,t,3)}function s(){return r()}function l(){return n(1,1,1)}function c(){return n(1,0,0)}function d(){return n(0,1,0)}function u(){return n(0,0,1)}i.d(t,{c:function(){return r},f:function(){return n}});const h=s(),f=l(),m=c(),p=d(),v=u();Object.freeze({__proto__:null,create:r,clone:o,fromValues:n,createView:a,zeros:s,ones:l,unitX:c,unitY:d,unitZ:u,ZEROS:h,ONES:f,UNIT_X:m,UNIT_Y:p,UNIT_Z:v})},57989:function(e,t,i){function r(e){return e=e||globalThis.location.hostname,c.some((t=>{var i;return null!=(null==(i=e)?void 0:i.match(t))}))}function o(e,t){return e&&(t=t||globalThis.location.hostname)?null!=t.match(n)||null!=t.match(s)?e.replace("static.arcgis.com","staticdev.arcgis.com"):null!=t.match(a)||null!=t.match(l)?e.replace("static.arcgis.com","staticqa.arcgis.com"):e:e}i.d(t,{XO:function(){return r},pJ:function(){return o}});const n=/^devext.arcgis.com$/,a=/^qaext.arcgis.com$/,s=/^[\w-]*\.mapsdevext.arcgis.com$/,l=/^[\w-]*\.mapsqa.arcgis.com$/,c=[/^([\w-]*\.)?[\w-]*\.zrh-dev-local.esri.com$/,n,a,/^jsapps.esri.com$/,s,l]},44883:function(e,t,i){i.d(t,{t:function(){return o}});var r=i(66341);async function o(e,t){const{data:i}=await(0,r["default"])(e,{responseType:"image",...t});return i}},60160:function(e,t,i){i.r(t),i.d(t,{fetch:function(){return sr},gltfToEngineResources:function(){return cr},parseUrl:function(){return lr}});var r=i(57989),o=i(61681),n=i(1662),a=i(34344),s=i(24455),l=i(39100),c=i(6766),d=i(8909),u=i(37116),h=i(63338),f=i(86717),m=i(56999),p=i(64122),v=i(91420),g=i(64987),x=i(1731),T=i(66341),_=i(67979),b=i(70375),A=i(13802),S=i(78668),C=i(26139),O=i(44883),M=i(70984),E=i(17993),y=i(15095);class w{constructor(e,t,i,r){this.primitiveIndices=e,this._numIndexPerPrimitive=t,this.indices=i,this.position=r,this.center=(0,d.c)(),(0,y.hu)(e.length>=1),(0,y.hu)(i.length%this._numIndexPerPrimitive==0),(0,y.hu)(i.length>=e.length*this._numIndexPerPrimitive),(0,y.hu)(3===r.size||4===r.size);const{data:o,size:n}=r,a=e.length;let s=n*i[this._numIndexPerPrimitive*e[0]];R.clear(),R.push(s),this.bbMin=(0,d.f)(o[s],o[s+1],o[s+2]),this.bbMax=(0,d.a)(this.bbMin);for(let c=0;c<a;++c){const t=this._numIndexPerPrimitive*e[c];for(let e=0;e<this._numIndexPerPrimitive;++e){s=n*i[t+e],R.push(s);let r=o[s];this.bbMin[0]=Math.min(r,this.bbMin[0]),this.bbMax[0]=Math.max(r,this.bbMax[0]),r=o[s+1],this.bbMin[1]=Math.min(r,this.bbMin[1]),this.bbMax[1]=Math.max(r,this.bbMax[1]),r=o[s+2],this.bbMin[2]=Math.min(r,this.bbMin[2]),this.bbMax[2]=Math.max(r,this.bbMax[2])}}(0,c.e)(this.center,this.bbMin,this.bbMax,.5),this.radius=.5*Math.max(Math.max(this.bbMax[0]-this.bbMin[0],this.bbMax[1]-this.bbMin[1]),this.bbMax[2]-this.bbMin[2]);let l=this.radius*this.radius;for(let c=0;c<R.length;++c){s=R.getItemAt(c);const e=o[s]-this.center[0],t=o[s+1]-this.center[1],i=o[s+2]-this.center[2],r=e*e+t*t+i*i;if(r<=l)continue;const n=Math.sqrt(r),a=.5*(n-this.radius);this.radius=this.radius+a,l=this.radius*this.radius;const d=a/n;this.center[0]+=e*d,this.center[1]+=t*d,this.center[2]+=i*d}R.clear()}getCenter(){return this.center}getBSRadius(){return this.radius}getBBMin(){return this.bbMin}getBBMax(){return this.bbMax}getChildren(){if(this._children)return this._children;if((0,c.h)(this.bbMin,this.bbMax)>1){const e=(0,c.e)((0,d.c)(),this.bbMin,this.bbMax,.5),t=this.primitiveIndices.length,i=new Uint8Array(t),r=new Array(8);for(let l=0;l<8;++l)r[l]=0;const{data:o,size:n}=this.position;for(let l=0;l<t;++l){let t=0;const a=this._numIndexPerPrimitive*this.primitiveIndices[l];let s=n*this.indices[a],c=o[s],d=o[s+1],u=o[s+2];for(let e=1;e<this._numIndexPerPrimitive;++e){s=n*this.indices[a+e];const t=o[s],i=o[s+1],r=o[s+2];t<c&&(c=t),i<d&&(d=i),r<u&&(u=r)}c<e[0]&&(t|=1),d<e[1]&&(t|=2),u<e[2]&&(t|=4),i[l]=t,++r[t]}let a=0;for(let l=0;l<8;++l)r[l]>0&&++a;if(a<2)return;const s=new Array(8);for(let l=0;l<8;++l)s[l]=r[l]>0?new Uint32Array(r[l]):void 0;for(let l=0;l<8;++l)r[l]=0;for(let l=0;l<t;++l){const e=i[l];s[e][r[e]++]=this.primitiveIndices[l]}this._children=new Array(8);for(let l=0;l<8;++l)void 0!==s[l]&&(this._children[l]=new w(s[l],this._numIndexPerPrimitive,this.indices,this.position))}return this._children}static prune(){R.prune()}}const R=new E.Z({deallocator:null});var P,I=i(7958);class N{constructor(){this.id=(0,I.D)()}unload(){}}!function(e){e[e.Layer=0]="Layer",e[e.Object=1]="Object",e[e.Geometry=2]="Geometry",e[e.Material=3]="Material",e[e.Texture=4]="Texture",e[e.COUNT=5]="COUNT"}(P||(P={}));var L=i(8860),D=i(21414);class H extends N{constructor(e,t=[],i=M.MX.Triangle,r=-1){super(),this._primitiveType=i,this.edgeIndicesLength=r,this.type=P.Geometry,this._vertexAttributes=new Map,this._indices=new Map,this._boundingInfo=null;for(const[o,n]of e)n&&this._vertexAttributes.set(o,{...n});if(null==t||0===t.length){const e=F(this._vertexAttributes),t=(0,L.p)(e);this.edgeIndicesLength=this.edgeIndicesLength<0?e:this.edgeIndicesLength;for(const i of this._vertexAttributes.keys())this._indices.set(i,t)}else for(const[o,n]of t)n&&(this._indices.set(o,z(n)),o===D.T.POSITION&&(this.edgeIndicesLength=this.edgeIndicesLength<0?this._indices.get(o).length:this.edgeIndicesLength))}cloneShallow(){const e=new H([],void 0,this._primitiveType,void 0),{_vertexAttributes:t,_indices:i}=e;return this._vertexAttributes.forEach(((e,i)=>{t.set(i,e)})),this._indices.forEach(((e,t)=>{i.set(t,e)})),e.screenToWorldRatio=this.screenToWorldRatio,e._boundingInfo=this._boundingInfo,e}get vertexAttributes(){return this._vertexAttributes}getMutableAttribute(e){const t=this._vertexAttributes.get(e);return t&&!t.exclusive&&(t.data=Array.from(t.data),t.exclusive=!0),t}get indices(){return this._indices}get indexCount(){const e=this._indices.values().next().value;return e?e.length:0}get primitiveType(){return this._primitiveType}get faceCount(){return this.indexCount/3}get boundingInfo(){return(0,o.Wi)(this._boundingInfo)&&(this._boundingInfo=this._calculateBoundingInfo()),this._boundingInfo}computeAttachmentOrigin(e){return this.primitiveType===M.MX.Triangle?this._computeAttachmentOriginTriangles(e):this._computeAttachmentOriginPoints(e)}_computeAttachmentOriginTriangles(e){const t=this.indices.get(D.T.POSITION),i=this.vertexAttributes.get(D.T.POSITION);return(0,L.cM)(i,t,e)}_computeAttachmentOriginPoints(e){const t=this.indices.get(D.T.POSITION),i=this.vertexAttributes.get(D.T.POSITION);return(0,L.NO)(i,t,e)}invalidateBoundingInfo(){this._boundingInfo=null}_calculateBoundingInfo(){const e=this.indices.get(D.T.POSITION);if(0===e.length)return null;const t=this.primitiveType===M.MX.Triangle?3:1;(0,y.hu)(e.length%t==0,"Indexing error: "+e.length+" not divisible by "+t);const i=(0,L.p)(e.length/t),r=this.vertexAttributes.get(D.T.POSITION);return new w(i,t,e,r)}}function F(e){const t=e.values().next().value;return null==t?0:t.data.length/t.size}function z(e){if(e.BYTES_PER_ELEMENT===Uint16Array.BYTES_PER_ELEMENT)return e;for(const t of e)if(t>=65536)return e;return new Uint16Array(e)}var B=i(27755),U=i(31355),G=i(19431),V=i(86098),W=i(3466),k=i(73401),q=i(36567);function $(){if((0,o.Wi)(j)){const e=e=>(0,q.V)(`esri/libs/basisu/${e}`);j=i.e(1681).then(i.bind(i,21681)).then((e=>e.b)).then((({default:t})=>t({locateFile:e}).then((e=>(e.initializeBasis(),delete e.then,e)))))}return j}let j;var X;!function(e){e[e.ETC1_RGB=0]="ETC1_RGB",e[e.ETC2_RGBA=1]="ETC2_RGBA",e[e.BC1_RGB=2]="BC1_RGB",e[e.BC3_RGBA=3]="BC3_RGBA",e[e.BC4_R=4]="BC4_R",e[e.BC5_RG=5]="BC5_RG",e[e.BC7_M6_RGB=6]="BC7_M6_RGB",e[e.BC7_M5_RGBA=7]="BC7_M5_RGBA",e[e.PVRTC1_4_RGB=8]="PVRTC1_4_RGB",e[e.PVRTC1_4_RGBA=9]="PVRTC1_4_RGBA",e[e.ASTC_4x4_RGBA=10]="ASTC_4x4_RGBA",e[e.ATC_RGB=11]="ATC_RGB",e[e.ATC_RGBA=12]="ATC_RGBA",e[e.FXT1_RGB=17]="FXT1_RGB",e[e.PVRTC2_4_RGB=18]="PVRTC2_4_RGB",e[e.PVRTC2_4_RGBA=19]="PVRTC2_4_RGBA",e[e.ETC2_EAC_R11=20]="ETC2_EAC_R11",e[e.ETC2_EAC_RG11=21]="ETC2_EAC_RG11",e[e.RGBA32=13]="RGBA32",e[e.RGB565=14]="RGB565",e[e.BGR565=15]="BGR565",e[e.RGBA4444=16]="RGBA4444"}(X||(X={}));var J=i(91907),K=i(43487),Y=i(62486);let Z=null,Q=null;async function ee(){return(0,o.Wi)(Q)&&(Q=$(),Z=await Q),Q}function te(e,t){if((0,o.Wi)(Z))return e.byteLength;const i=new Z.BasisFile(new Uint8Array(e)),r=oe(i)?re(i.getNumLevels(0),i.getHasAlpha(),i.getImageWidth(0,0),i.getImageHeight(0,0),t):0;return i.close(),i.delete(),r}function ie(e,t){if((0,o.Wi)(Z))return e.byteLength;const i=new Z.KTX2File(new Uint8Array(e)),r=ne(i)?re(i.getLevels(),i.getHasAlpha(),i.getWidth(),i.getHeight(),t):0;return i.close(),i.delete(),r}function re(e,t,i,r,o){const n=(0,Y.RG)(t?J.q_.COMPRESSED_RGBA8_ETC2_EAC:J.q_.COMPRESSED_RGB8_ETC2),a=o&&e>1?(4**e-1)/(3*4**(e-1)):1;return Math.ceil(i*r*n*a)}function oe(e){return e.getNumImages()>=1&&!e.isUASTC()}function ne(e){return e.getFaces()>=1&&e.isETC1S()}async function ae(e,t,i){(0,o.Wi)(Z)&&(Z=await ee());const r=new Z.BasisFile(new Uint8Array(i));if(!oe(r))return null;r.startTranscoding();const n=le(e,t,r.getNumLevels(0),r.getHasAlpha(),r.getImageWidth(0,0),r.getImageHeight(0,0),((e,t)=>r.getImageTranscodedSizeInBytes(0,e,t)),((e,t,i)=>r.transcodeImage(i,0,e,t,0,0)));return r.close(),r.delete(),n}async function se(e,t,i){(0,o.Wi)(Z)&&(Z=await ee());const r=new Z.KTX2File(new Uint8Array(i));if(!ne(r))return null;r.startTranscoding();const n=le(e,t,r.getLevels(),r.getHasAlpha(),r.getWidth(),r.getHeight(),((e,t)=>r.getImageTranscodedSizeInBytes(e,0,0,t)),((e,t,i)=>r.transcodeImage(i,e,0,0,t,0,-1,-1)));return r.close(),r.delete(),n}function le(e,t,i,r,o,n,a,s){const{compressedTextureETC:l,compressedTextureS3TC:c}=e.capabilities,[d,u]=l?r?[X.ETC2_RGBA,J.q_.COMPRESSED_RGBA8_ETC2_EAC]:[X.ETC1_RGB,J.q_.COMPRESSED_RGB8_ETC2]:c?r?[X.BC3_RGBA,J.q_.COMPRESSED_RGBA_S3TC_DXT5_EXT]:[X.BC1_RGB,J.q_.COMPRESSED_RGB_S3TC_DXT1_EXT]:[X.RGBA32,J.VI.RGBA],h=t.hasMipmap?i:Math.min(1,i),f=[];for(let g=0;g<h;g++)f.push(new Uint8Array(a(g,d))),s(g,d,f[g]);const m=f.length>1,p=m?J.cw.LINEAR_MIPMAP_LINEAR:J.cw.LINEAR,v={...t,samplingMode:p,hasMipmap:m,internalFormat:u,width:o,height:n};return new K.x(e,v,{type:"compressed",levels:f})}const ce=A.Z.getLogger("esri.views.3d.webgl-engine.lib.DDSUtil"),de=542327876,ue=131072,he=4;function fe(e){return e.charCodeAt(0)+(e.charCodeAt(1)<<8)+(e.charCodeAt(2)<<16)+(e.charCodeAt(3)<<24)}function me(e){return String.fromCharCode(255&e,e>>8&255,e>>16&255,e>>24&255)}const pe=fe("DXT1"),ve=fe("DXT3"),ge=fe("DXT5"),xe=31,Te=0,_e=1,be=2,Ae=3,Se=4,Ce=7,Oe=20,Me=21;function Ee(e,t,i){const{textureData:r,internalFormat:o,width:n,height:a}=ye(i,t.hasMipmap);return t.samplingMode=r.levels.length>1?J.cw.LINEAR_MIPMAP_LINEAR:J.cw.LINEAR,t.hasMipmap=r.levels.length>1,t.internalFormat=o,t.width=n,t.height=a,new K.x(e,t,r)}function ye(e,t){const i=new Int32Array(e,0,xe);if(i[Te]!==de)return ce.error("Invalid magic number in DDS header"),null;if(!(i[Oe]&he))return ce.error("Unsupported format, must contain a FourCC code"),null;const r=i[Me];let o,n;switch(r){case pe:o=8,n=J.q_.COMPRESSED_RGB_S3TC_DXT1_EXT;break;case ve:o=16,n=J.q_.COMPRESSED_RGBA_S3TC_DXT3_EXT;break;case ge:o=16,n=J.q_.COMPRESSED_RGBA_S3TC_DXT5_EXT;break;default:return ce.error("Unsupported FourCC code:",me(r)),null}let a=1,s=i[Se],l=i[Ae];0==(3&s)&&0==(3&l)||(ce.warn("Rounding up compressed texture size to nearest multiple of 4."),s=s+3&-4,l=l+3&-4);const c=s,d=l;let u,h;i[be]&ue&&!1!==t&&(a=Math.max(1,i[Ce])),1===a||(0,G.wt)(s)&&(0,G.wt)(l)||(ce.warn("Ignoring mipmaps of non power of two sized compressed texture."),a=1);let f=i[_e]+4;const m=[];for(let p=0;p<a;++p)h=(s+3>>2)*(l+3>>2)*o,u=new Uint8Array(e,f,h),m.push(u),f+=h,s=Math.max(1,s>>1),l=Math.max(1,l>>1);return{textureData:{type:"compressed",levels:m},internalFormat:n,width:c,height:d}}const we=new Map([[D.T.POSITION,0],[D.T.NORMAL,1],[D.T.UV0,2],[D.T.COLOR,3],[D.T.SIZE,4],[D.T.TANGENT,4],[D.T.AUXPOS1,5],[D.T.SYMBOLCOLOR,5],[D.T.AUXPOS2,6],[D.T.FEATUREATTRIBUTE,6],[D.T.INSTANCEFEATUREATTRIBUTE,6],[D.T.INSTANCECOLOR,7],[D.T.MODEL,8],[D.T.MODELNORMAL,12],[D.T.MODELORIGINHI,11],[D.T.MODELORIGINLO,15]]);var Re=i(41163);new Re.G(D.T.POSITION,3,J.g.FLOAT,0,12),new Re.G(D.T.POSITION,3,J.g.FLOAT,0,20),new Re.G(D.T.UV0,2,J.g.FLOAT,12,20),new Re.G(D.T.POSITION,3,J.g.FLOAT,0,32),new Re.G(D.T.NORMAL,3,J.g.FLOAT,12,32),new Re.G(D.T.UV0,2,J.g.FLOAT,24,32),new Re.G(D.T.POSITION,3,J.g.FLOAT,0,16),new Re.G(D.T.COLOR,4,J.g.UNSIGNED_BYTE,12,16);const Pe=[new Re.G(D.T.POSITION,2,J.g.FLOAT,0,8)],Ie=[new Re.G(D.T.POSITION,2,J.g.FLOAT,0,16),new Re.G(D.T.UV0,2,J.g.FLOAT,8,16)];var Ne=i(78951),Le=i(29620);function De(e,t=Pe,i=we,r=-1,o=1){let n=null;return n=t===Ie?new Float32Array([r,r,0,0,o,r,1,0,r,o,0,1,o,o,1,1]):new Float32Array([r,r,o,r,r,o,o,o]),new Le.U(e,i,{geometry:t},{geometry:Ne.f.createVertex(e,J.l1.STATIC_DRAW,n)})}var He,Fe=i(18567),ze=i(79193);class Be extends N{constructor(e,t){super(),this.data=e,this.type=P.Texture,this._glTexture=null,this._powerOfTwoStretchInfo=null,this._loadingPromise=null,this._loadingController=null,this.events=new U.Z,this.params=t||{},this.params.mipmap=!1!==this.params.mipmap,this.params.noUnpackFlip=this.params.noUnpackFlip||!1,this.params.preMultiplyAlpha=this.params.preMultiplyAlpha||!1,this.params.wrap=this.params.wrap||{s:J.e8.REPEAT,t:J.e8.REPEAT},this.params.powerOfTwoResizeMode=this.params.powerOfTwoResizeMode||M.CE.STRETCH,this.estimatedTexMemRequired=Be._estimateTexMemRequired(this.data,this.params),this._startPreload()}_startPreload(){const e=this.data;(0,o.Wi)(e)||(e instanceof HTMLVideoElement?this._startPreloadVideoElement(e):e instanceof HTMLImageElement&&this._startPreloadImageElement(e))}_startPreloadVideoElement(e){(0,W.jc)(e.src)||"auto"===e.preload&&e.crossOrigin||(e.preload="auto",e.crossOrigin="anonymous",e.src=e.src)}_startPreloadImageElement(e){(0,W.HK)(e.src)||(0,W.jc)(e.src)||e.crossOrigin||(e.crossOrigin="anonymous",e.src=e.src)}static _getDataDimensions(e){return e instanceof HTMLVideoElement?{width:e.videoWidth,height:e.videoHeight}:e}static _estimateTexMemRequired(e,t){if((0,o.Wi)(e))return 0;if((0,V.eP)(e)||(0,V.lq)(e))return t.encoding===Be.KTX2_ENCODING?ie(e,t.mipmap):t.encoding===Be.BASIS_ENCODING?te(e,t.mipmap):e.byteLength;const{width:i,height:r}=e instanceof Image||e instanceof ImageData||e instanceof HTMLCanvasElement||e instanceof HTMLVideoElement?Be._getDataDimensions(e):t;return(t.mipmap?4/3:1)*i*r*(t.components||4)||0}dispose(){this.data=void 0}get width(){return this.params.width}get height(){return this.params.height}_createDescriptor(e){var t;return{target:J.No.TEXTURE_2D,pixelFormat:J.VI.RGBA,dataType:J.Br.UNSIGNED_BYTE,wrapMode:this.params.wrap,flipped:!this.params.noUnpackFlip,samplingMode:this.params.mipmap?J.cw.LINEAR_MIPMAP_LINEAR:J.cw.LINEAR,hasMipmap:this.params.mipmap,preMultiplyAlpha:this.params.preMultiplyAlpha,maxAnisotropy:null!=(t=this.params.maxAnisotropy)?t:this.params.mipmap?e.parameters.maxMaxAnisotropy:1}}get glTexture(){return this._glTexture}load(e,t){if((0,o.pC)(this._glTexture))return this._glTexture;if((0,o.pC)(this._loadingPromise))return this._loadingPromise;const i=this.data;return(0,o.Wi)(i)?(this._glTexture=new K.x(e,this._createDescriptor(e),null),this._glTexture):"string"==typeof i?this._loadFromURL(e,t,i):i instanceof Image?this._loadFromImageElement(e,t,i):i instanceof HTMLVideoElement?this._loadFromVideoElement(e,t,i):i instanceof ImageData||i instanceof HTMLCanvasElement?this._loadFromImage(e,i,t):((0,V.eP)(i)||(0,V.lq)(i))&&this.params.encoding===Be.DDS_ENCODING?(this.data=void 0,this._loadFromDDSData(e,i)):((0,V.eP)(i)||(0,V.lq)(i))&&this.params.encoding===Be.KTX2_ENCODING?(this.data=void 0,this._loadFromKTX2(e,i)):((0,V.eP)(i)||(0,V.lq)(i))&&this.params.encoding===Be.BASIS_ENCODING?(this.data=void 0,this._loadFromBasis(e,i)):(0,V.lq)(i)?this._loadFromPixelData(e,i):(0,V.eP)(i)?this._loadFromPixelData(e,new Uint8Array(i)):null}get requiresFrameUpdates(){return this.data instanceof HTMLVideoElement}frameUpdate(e,t,i){if(!(this.data instanceof HTMLVideoElement)||(0,o.Wi)(this._glTexture))return i;if(this.data.readyState<He.HAVE_CURRENT_DATA||i===this.data.currentTime)return i;if((0,o.pC)(this._powerOfTwoStretchInfo)){const{framebuffer:i,vao:r,sourceTexture:o}=this._powerOfTwoStretchInfo;o.setData(this.data),this._drawStretchedTexture(e,t,i,r,o,this._glTexture)}else{const{width:e,height:t}=this.data,{width:i,height:r}=this._glTexture.descriptor;e!==i||t!==r?this._glTexture.updateData(0,0,0,Math.min(e,i),Math.min(t,r),this.data):this._glTexture.setData(this.data)}return this._glTexture.descriptor.hasMipmap&&this._glTexture.generateMipmap(),this.data.currentTime}_loadFromDDSData(e,t){return this._glTexture=Ee(e,this._createDescriptor(e),t),this._glTexture}_loadFromKTX2(e,t){return this._loadAsync((()=>se(e,this._createDescriptor(e),t).then((e=>(this._glTexture=e,e)))))}_loadFromBasis(e,t){return this._loadAsync((()=>ae(e,this._createDescriptor(e),t).then((e=>(this._glTexture=e,e)))))}_loadFromPixelData(e,t){(0,y.hu)(this.params.width>0&&this.params.height>0);const i=this._createDescriptor(e);return i.pixelFormat=1===this.params.components?J.VI.LUMINANCE:3===this.params.components?J.VI.RGB:J.VI.RGBA,i.width=this.params.width,i.height=this.params.height,this._glTexture=new K.x(e,i,t),this._glTexture}_loadFromURL(e,t,i){return this._loadAsync((async r=>{const o=await(0,O.t)(i,{signal:r});return(0,S.k_)(r),this._loadFromImage(e,o,t)}))}_loadFromImageElement(e,t,i){return i.complete?this._loadFromImage(e,i,t):this._loadAsync((async r=>{const o=await(0,k.f)(i,i.src,!1,r);return(0,S.k_)(r),this._loadFromImage(e,o,t)}))}_loadFromVideoElement(e,t,i){return i.readyState>=He.HAVE_CURRENT_DATA?this._loadFromImage(e,i,t):this._loadFromVideoElementAsync(e,t,i)}_loadFromVideoElementAsync(e,t,i){return this._loadAsync((r=>new Promise(((n,a)=>{const s=()=>{i.removeEventListener("loadeddata",l),i.removeEventListener("error",c),(0,o.hw)(d)},l=()=>{i.readyState>=He.HAVE_CURRENT_DATA&&(s(),n(this._loadFromImage(e,i,t)))},c=e=>{s(),a(e||new b.Z("Failed to load video"))};i.addEventListener("loadeddata",l),i.addEventListener("error",c);const d=(0,S.fu)(r,(()=>c((0,S.zE)())))}))))}_loadFromImage(e,t,i){const r=Be._getDataDimensions(t);this.params.width=r.width,this.params.height=r.height;const o=this._createDescriptor(e);return o.pixelFormat=3===this.params.components?J.VI.RGB:J.VI.RGBA,!this._requiresPowerOfTwo(e,o)||(0,G.wt)(r.width)&&(0,G.wt)(r.height)?(o.width=r.width,o.height=r.height,this._glTexture=new K.x(e,o,t),this._glTexture):(this._glTexture=this._makePowerOfTwoTexture(e,t,r,o,i),this._glTexture)}_loadAsync(e){const t=new AbortController;this._loadingController=t;const i=e(t.signal);this._loadingPromise=i;const r=()=>{this._loadingController===t&&(this._loadingController=null),this._loadingPromise===i&&(this._loadingPromise=null)};return i.then(r,r),i}_requiresPowerOfTwo(e,t){const i=J.e8.CLAMP_TO_EDGE,r="number"==typeof t.wrapMode?t.wrapMode===i:t.wrapMode.s===i&&t.wrapMode.t===i;return!(0,ze.Z)(e.gl)&&(t.hasMipmap||!r)}_makePowerOfTwoTexture(e,t,i,r,o){const{width:n,height:a}=i,s=(0,G.Sf)(n),l=(0,G.Sf)(a);let c;switch(r.width=s,r.height=l,this.params.powerOfTwoResizeMode){case M.CE.PAD:r.textureCoordinateScaleFactor=[n/s,a/l],c=new K.x(e,r),c.updateData(0,0,0,n,a,t);break;case M.CE.STRETCH:case null:case void 0:c=this._stretchToPowerOfTwo(e,t,r,o());break;default:(0,B.Bg)(this.params.powerOfTwoResizeMode)}return r.hasMipmap&&c.generateMipmap(),c}_stretchToPowerOfTwo(e,t,i,r){const o=new K.x(e,i),n=new Fe.X(e,{colorTarget:J.Lm.TEXTURE,depthStencilTarget:J.OU.NONE},o),a=new K.x(e,{target:J.No.TEXTURE_2D,pixelFormat:i.pixelFormat,dataType:J.Br.UNSIGNED_BYTE,wrapMode:J.e8.CLAMP_TO_EDGE,samplingMode:J.cw.LINEAR,flipped:!!i.flipped,maxAnisotropy:8,preMultiplyAlpha:i.preMultiplyAlpha},t),s=De(e),l=e.getBoundFramebufferObject();return this._drawStretchedTexture(e,r,n,s,a,o),this.requiresFrameUpdates?this._powerOfTwoStretchInfo={vao:s,sourceTexture:a,framebuffer:n}:(s.dispose(!0),a.dispose(),n.detachColorTexture(),n.dispose()),e.bindFramebuffer(l),o}_drawStretchedTexture(e,t,i,r,o,n){e.bindFramebuffer(i);const a=e.getViewport();e.setViewport(0,0,n.descriptor.width,n.descriptor.height);const s=e.useTechnique(t);s.setUniform4f("uColor",1,1,1,1),s.bindTexture(o,"tex"),e.bindVAO(r),e.drawArrays(J.MX.TRIANGLE_STRIP,0,(0,Y._V)(r,"geometry")),e.bindFramebuffer(null),e.setViewport(a.x,a.y,a.width,a.height)}unload(){if((0,o.pC)(this._powerOfTwoStretchInfo)){const{framebuffer:e,vao:t,sourceTexture:i}=this._powerOfTwoStretchInfo;t.dispose(!0),i.dispose(),e.dispose(),this._glTexture=null,this._powerOfTwoStretchInfo=null}if((0,o.pC)(this._glTexture)&&(this._glTexture.dispose(),this._glTexture=null),(0,o.pC)(this._loadingController)){const e=this._loadingController;this._loadingController=null,this._loadingPromise=null,e.abort()}this.events.emit("unloaded")}}Be.DDS_ENCODING="image/vnd-ms.dds",Be.KTX2_ENCODING="image/ktx2",Be.BASIS_ENCODING="image/x.basis",function(e){e[e.HAVE_NOTHING=0]="HAVE_NOTHING",e[e.HAVE_METADATA=1]="HAVE_METADATA",e[e.HAVE_CURRENT_DATA=2]="HAVE_CURRENT_DATA",e[e.HAVE_FUTURE_DATA=3]="HAVE_FUTURE_DATA",e[e.HAVE_ENOUGH_DATA=4]="HAVE_ENOUGH_DATA"}(He||(He={}));var Ue=i(65684),Ge=i(44685),Ve=i(25714),We=i(2833),ke=i(41272);class qe{constructor(e){this._material=e.material,this._techniqueRep=e.techniqueRep,this._output=e.output}dispose(){this._techniqueRep.release(this._technique)}get technique(){return this._technique}ensureTechnique(e,t,i=this._output){return this._technique=this._techniqueRep.releaseAndAcquire(e,this._material.getTechniqueConfig(i,t),this._technique),this._technique}ensureResources(e){return M.Rw.LOADED}}class $e extends qe{constructor(e){super(e),this._numLoading=0,this._disposed=!1,this._textureRepository=e.textureRep,this._textureId=e.textureId,this._acquire(e.textureId,(e=>this._texture=e)),this._acquire(e.normalTextureId,(e=>this._textureNormal=e)),this._acquire(e.emissiveTextureId,(e=>this._textureEmissive=e)),this._acquire(e.occlusionTextureId,(e=>this._textureOcclusion=e)),this._acquire(e.metallicRoughnessTextureId,(e=>this._textureMetallicRoughness=e))}dispose(){this._texture=(0,o.RY)(this._texture),this._textureNormal=(0,o.RY)(this._textureNormal),this._textureEmissive=(0,o.RY)(this._textureEmissive),this._textureOcclusion=(0,o.RY)(this._textureOcclusion),this._textureMetallicRoughness=(0,o.RY)(this._textureMetallicRoughness),this._disposed=!0}ensureResources(e){return 0===this._numLoading?M.Rw.LOADED:M.Rw.LOADING}updateTexture(e){((0,o.Wi)(this._texture)||e!==this._texture.id)&&(this._texture=(0,o.RY)(this._texture),this._textureId=e,this._acquire(this._textureId,(e=>this._texture=e)))}bindTextures(e){(0,o.pC)(this._texture)&&e.bindTexture(this._texture.glTexture,"tex"),(0,o.pC)(this._textureNormal)&&e.bindTexture(this._textureNormal.glTexture,"normalTexture"),(0,o.pC)(this._textureEmissive)&&e.bindTexture(this._textureEmissive.glTexture,"texEmission"),(0,o.pC)(this._textureOcclusion)&&e.bindTexture(this._textureOcclusion.glTexture,"texOcclusion"),(0,o.pC)(this._textureMetallicRoughness)&&e.bindTexture(this._textureMetallicRoughness.glTexture,"texMetallicRoughness")}bindTextureScale(e){const t=(0,o.pC)(this._texture)?this._texture.glTexture:null;(0,o.pC)(t)&&t.descriptor.textureCoordinateScaleFactor?e.setUniform2fv("textureCoordinateScaleFactor",t.descriptor.textureCoordinateScaleFactor):e.setUniform2f("textureCoordinateScaleFactor",1,1)}_acquire(e,t){if((0,o.Wi)(e))return void t(null);const i=this._textureRepository.acquire(e);if((0,S.y8)(i))return++this._numLoading,void i.then((e=>{if(this._disposed)return(0,o.RY)(e),void t(null);t(e)})).finally((()=>--this._numLoading));t(i)}}var je,Xe=i(16318);class Je extends N{constructor(e,t){super(),this.type=P.Material,this.supportsEdges=!1,this._visible=!0,this._renderPriority=0,this._insertOrder=0,this._vertexAttributeLocations=we,this._parameters=(0,Xe.Uf)(e,t),this.validateParameters(this._parameters)}dispose(){}get parameters(){return this._parameters}update(e){return!1}setParameters(e){(0,Xe.LO)(this._parameters,e)&&(this.validateParameters(this._parameters),this.parametersChanged())}validateParameters(e){}get visible(){return this._visible}set visible(e){e!==this._visible&&(this._visible=e,this.parametersChanged())}shouldRender(e){return this.isVisible()&&this.isVisibleInPass(e.pass)&&0!=(this.renderOccluded&e.renderOccludedMask)}isVisibleInPass(e){return!0}get renderOccluded(){return this.parameters.renderOccluded}get renderPriority(){return this._renderPriority}set renderPriority(e){e!==this._renderPriority&&(this._renderPriority=e,this.parametersChanged())}get insertOrder(){return this._insertOrder}set insertOrder(e){e!==this._insertOrder&&(this._insertOrder=e,this.parametersChanged())}get vertexAttributeLocations(){return this._vertexAttributeLocations}isVisible(){return this._visible}parametersChanged(){(0,o.pC)(this.repository)&&this.repository.materialChanged(this)}}!function(e){e[e.Occlude=1]="Occlude",e[e.Transparent=2]="Transparent",e[e.OccludeAndTransparent=4]="OccludeAndTransparent",e[e.OccludeAndTransparentStencil=8]="OccludeAndTransparentStencil",e[e.Opaque=16]="Opaque"}(je||(je={}));const Ke={renderOccluded:je.Occlude};var Ye,Ze=i(12045),Qe=i(38208),et=i(46378),tt=i(10663),it=i(61044),rt=i(79912),ot=i(1983),nt=(i(39994),i(88589));!function(e){e[e.X=0]="X",e[e.Y=1]="Y",e[e.Z=2]="Z"}(Ye||(Ye={}));i(7753);var at=i(19480),st=i(28888);function lt(e,t,i){const r=(0,c.d)(e.direction,(0,c.f)(i,t,e.origin));return(0,c.b)(i,e.origin,(0,c.a)(i,e.direction,r)),i}function ct(){return{origin:null,direction:null}}new at.x(ct);function dt(e,t){const i=(0,c.d)(e,t)/((0,c.l)(e)*(0,c.l)(t));return-(0,G.ZF)(i)}(0,d.c)(),(0,d.c)();const ut=A.Z.getLogger("esri.geometry.support.sphere");function ht(){return(0,ot.c)()}function ft(e,t=ht()){return(0,nt.c)(t,e)}function mt(e,t){return(0,ot.f)(e[0],e[1],e[2],t)}function pt(e){return e}function vt(e){e[0]=e[1]=e[2]=e[3]=0}function gt(e){return e}function xt(e){return Array.isArray(e)?e[3]:e}function Tt(e){return Array.isArray(e)?e:Ht}function _t(e,t,i,r){return(0,ot.f)(e,t,i,r)}function bt(e,t,i){return e!==i&&(0,c.g)(i,e),i[3]=e[3]+t,i}function At(e,t,i){return ut.error("sphere.setExtent is not yet supported"),e===i?i:ft(e,i)}function St(e,t,i){if((0,o.Wi)(t))return!1;const{origin:r,direction:n}=t,a=Ct;a[0]=r[0]-e[0],a[1]=r[1]-e[1],a[2]=r[2]-e[2];const s=n[0]*n[0]+n[1]*n[1]+n[2]*n[2],l=2*(n[0]*a[0]+n[1]*a[1]+n[2]*a[2]),c=l*l-4*s*(a[0]*a[0]+a[1]*a[1]+a[2]*a[2]-e[3]*e[3]);if(c<0)return!1;const d=Math.sqrt(c);let u=(-l-d)/(2*s);const h=(-l+d)/(2*s);return(u<0||h<u&&h>0)&&(u=h),!(u<0)&&(i&&(i[0]=r[0]+n[0]*u,i[1]=r[1]+n[1]*u,i[2]=r[2]+n[2]*u),!0)}const Ct=(0,d.c)();function Ot(e,t){return St(e,t,null)}function Mt(e,t,i){if(St(e,t,i))return i;const r=Et(e,t,st.WM.get());return(0,c.b)(i,t.origin,(0,c.a)(st.WM.get(),t.direction,(0,c.i)(t.origin,r)/(0,c.l)(t.direction))),i}function Et(e,t,i){const r=st.WM.get(),o=st.MP.get();(0,c.c)(r,t.origin,t.direction);const n=xt(e);(0,c.c)(i,r,t.origin),(0,c.a)(i,i,1/(0,c.l)(i)*n);const a=Pt(e,t.origin),l=dt(t.origin,i);return(0,s.d)(o,l+a,r),(0,c.m)(i,i,o),i}function yt(e,t,i){return St(e,t,i)?i:(lt(t,Tt(e),i),wt(e,i,i))}function wt(e,t,i){const r=(0,c.f)(st.WM.get(),t,Tt(e)),o=(0,c.a)(st.WM.get(),r,e[3]/(0,c.l)(r));return(0,c.b)(i,o,Tt(e))}function Rt(e,t){const i=(0,c.f)(st.WM.get(),t,Tt(e)),r=(0,c.p)(i),o=e[3]*e[3];return Math.sqrt(Math.abs(r-o))}function Pt(e,t){const i=(0,c.f)(st.WM.get(),t,Tt(e)),r=(0,c.l)(i),o=xt(e),n=o+Math.abs(o-r);return(0,G.ZF)(o/n)}const It=(0,d.c)();function Nt(e,t,i,r){const o=(0,c.f)(It,t,Tt(e));switch(i){case Ye.X:{const e=(0,G.jE)(o,It)[2];return(0,c.s)(r,-Math.sin(e),Math.cos(e),0)}case Ye.Y:{const e=(0,G.jE)(o,It),t=e[1],i=e[2],n=Math.sin(t);return(0,c.s)(r,-n*Math.cos(i),-n*Math.sin(i),Math.cos(t))}case Ye.Z:return(0,c.n)(r,o);default:return}}function Lt(e,t){const i=(0,c.f)(Ft,t,Tt(e));return(0,c.l)(i)-e[3]}function Dt(e,t,i,r){const o=Lt(e,t),n=Nt(e,t,Ye.Z,Ft),a=(0,c.a)(Ft,n,i-o);return(0,c.b)(r,t,a)}const Ht=(0,d.c)(),Ft=(0,d.c)();Object.freeze({__proto__:null,create:ht,copy:ft,fromCenterAndRadius:mt,wrap:pt,clear:vt,fromRadius:gt,getRadius:xt,getCenter:Tt,fromValues:_t,elevate:bt,setExtent:At,intersectRay:St,intersectsRay:Ot,intersectRayClosestSilhouette:Mt,closestPointOnSilhouette:Et,closestPoint:yt,projectPoint:wt,distanceToSilhouette:Rt,angleToSilhouette:Pt,axisAt:Nt,altitudeAt:Lt,setAltitudeAt:Dt});class zt{constructor(e=0){this.offset=e,this.tmpVertex=(0,d.c)()}applyToVertex(e,t,i){const r=e+this.localOrigin[0],o=t+this.localOrigin[1],n=i+this.localOrigin[2],a=this.offset/Math.sqrt(r*r+o*o+n*n);return this.tmpVertex[0]=e+r*a,this.tmpVertex[1]=t+o*a,this.tmpVertex[2]=i+n*a,this.tmpVertex}applyToAabb(e){const t=e[0]+this.localOrigin[0],i=e[1]+this.localOrigin[1],r=e[2]+this.localOrigin[2],o=e[3]+this.localOrigin[0],n=e[4]+this.localOrigin[1],a=e[5]+this.localOrigin[2],s=this.offset/Math.sqrt(t*t+i*i+r*r);e[0]+=t*s,e[1]+=i*s,e[2]+=r*s;const l=this.offset/Math.sqrt(o*o+n*n+a*a);return e[3]+=o*l,e[4]+=n*l,e[5]+=a*l,e}}class Bt{constructor(e=0){this.offset=e,this.componentLocalOriginLength=0,this.tmpVertex=(0,d.c)(),this.mbs=(0,ot.c)(),this.obb={center:(0,d.c)(),halfSize:(0,rt.c)(),quaternion:null}}set localOrigin(e){this.componentLocalOriginLength=Math.sqrt(e[0]*e[0]+e[1]*e[1]+e[2]*e[2])}applyToVertex(e,t,i){const r=e,o=t,n=i+this.componentLocalOriginLength,a=this.offset/Math.sqrt(r*r+o*o+n*n);return this.tmpVertex[0]=e+r*a,this.tmpVertex[1]=t+o*a,this.tmpVertex[2]=i+n*a,this.tmpVertex}applyToAabb(e){const t=e[0],i=e[1],r=e[2]+this.componentLocalOriginLength,o=e[3],n=e[4],a=e[5]+this.componentLocalOriginLength,s=this.offset/Math.sqrt(t*t+i*i+r*r);e[0]+=t*s,e[1]+=i*s,e[2]+=r*s;const l=this.offset/Math.sqrt(o*o+n*n+a*a);return e[3]+=o*l,e[4]+=n*l,e[5]+=a*l,e}applyToMbs(e){const t=Math.sqrt(e[0]*e[0]+e[1]*e[1]+e[2]*e[2]),i=this.offset/t;return this.mbs[0]=e[0]+e[0]*i,this.mbs[1]=e[1]+e[1]*i,this.mbs[2]=e[2]+e[2]*i,this.mbs[3]=e[3]+e[3]*this.offset/t,this.mbs}applyToObb(e){const t=e.center,i=this.offset/Math.sqrt(t[0]*t[0]+t[1]*t[1]+t[2]*t[2]);this.obb.center[0]=t[0]+t[0]*i,this.obb.center[1]=t[1]+t[1]*i,this.obb.center[2]=t[2]+t[2]*i,(0,c.q)(this.obb.halfSize,e.halfSize,e.quaternion),(0,c.b)(this.obb.halfSize,this.obb.halfSize,e.center);const r=this.offset/Math.sqrt(this.obb.halfSize[0]*this.obb.halfSize[0]+this.obb.halfSize[1]*this.obb.halfSize[1]+this.obb.halfSize[2]*this.obb.halfSize[2]);return this.obb.halfSize[0]+=this.obb.halfSize[0]*r,this.obb.halfSize[1]+=this.obb.halfSize[1]*r,this.obb.halfSize[2]+=this.obb.halfSize[2]*r,(0,c.f)(this.obb.halfSize,this.obb.halfSize,e.center),(0,tt.c)(Wt,e.quaternion),(0,c.q)(this.obb.halfSize,this.obb.halfSize,Wt),this.obb.halfSize[0]*=this.obb.halfSize[0]<0?-1:1,this.obb.halfSize[1]*=this.obb.halfSize[1]<0?-1:1,this.obb.halfSize[2]*=this.obb.halfSize[2]<0?-1:1,this.obb.quaternion=e.quaternion,this.obb}}class Ut{constructor(e=0){this.offset=e,this.sphere=ht(),this.tmpVertex=(0,d.c)()}applyToVertex(e,t,i){const r=this.objectTransform.transform;let o=r[0]*e+r[4]*t+r[8]*i+r[12],n=r[1]*e+r[5]*t+r[9]*i+r[13],a=r[2]*e+r[6]*t+r[10]*i+r[14];const s=this.offset/Math.sqrt(o*o+n*n+a*a);o+=o*s,n+=n*s,a+=a*s;const l=this.objectTransform.inverse;return this.tmpVertex[0]=l[0]*o+l[4]*n+l[8]*a+l[12],this.tmpVertex[1]=l[1]*o+l[5]*n+l[9]*a+l[13],this.tmpVertex[2]=l[2]*o+l[6]*n+l[10]*a+l[14],this.tmpVertex}applyToMinMax(e,t){const i=this.offset/Math.sqrt(e[0]*e[0]+e[1]*e[1]+e[2]*e[2]);e[0]+=e[0]*i,e[1]+=e[1]*i,e[2]+=e[2]*i;const r=this.offset/Math.sqrt(t[0]*t[0]+t[1]*t[1]+t[2]*t[2]);t[0]+=t[0]*r,t[1]+=t[1]*r,t[2]+=t[2]*r}applyToAabb(e){const t=this.offset/Math.sqrt(e[0]*e[0]+e[1]*e[1]+e[2]*e[2]);e[0]+=e[0]*t,e[1]+=e[1]*t,e[2]+=e[2]*t;const i=this.offset/Math.sqrt(e[3]*e[3]+e[4]*e[4]+e[5]*e[5]);return e[3]+=e[3]*i,e[4]+=e[4]*i,e[5]+=e[5]*i,e}applyToBoundingSphere(e){const t=Math.sqrt(e[0]*e[0]+e[1]*e[1]+e[2]*e[2]),i=this.offset/t;return this.sphere[0]=e[0]+e[0]*i,this.sphere[1]=e[1]+e[1]*i,this.sphere[2]=e[2]+e[2]*i,this.sphere[3]=e[3]+e[3]*this.offset/t,this.sphere}}const Gt=new Ut;function Vt(e){return(0,o.pC)(e)?(Gt.offset=e,Gt):null}new Bt;new zt;const Wt=(0,it.a)();function kt(e,t,i,r){const o=i.typedBuffer,n=i.typedBufferStride,a=e.length;r*=n;for(let s=0;s<a;++s){const i=2*e[s];o[r]=t[i],o[r+1]=t[i+1],r+=n}}function qt(e,t,i,r,o){const n=i.typedBuffer,a=i.typedBufferStride,s=e.length;if(r*=a,null==o||1===o)for(let l=0;l<s;++l){const i=3*e[l];n[r]=t[i],n[r+1]=t[i+1],n[r+2]=t[i+2],r+=a}else for(let l=0;l<s;++l){const i=3*e[l];for(let e=0;e<o;++e)n[r]=t[i],n[r+1]=t[i+1],n[r+2]=t[i+2],r+=a}}function $t(e,t,i,r,o=1){const n=i.typedBuffer,a=i.typedBufferStride,s=e.length;if(r*=a,1===o)for(let l=0;l<s;++l){const i=4*e[l];n[r]=t[i],n[r+1]=t[i+1],n[r+2]=t[i+2],n[r+3]=t[i+3],r+=a}else for(let l=0;l<s;++l){const i=4*e[l];for(let e=0;e<o;++e)n[r]=t[i],n[r+1]=t[i+1],n[r+2]=t[i+2],n[r+3]=t[i+3],r+=a}}function jt(e,t,i,r,o,n=1){if(!i)return void qt(e,t,r,o,n);const a=r.typedBuffer,s=r.typedBufferStride,l=e.length,c=i[0],d=i[1],u=i[2],h=i[4],f=i[5],m=i[6],p=i[8],v=i[9],g=i[10],x=i[12],T=i[13],_=i[14];if(o*=s,1===n)for(let b=0;b<l;++b){const i=3*e[b],r=t[i],n=t[i+1],l=t[i+2];a[o]=c*r+h*n+p*l+x,a[o+1]=d*r+f*n+v*l+T,a[o+2]=u*r+m*n+g*l+_,o+=s}else for(let b=0;b<l;++b){const i=3*e[b],r=t[i],l=t[i+1],A=t[i+2],S=c*r+h*l+p*A+x,C=d*r+f*l+v*A+T,O=u*r+m*l+g*A+_;for(let e=0;e<n;++e)a[o]=S,a[o+1]=C,a[o+2]=O,o+=s}}function Xt(e,t,i,r,o,n=1){if(!i)return void qt(e,t,r,o,n);const a=i,l=r.typedBuffer,c=r.typedBufferStride,d=e.length,u=a[0],h=a[1],f=a[2],m=a[4],p=a[5],v=a[6],g=a[8],x=a[9],T=a[10],_=!(0,s.p)(a),b=1e-6,A=1-b;if(o*=c,1===n)for(let s=0;s<d;++s){const i=3*e[s],r=t[i],n=t[i+1],a=t[i+2];let d=u*r+m*n+g*a,S=h*r+p*n+x*a,C=f*r+v*n+T*a;if(_){const e=d*d+S*S+C*C;if(e<A&&e>b){const t=1/Math.sqrt(e);d*=t,S*=t,C*=t}}l[o+0]=d,l[o+1]=S,l[o+2]=C,o+=c}else for(let s=0;s<d;++s){const i=3*e[s],r=t[i],a=t[i+1],d=t[i+2];let S=u*r+m*a+g*d,C=h*r+p*a+x*d,O=f*r+v*a+T*d;if(_){const e=S*S+C*C+O*O;if(e<A&&e>b){const t=1/Math.sqrt(e);S*=t,C*=t,O*=t}}for(let e=0;e<n;++e)l[o+0]=S,l[o+1]=C,l[o+2]=O,o+=c}}function Jt(e,t,i,r,o,n=1){if(!i)return void $t(e,t,r,o,n);const a=i,l=r.typedBuffer,c=r.typedBufferStride,d=e.length,u=a[0],h=a[1],f=a[2],m=a[4],p=a[5],v=a[6],g=a[8],x=a[9],T=a[10],_=!(0,s.p)(a),b=1e-6,A=1-b;if(o*=c,1===n)for(let s=0;s<d;++s){const i=4*e[s],r=t[i],n=t[i+1],a=t[i+2],d=t[i+3];let S=u*r+m*n+g*a,C=h*r+p*n+x*a,O=f*r+v*n+T*a;if(_){const e=S*S+C*C+O*O;if(e<A&&e>b){const t=1/Math.sqrt(e);S*=t,C*=t,O*=t}}l[o+0]=S,l[o+1]=C,l[o+2]=O,l[o+3]=d,o+=c}else for(let s=0;s<d;++s){const i=4*e[s],r=t[i],a=t[i+1],d=t[i+2],S=t[i+3];let C=u*r+m*a+g*d,O=h*r+p*a+x*d,M=f*r+v*a+T*d;if(_){const e=C*C+O*O+M*M;if(e<A&&e>b){const t=1/Math.sqrt(e);C*=t,O*=t,M*=t}}for(let e=0;e<n;++e)l[o+0]=C,l[o+1]=O,l[o+2]=M,l[o+3]=S,o+=c}}function Kt(e,t,i,r,o,n=1){const a=r.typedBuffer,s=r.typedBufferStride,l=e.length;if(o*=s,1===n){if(4===i)for(let c=0;c<l;++c){const i=4*e[c];a[o]=t[i],a[o+1]=t[i+1],a[o+2]=t[i+2],a[o+3]=t[i+3],o+=s}else if(3===i)for(let c=0;c<l;++c){const i=3*e[c];a[o]=t[i],a[o+1]=t[i+1],a[o+2]=t[i+2],a[o+3]=255,o+=s}}else if(4===i)for(let c=0;c<l;++c){const i=4*e[c];for(let e=0;e<n;++e)a[o]=t[i],a[o+1]=t[i+1],a[o+2]=t[i+2],a[o+3]=t[i+3],o+=s}else if(3===i)for(let c=0;c<l;++c){const i=3*e[c];for(let e=0;e<n;++e)a[o]=t[i],a[o+1]=t[i+1],a[o+2]=t[i+2],a[o+3]=255,o+=s}}function Yt(e,t,i,r,o,n){for(const a of t.fieldNames){const t=e.vertexAttributes.get(a),s=e.indices.get(a);if(t&&s)switch(a){case D.T.POSITION:{(0,y.hu)(3===t.size);const e=o.getField(a,h.ct);e&&jt(s,t.data,i,e,n);break}case D.T.NORMAL:{(0,y.hu)(3===t.size);const e=o.getField(a,h.ct);e&&Xt(s,t.data,r,e,n);break}case D.T.UV0:{(0,y.hu)(2===t.size);const e=o.getField(a,h.Eu);e&&kt(s,t.data,e,n);break}case D.T.COLOR:{(0,y.hu)(3===t.size||4===t.size);const e=o.getField(a,h.mc);e&&Kt(s,t.data,t.size,e,n);break}case D.T.SYMBOLCOLOR:{(0,y.hu)(3===t.size||4===t.size);const e=o.getField(a,h.mc);e&&Kt(s,t.data,t.size,e,n);break}case D.T.TANGENT:{(0,y.hu)(4===t.size);const e=o.getField(a,h.ek);e&&Jt(s,t.data,r,e,n);break}}}}var Zt=i(36663),Qt=i(5885),ei=i(99163),ti=i(27125),ii=i(82082),ri=i(78183),oi=i(55994),ni=i(73393),ai=i(3864),si=i(20105),li=i(12664),ci=i(5331),di=i(64790);function ui(e,t,i){e.setUniform3f("cameraPosition",i[3]-t[0],i[7]-t[1],i[11]-t[2])}function hi(e,t){e.setUniformMatrix4fv("proj",t)}function fi(e,t,i){(0,s.j)(mi,i,t),e.setUniform3fv("localOrigin",t),e.setUniformMatrix4fv("view",mi)}const mi=(0,di.c)();class pi{constructor(e,t){this._module=e,this._loadModule=t}get(){return this._module}async reload(){return this._module=await this._loadModule(),this._module}}class vi{constructor(e,t,i){this.release=i,t&&(this._config=t.snapshot()),this._program=this.initializeProgram(e),this._pipeline=this.initializePipeline(e)}destroy(){this._program=(0,o.O3)(this._program),this._pipeline=this._config=null}reload(e){(0,o.O3)(this._program),this._program=this.initializeProgram(e),this._pipeline=this.initializePipeline(e)}get program(){return this._program}get key(){return this._config.key}get configuration(){return this._config}bindPass(e,t){}bindMaterial(e,t){}bindDraw(e){}bindPipelineState(e,t=null,i){e.setPipelineState(this.getPipelineState(t,i))}ensureAttributeLocations(e){this.program.assertCompatibleVertexAttributeLocations(e)}get primitiveType(){return J.MX.TRIANGLES}getPipelineState(e,t){return this._pipeline}}class gi{constructor(){this._key="",this._keyDirty=!1,this._parameterBits=this._parameterBits?this._parameterBits.map((()=>0)):[],this._parameterNames||(this._parameterNames=[])}get key(){return this._keyDirty&&(this._keyDirty=!1,this._key=String.fromCharCode.apply(String,this._parameterBits)),this._key}snapshot(){const e=this._parameterNames,t={key:this.key};for(const i of e)t[i]=this[i];return t}}function xi(e={}){return(t,i)=>{var r,o;t._parameterNames=null!=(r=t._parameterNames)?r:[],t._parameterNames.push(i);const n=t._parameterNames.length-1,a=e.count||2,s=Math.ceil(Math.log2(a)),l=null!=(o=t._parameterBits)?o:[0];let c=0;for(;l[c]+s>16;)c++,c>=l.length&&l.push(0);t._parameterBits=l;const d=l[c],u=(1<<s)-1<<d;l[c]+=s,Object.defineProperty(t,i,{get(){return this[n]},set(e){if(this[n]!==e&&(this[n]=e,this._keyDirty=!0,this._parameterBits[c]=this._parameterBits[c]&~u|+e<<d&u,"number"!=typeof e&&"boolean"!=typeof e))throw"Configuration value for "+i+" must be boolean or number, got "+typeof e}})}}var Ti=i(6174);class _i{constructor(e,t,i){this._context=e,this._locations=i,this._textures=new Map,this._freeTextureUnits=new E.Z({deallocator:null}),this._glProgram=e.programCache.acquire(t.generateSource("vertex"),t.generateSource("fragment"),i),this._glProgram.stop=()=>{throw new Error("Wrapped _glProgram used directly")},this._fragmentUniforms=(0,Ti.hZ)()?t.fragmentUniforms.entries:null}dispose(){this._glProgram.dispose()}get glName(){return this._glProgram.glName}get isCompiled(){return this._glProgram.isCompiled}setUniform1b(e,t){this._glProgram.setUniform1i(e,t?1:0)}setUniform1i(e,t){this._glProgram.setUniform1i(e,t)}setUniform1f(e,t){this._glProgram.setUniform1f(e,t)}setUniform1fv(e,t){this._glProgram.setUniform1fv(e,t)}setUniform1iv(e,t){this._glProgram.setUniform1iv(e,t)}setUniform2f(e,t,i){this._glProgram.setUniform2f(e,t,i)}setUniform2fv(e,t){this._glProgram.setUniform2fv(e,t)}setUniform2iv(e,t){this._glProgram.setUniform2iv(e,t)}setUniform3f(e,t,i,r){this._glProgram.setUniform3f(e,t,i,r)}setUniform3fv(e,t){this._glProgram.setUniform3fv(e,t)}setUniform3iv(e,t){this._glProgram.setUniform3iv(e,t)}setUniform4f(e,t,i,r,o){this._glProgram.setUniform4f(e,t,i,r,o)}setUniform4fv(e,t){this._glProgram.setUniform4fv(e,t)}setUniform4iv(e,t){this._glProgram.setUniform4iv(e,t)}setUniformMatrix3fv(e,t){this._glProgram.setUniformMatrix3fv(e,t)}setUniformMatrix4fv(e,t){this._glProgram.setUniformMatrix4fv(e,t)}assertCompatibleVertexAttributeLocations(e){e.locations!==this._locations&&console.error("VertexAttributeLocations are incompatible")}stop(){this._textures.clear(),this._freeTextureUnits.clear()}bindTexture(e,t){if((0,o.Wi)(e)||null==e.glName){const e=this._textures.get(t);return e&&(this._context.bindTexture(null,e.unit),this._freeTextureUnit(e),this._textures.delete(t)),null}let i=this._textures.get(t);return null==i?(i=this._allocTextureUnit(e),this._textures.set(t,i)):i.texture=e,this._context.useProgram(this),this.setUniform1i(t,i.unit),this._context.bindTexture(e,i.unit),i.unit}rebindTextures(){this._context.useProgram(this),this._textures.forEach(((e,t)=>{this._context.bindTexture(e.texture,e.unit),this.setUniform1i(t,e.unit)})),(0,o.pC)(this._fragmentUniforms)&&this._fragmentUniforms.forEach((e=>{if(("sampler2D"===e.type||"samplerCube"===e.type)&&!this._textures.has(e.name))throw new Error(`Texture sampler ${e.name} has no bound texture`)}))}_allocTextureUnit(e){return{texture:e,unit:0===this._freeTextureUnits.length?this._textures.size:this._freeTextureUnits.pop()}}_freeTextureUnit(e){this._freeTextureUnits.push(e.unit)}}J.wb.LESS,J.wb.ALWAYS;const bi={mask:255},Ai={function:{func:J.wb.ALWAYS,ref:M.hU.OutlineVisualElementMask,mask:M.hU.OutlineVisualElementMask},operation:{fail:J.xS.KEEP,zFail:J.xS.KEEP,zPass:J.xS.ZERO}},Si={function:{func:J.wb.ALWAYS,ref:M.hU.OutlineVisualElementMask,mask:M.hU.OutlineVisualElementMask},operation:{fail:J.xS.KEEP,zFail:J.xS.KEEP,zPass:J.xS.REPLACE}};J.wb.EQUAL,M.hU.OutlineVisualElementMask,M.hU.OutlineVisualElementMask,J.xS.KEEP,J.xS.KEEP,J.xS.KEEP,J.wb.NOTEQUAL,M.hU.OutlineVisualElementMask,M.hU.OutlineVisualElementMask,J.xS.KEEP,J.xS.KEEP,J.xS.KEEP;var Ci=i(45584),Oi=i(17346);const Mi=A.Z.getLogger("esri.views.3d.webgl-engine.shaders.DefaultTechnique");class Ei extends vi{initializeProgram(e){const t=Ei.shader.get(),i=this.configuration,r=t.build({oitEnabled:i.transparencyPassType===M.Am.Color,output:i.output,viewingMode:e.viewingMode,receiveShadows:i.receiveShadows,slicePlaneEnabled:i.slicePlaneEnabled,sliceHighlightDisabled:i.sliceHighlightDisabled,sliceEnabledForVertexPrograms:!1,symbolColor:i.symbolColors,vvSize:i.vvSize,vvColor:i.vvColor,vvInstancingEnabled:!0,instanced:i.instanced,instancedColor:i.instancedColor,instancedDoublePrecision:i.instancedDoublePrecision,pbrMode:i.usePBR?i.isSchematic?ai.f7.Schematic:ai.f7.Normal:ai.f7.Disabled,hasMetalnessAndRoughnessTexture:i.hasMetalnessAndRoughnessTexture,hasEmissionTexture:i.hasEmissionTexture,hasOcclusionTexture:i.hasOcclusionTexture,hasNormalTexture:i.hasNormalTexture,hasColorTexture:i.hasColorTexture,hasModelTransformation:i.hasModelTransformation,receiveAmbientOcclusion:i.receiveAmbientOcclusion,useCustomDTRExponentForWater:!1,normalType:i.normalsTypeDerivate?ti.h.ScreenDerivative:ti.h.Attribute,doubleSidedMode:i.doubleSidedMode,vertexTangents:i.vertexTangents,attributeTextureCoordinates:i.hasMetalnessAndRoughnessTexture||i.hasEmissionTexture||i.hasOcclusionTexture||i.hasNormalTexture||i.hasColorTexture?ii.N.Default:ii.N.None,textureAlphaPremultiplied:i.textureAlphaPremultiplied,attributeColor:i.vertexColors,screenSizePerspectiveEnabled:i.screenSizePerspective,verticalOffsetEnabled:i.verticalOffset,offsetBackfaces:i.offsetBackfaces,doublePrecisionRequiresObfuscation:(0,ci.I)(e.rctx),alphaDiscardMode:i.alphaDiscardMode,supportsTextureAtlas:!1,multipassTerrainEnabled:i.multipassTerrainEnabled,cullAboveGround:i.cullAboveGround});return new _i(e.rctx,r,we)}bindPass(e,t){var i,r;hi(this.program,t.camera.projectionMatrix);const n=this.configuration.output;this.configuration.hasModelTransformation&&((0,o.pC)(e.modelTransformation)?this.program.setUniformMatrix4fv("model",e.modelTransformation):Mi.warnOnce("hasModelTransformation true, but no modelTransformation found.")),(this.configuration.output===Ve.H.Depth||t.multipassTerrainEnabled||n===Ve.H.Shadow)&&this.program.setUniform2fv("nearFar",t.camera.nearFar),t.multipassTerrainEnabled&&(this.program.setUniform2fv("inverseViewport",t.inverseViewport),(0,ni.p)(this.program,t)),n===Ve.H.Alpha&&(this.program.setUniform1f("opacity",e.opacity),this.program.setUniform1f("layerOpacity",e.layerOpacity),this.program.setUniform4fv("externalColor",e.externalColor),this.program.setUniform1i("colorMixMode",Xe.FZ[e.colorMixMode])),n===Ve.H.Color?(t.lighting.setUniforms(this.program,!1,t.hasFillLights),this.program.setUniform3fv("ambient",e.ambient),this.program.setUniform3fv("diffuse",e.diffuse),this.program.setUniform4fv("externalColor",e.externalColor),this.program.setUniform1i("colorMixMode",Xe.FZ[e.colorMixMode]),this.program.setUniform1f("opacity",e.opacity),this.program.setUniform1f("layerOpacity",e.layerOpacity),this.configuration.usePBR&&(0,ai.nW)(this.program,e,this.configuration.isSchematic)):n===Ve.H.Highlight&&(0,oi.wW)(this.program,t),(0,li.uj)(this.program,e),(0,ri.Mo)(this.program,e,t),(0,Xe.bj)(e.screenSizePerspective,this.program,"screenSizePerspectiveAlignment"),e.textureAlphaMode!==M.JJ.Mask&&e.textureAlphaMode!==M.JJ.MaskBlend||this.program.setUniform1f("textureAlphaCutoff",e.textureAlphaCutoff),null==(i=t.shadowMap)||i.bind(this.program),null==(r=t.ssaoHelper)||r.bind(this.program,t.camera)}bindDraw(e){const t=this.configuration.instancedDoublePrecision?(0,d.f)(e.camera.viewInverseTransposeMatrix[3],e.camera.viewInverseTransposeMatrix[7],e.camera.viewInverseTransposeMatrix[11]):e.origin;fi(this.program,t,e.camera.viewMatrix),this.program.rebindTextures(),(this.configuration.output===Ve.H.Color||this.configuration.output===Ve.H.Alpha||this.configuration.output===Ve.H.Depth&&this.configuration.screenSizePerspective||this.configuration.output===Ve.H.Normal&&this.configuration.screenSizePerspective||this.configuration.output===Ve.H.Highlight&&this.configuration.screenSizePerspective)&&ui(this.program,t,e.camera.viewInverseTransposeMatrix),this.configuration.output===Ve.H.Normal&&this.program.setUniformMatrix4fv("viewNormal",e.camera.viewInverseTransposeMatrix),this.configuration.instancedDoublePrecision&&(0,ei.d3)(this.program,t),(0,Qt.Vv)(this.program,this.configuration,e.slicePlane,{origin:t}),this.configuration.output===Ve.H.Color&&(0,si.vL)(this.program,e,t)}_convertDepthTestFunction(e){return e===M.Gv.Lequal?J.wb.LEQUAL:J.wb.LESS}_setPipeline(e,t){const i=this.configuration,r=e===M.Am.NONE,o=e===M.Am.FrontFace;return(0,Oi.sm)({blending:i.output!==Ve.H.Color&&i.output!==Ve.H.Alpha||!i.transparent?null:r?Ze.wu:(0,Ze.j7)(e),culling:yi(i)&&(0,Oi.zp)(i.cullFace),depthTest:{func:(0,Ze.Bh)(e,this._convertDepthTestFunction(i.customDepthTest))},depthWrite:r||o?i.writeDepth&&Oi.LZ:null,colorWrite:Oi.BK,stencilWrite:i.sceneHasOcludees?bi:null,stencilTest:i.sceneHasOcludees?t?Si:Ai:null,polygonOffset:r||o?null:(0,Ze.je)(i.enableOffset)})}initializePipeline(){return this._occludeePipelineState=this._setPipeline(this.configuration.transparencyPassType,!0),this._setPipeline(this.configuration.transparencyPassType,!1)}getPipelineState(e,t){return t?this._occludeePipelineState:super.getPipelineState(e,t)}}function yi(e){return e.cullFace?e.cullFace!==M.Vr.None:!e.slicePlaneEnabled&&!e.transparent&&!e.doubleSidedMode}Ei.shader=new pi(Ci.D,(()=>i.e(1180).then(i.bind(i,1180))));class wi extends gi{constructor(){super(...arguments),this.output=Ve.H.Color,this.alphaDiscardMode=M.JJ.Opaque,this.doubleSidedMode=We.q.None,this.isSchematic=!1,this.vertexColors=!1,this.offsetBackfaces=!1,this.symbolColors=!1,this.vvSize=!1,this.vvColor=!1,this.verticalOffset=!1,this.receiveShadows=!1,this.slicePlaneEnabled=!1,this.sliceHighlightDisabled=!1,this.receiveAmbientOcclusion=!1,this.screenSizePerspective=!1,this.textureAlphaPremultiplied=!1,this.hasColorTexture=!1,this.usePBR=!1,this.hasMetalnessAndRoughnessTexture=!1,this.hasEmissionTexture=!1,this.hasOcclusionTexture=!1,this.hasNormalTexture=!1,this.instanced=!1,this.instancedColor=!1,this.instancedDoublePrecision=!1,this.vertexTangents=!1,this.normalsTypeDerivate=!1,this.writeDepth=!0,this.sceneHasOcludees=!1,this.transparent=!1,this.enableOffset=!0,this.cullFace=M.Vr.None,this.transparencyPassType=M.Am.NONE,this.multipassTerrainEnabled=!1,this.cullAboveGround=!1,this.hasModelTransformation=!1,this.customDepthTest=M.Gv.Less}}(0,Zt._)([xi({count:Ve.H.COUNT})],wi.prototype,"output",void 0),(0,Zt._)([xi({count:M.JJ.COUNT})],wi.prototype,"alphaDiscardMode",void 0),(0,Zt._)([xi({count:We.q.COUNT})],wi.prototype,"doubleSidedMode",void 0),(0,Zt._)([xi()],wi.prototype,"isSchematic",void 0),(0,Zt._)([xi()],wi.prototype,"vertexColors",void 0),(0,Zt._)([xi()],wi.prototype,"offsetBackfaces",void 0),(0,Zt._)([xi()],wi.prototype,"symbolColors",void 0),(0,Zt._)([xi()],wi.prototype,"vvSize",void 0),(0,Zt._)([xi()],wi.prototype,"vvColor",void 0),(0,Zt._)([xi()],wi.prototype,"verticalOffset",void 0),(0,Zt._)([xi()],wi.prototype,"receiveShadows",void 0),(0,Zt._)([xi()],wi.prototype,"slicePlaneEnabled",void 0),(0,Zt._)([xi()],wi.prototype,"sliceHighlightDisabled",void 0),(0,Zt._)([xi()],wi.prototype,"receiveAmbientOcclusion",void 0),(0,Zt._)([xi()],wi.prototype,"screenSizePerspective",void 0),(0,Zt._)([xi()],wi.prototype,"textureAlphaPremultiplied",void 0),(0,Zt._)([xi()],wi.prototype,"hasColorTexture",void 0),(0,Zt._)([xi()],wi.prototype,"usePBR",void 0),(0,Zt._)([xi()],wi.prototype,"hasMetalnessAndRoughnessTexture",void 0),(0,Zt._)([xi()],wi.prototype,"hasEmissionTexture",void 0),(0,Zt._)([xi()],wi.prototype,"hasOcclusionTexture",void 0),(0,Zt._)([xi()],wi.prototype,"hasNormalTexture",void 0),(0,Zt._)([xi()],wi.prototype,"instanced",void 0),(0,Zt._)([xi()],wi.prototype,"instancedColor",void 0),(0,Zt._)([xi()],wi.prototype,"instancedDoublePrecision",void 0),(0,Zt._)([xi()],wi.prototype,"vertexTangents",void 0),(0,Zt._)([xi()],wi.prototype,"normalsTypeDerivate",void 0),(0,Zt._)([xi()],wi.prototype,"writeDepth",void 0),(0,Zt._)([xi()],wi.prototype,"sceneHasOcludees",void 0),(0,Zt._)([xi()],wi.prototype,"transparent",void 0),(0,Zt._)([xi()],wi.prototype,"enableOffset",void 0),(0,Zt._)([xi({count:M.Vr.COUNT})],wi.prototype,"cullFace",void 0),(0,Zt._)([xi({count:M.Am.COUNT})],wi.prototype,"transparencyPassType",void 0),(0,Zt._)([xi()],wi.prototype,"multipassTerrainEnabled",void 0),(0,Zt._)([xi()],wi.prototype,"cullAboveGround",void 0),(0,Zt._)([xi()],wi.prototype,"hasModelTransformation",void 0),(0,Zt._)([xi({count:M.Gv.COUNT})],wi.prototype,"customDepthTest",void 0);var Ri=i(60926);class Pi extends Ei{initializeProgram(e){const t=Pi.shader.get(),i=this.configuration,r=t.build({oitEnabled:i.transparencyPassType===M.Am.Color,output:i.output,viewingMode:e.viewingMode,receiveShadows:i.receiveShadows,slicePlaneEnabled:i.slicePlaneEnabled,sliceHighlightDisabled:i.sliceHighlightDisabled,sliceEnabledForVertexPrograms:!1,symbolColor:i.symbolColors,vvSize:i.vvSize,vvColor:i.vvColor,vvInstancingEnabled:!0,instanced:i.instanced,instancedColor:i.instancedColor,instancedDoublePrecision:i.instancedDoublePrecision,pbrMode:i.usePBR?ai.f7.Normal:ai.f7.Disabled,hasMetalnessAndRoughnessTexture:!1,hasEmissionTexture:!1,hasOcclusionTexture:!1,hasNormalTexture:!1,hasColorTexture:i.hasColorTexture,hasModelTransformation:!1,receiveAmbientOcclusion:i.receiveAmbientOcclusion,useCustomDTRExponentForWater:!1,normalType:ti.h.Attribute,doubleSidedMode:We.q.WindingOrder,vertexTangents:!1,attributeTextureCoordinates:i.hasColorTexture?ii.N.Default:ii.N.None,textureAlphaPremultiplied:i.textureAlphaPremultiplied,attributeColor:i.vertexColors,screenSizePerspectiveEnabled:i.screenSizePerspective,verticalOffsetEnabled:i.verticalOffset,offsetBackfaces:i.offsetBackfaces,doublePrecisionRequiresObfuscation:(0,ci.I)(e.rctx),alphaDiscardMode:i.alphaDiscardMode,supportsTextureAtlas:!1,multipassTerrainEnabled:i.multipassTerrainEnabled,cullAboveGround:i.cullAboveGround});return new _i(e.rctx,r,we)}}Pi.shader=new pi(Ri.R,(()=>i.e(7578).then(i.bind(i,67578))));class Ii extends Je{constructor(e){super(e,Li),this.supportsEdges=!0,this.techniqueConfig=new wi,this.vertexBufferLayout=Hi(this.parameters),this.instanceBufferLayout=e.instanced?Fi(this.parameters):null}isVisibleInPass(e){return e!==Qe.C.MATERIAL_DEPTH_SHADOWMAP_ALL&&e!==Qe.C.MATERIAL_DEPTH_SHADOWMAP_DEFAULT&&e!==Qe.C.MATERIAL_DEPTH_SHADOWMAP_HIGHLIGHT||this.parameters.castShadows}isVisible(){const e=this.parameters;if(!super.isVisible()||0===e.layerOpacity)return!1;const t=e.instanced,i=e.vertexColors,r=e.symbolColors,o=!!t&&t.indexOf("color")>-1,n=e.vvColorEnabled,a="replace"===e.colorMixMode,s=e.opacity>0,l=e.externalColor&&e.externalColor[3]>0;return i&&(o||n||r)?!!a||s:i?a?l:s:o||n||r?!!a||s:a?l:s}getTechniqueConfig(e,t){return this.techniqueConfig.output=e,this.techniqueConfig.hasNormalTexture=!!this.parameters.normalTextureId,this.techniqueConfig.hasColorTexture=!!this.parameters.textureId,this.techniqueConfig.vertexTangents=this.parameters.vertexTangents,this.techniqueConfig.instanced=!!this.parameters.instanced,this.techniqueConfig.instancedDoublePrecision=this.parameters.instancedDoublePrecision,this.techniqueConfig.vvSize=this.parameters.vvSizeEnabled,this.techniqueConfig.verticalOffset=null!==this.parameters.verticalOffset,this.techniqueConfig.screenSizePerspective=null!==this.parameters.screenSizePerspective,this.techniqueConfig.slicePlaneEnabled=this.parameters.slicePlaneEnabled,this.techniqueConfig.sliceHighlightDisabled=this.parameters.sliceHighlightDisabled,this.techniqueConfig.alphaDiscardMode=this.parameters.textureAlphaMode,this.techniqueConfig.normalsTypeDerivate="screenDerivative"===this.parameters.normals,this.techniqueConfig.transparent=this.parameters.transparent,this.techniqueConfig.writeDepth=this.parameters.writeDepth,(0,o.pC)(this.parameters.customDepthTest)&&(this.techniqueConfig.customDepthTest=this.parameters.customDepthTest),this.techniqueConfig.sceneHasOcludees=this.parameters.sceneHasOcludees,this.techniqueConfig.cullFace=this.parameters.slicePlaneEnabled?M.Vr.None:this.parameters.cullFace,this.techniqueConfig.multipassTerrainEnabled=t.multipassTerrainEnabled,this.techniqueConfig.cullAboveGround=t.cullAboveGround,this.techniqueConfig.hasModelTransformation=(0,o.pC)(this.parameters.modelTransformation),e!==Ve.H.Color&&e!==Ve.H.Alpha||(this.techniqueConfig.vertexColors=this.parameters.vertexColors,this.techniqueConfig.symbolColors=this.parameters.symbolColors,this.parameters.treeRendering?this.techniqueConfig.doubleSidedMode=We.q.WindingOrder:this.techniqueConfig.doubleSidedMode=this.parameters.doubleSided&&"normal"===this.parameters.doubleSidedType?We.q.View:this.parameters.doubleSided&&"winding-order"===this.parameters.doubleSidedType?We.q.WindingOrder:We.q.None,this.techniqueConfig.instancedColor=!!this.parameters.instanced&&this.parameters.instanced.indexOf("color")>-1,this.techniqueConfig.receiveShadows=this.parameters.receiveShadows&&this.parameters.shadowMappingEnabled,this.techniqueConfig.receiveAmbientOcclusion=!!t.ssaoEnabled&&this.parameters.receiveSSAO,this.techniqueConfig.vvColor=this.parameters.vvColorEnabled,this.techniqueConfig.textureAlphaPremultiplied=!!this.parameters.textureAlphaPremultiplied,this.techniqueConfig.usePBR=this.parameters.usePBR,this.techniqueConfig.hasMetalnessAndRoughnessTexture=!!this.parameters.metallicRoughnessTextureId,this.techniqueConfig.hasEmissionTexture=!!this.parameters.emissiveTextureId,this.techniqueConfig.hasOcclusionTexture=!!this.parameters.occlusionTextureId,this.techniqueConfig.offsetBackfaces=!(!this.parameters.transparent||!this.parameters.offsetTransparentBackfaces),this.techniqueConfig.isSchematic=this.parameters.usePBR&&this.parameters.isSchematic,this.techniqueConfig.transparencyPassType=t.transparencyPassType,this.techniqueConfig.enableOffset=t.camera.relativeElevation<Ze.ve),this.techniqueConfig}intersect(e,t,i,r,o,n,a){if(null!==this.parameters.verticalOffset){const e=r.camera;(0,c.s)(Wi,i[12],i[13],i[14]);let t=null;switch(r.viewingMode){case Ue.JY.Global:t=(0,c.n)(Gi,Wi);break;case Ue.JY.Local:t=(0,c.g)(Gi,Ui)}let a=0;if(null!==this.parameters.verticalOffset){const i=(0,c.f)(ki,Wi,e.eye),r=(0,c.l)(i),o=(0,c.a)(i,i,1/r);let n=null;this.parameters.screenSizePerspective&&(n=(0,c.d)(t,o)),a+=(0,Xe.Hx)(e,r,this.parameters.verticalOffset,n,this.parameters.screenSizePerspective)}(0,c.a)(t,t,a),(0,c.t)(Vi,t,r.transform.inverseRotation),o=(0,c.f)(zi,o,Vi),n=(0,c.f)(Bi,n,Vi)}(0,Xe.Bw)(e,t,r,o,n,Vt(r.verticalOffset),a)}requiresSlot(e){return e===(this.parameters.transparent?this.parameters.writeDepth?et.r.TRANSPARENT_MATERIAL:et.r.TRANSPARENT_DEPTH_WRITE_DISABLED_MATERIAL:et.r.OPAQUE_MATERIAL)||e===et.r.DRAPED_MATERIAL}createGLMaterial(e){return e.output===Ve.H.Color||e.output===Ve.H.Alpha||e.output===Ve.H.Depth||e.output===Ve.H.Normal||e.output===Ve.H.Shadow||e.output===Ve.H.Highlight?new Ni(e):null}createBufferWriter(){return new Di(this.vertexBufferLayout,this.instanceBufferLayout)}}class Ni extends $e{constructor(e){super({...e,...e.material.parameters})}updateParameters(e){const t=this._material.parameters;return this.updateTexture(t.textureId),this.ensureTechnique(t.treeRendering?Pi:Ei,e)}_updateShadowState(e){e.shadowMappingEnabled!==this._material.parameters.shadowMappingEnabled&&this._material.setParameters({shadowMappingEnabled:e.shadowMappingEnabled})}_updateOccludeeState(e){e.hasOccludees!==this._material.parameters.sceneHasOcludees&&this._material.setParameters({sceneHasOcludees:e.hasOccludees})}beginSlot(e){return this._output!==Ve.H.Color&&this._output!==Ve.H.Alpha||(this._updateShadowState(e),this._updateOccludeeState(e)),this.updateParameters(e)}bind(e,t){t.bindPass(this._material.parameters,e),this.bindTextures(t.program)}}const Li={textureId:void 0,initTextureTransparent:!1,isSchematic:!1,usePBR:!1,normalTextureId:void 0,vertexTangents:!1,occlusionTextureId:void 0,emissiveTextureId:void 0,metallicRoughnessTextureId:void 0,emissiveFactor:[0,0,0],mrrFactors:[0,1,.5],ambient:[.2,.2,.2],diffuse:[.8,.8,.8],externalColor:[1,1,1,1],colorMixMode:"multiply",opacity:1,layerOpacity:1,vertexColors:!1,symbolColors:!1,doubleSided:!1,doubleSidedType:"normal",cullFace:M.Vr.Back,instanced:void 0,instancedDoublePrecision:!1,normals:"default",receiveSSAO:!0,fillLightsEnabled:!0,receiveShadows:!0,castShadows:!0,shadowMappingEnabled:!1,verticalOffset:null,screenSizePerspective:null,slicePlaneEnabled:!1,sliceHighlightDisabled:!1,offsetTransparentBackfaces:!1,vvSizeEnabled:!1,vvSizeMinSize:[1,1,1],vvSizeMaxSize:[100,100,100],vvSizeOffset:[0,0,0],vvSizeFactor:[1,1,1],vvSizeValue:[1,1,1],vvColorEnabled:!1,vvColorValues:[0,0,0,0,0,0,0,0],vvColorColors:[1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0],vvSymbolAnchor:[0,0,0],vvSymbolRotationMatrix:(0,a.c)(),modelTransformation:null,transparent:!1,writeDepth:!0,customDepthTest:M.Gv.Less,textureAlphaMode:M.JJ.Blend,textureAlphaCutoff:ke.F,textureAlphaPremultiplied:!1,sceneHasOcludees:!1,...Ke};class Di{constructor(e,t){this.vertexBufferLayout=e,this.instanceBufferLayout=t}allocate(e){return this.vertexBufferLayout.createBuffer(e)}elementCount(e){return e.indices.get(D.T.POSITION).length}write(e,t,i,r){Yt(t,this.vertexBufferLayout,e.transformation,e.invTranspTransformation,i,r)}}function Hi(e){const t=e.textureId||e.normalTextureId||e.metallicRoughnessTextureId||e.emissiveTextureId||e.occlusionTextureId,i=(0,Ge.U$)().vec3f(D.T.POSITION).vec3f(D.T.NORMAL);return e.vertexTangents&&i.vec4f(D.T.TANGENT),t&&i.vec2f(D.T.UV0),e.vertexColors&&i.vec4u8(D.T.COLOR),e.symbolColors&&i.vec4u8(D.T.SYMBOLCOLOR),i}function Fi(e){let t=(0,Ge.U$)();return t=e.instancedDoublePrecision?t.vec3f(D.T.MODELORIGINHI).vec3f(D.T.MODELORIGINLO).mat3f(D.T.MODEL).mat3f(D.T.MODELNORMAL):t.mat4f(D.T.MODEL).mat4f(D.T.MODELNORMAL),e.instanced&&e.instanced.indexOf("color")>-1&&(t=t.vec4f(D.T.INSTANCECOLOR)),e.instanced&&e.instanced.indexOf("featureAttribute")>-1&&(t=t.vec4f(D.T.INSTANCEFEATUREATTRIBUTE)),t}const zi=(0,d.c)(),Bi=(0,d.c)(),Ui=(0,d.f)(0,0,1),Gi=(0,d.c)(),Vi=(0,d.c)(),Wi=(0,d.c)(),ki=(0,d.c)(),qi=A.Z.getLogger("esri.views.3d.layers.graphics.objectResourceUtils");async function $i(e,t){const i=await ji(e,t);return{resource:i,textures:await Qi(i.textureDefinitions,t)}}async function ji(e,t){const i=(0,o.pC)(t)&&t.streamDataRequester;if(i)return Xi(e,i,t);const r=await(0,_.q6)((0,T["default"])(e,(0,o.Wg)(t)));if(!0===r.ok)return r.value.data;(0,S.r9)(r.error),Ji(r.error)}async function Xi(e,t,i){const r=await(0,_.q6)(t.request(e,"json",i));if(!0===r.ok)return r.value;(0,S.r9)(r.error),Ji(r.error.details.url)}function Ji(e){throw new b.Z("",`Request for object resource failed: ${e}`)}function Ki(e){const t=e.params,i=t.topology;let r=!0;switch(t.vertexAttributes||(qi.warn("Geometry must specify vertex attributes"),r=!1),t.topology){case"PerAttributeArray":break;case"Indexed":case null:case void 0:{const e=t.faces;if(e){if(t.vertexAttributes)for(const i in t.vertexAttributes){const t=e[i];t&&t.values?(null!=t.valueType&&"UInt32"!==t.valueType&&(qi.warn(`Unsupported indexed geometry indices type '${t.valueType}', only UInt32 is currently supported`),r=!1),null!=t.valuesPerElement&&1!==t.valuesPerElement&&(qi.warn(`Unsupported indexed geometry values per element '${t.valuesPerElement}', only 1 is currently supported`),r=!1)):(qi.warn(`Indexed geometry does not specify face indices for '${i}' attribute`),r=!1)}}else qi.warn("Indexed geometries must specify faces"),r=!1;break}default:qi.warn(`Unsupported topology '${i}'`),r=!1}e.params.material||(qi.warn("Geometry requires material"),r=!1);const o=e.params.vertexAttributes;for(const n in o)o[n].values||(qi.warn("Geometries with externally defined attributes are not yet supported"),r=!1);return r}function Yi(e,t){const i=[],r=[],n=[],a=[],s=e.resource,l=C.G.parse(s.version||"1.0","wosr");ir.validate(l);const c=s.model.name,u=s.model.geometries,h=s.materialDefinitions,f=e.textures;let m=0;const p=new Map;for(let v=0;v<u.length;v++){const e=u[v];if(!Ki(e))continue;const s=tr(e),l=e.params.vertexAttributes,c=[];for(const t in l){const e=l[t],i=e.values;c.push([t,{data:i,size:e.valuesPerElement,exclusive:!0}])}const g=[];if("PerAttributeArray"!==e.params.topology){const t=e.params.faces;for(const e in t)g.push([e,new Uint32Array(t[e].values)])}const x=f&&f[s.texture];if(x&&!p.has(s.texture)){const{image:e,params:t}=x,i=new Be(e,t);a.push(i),p.set(s.texture,i)}const T=p.get(s.texture),_=T?T.id:void 0;let b=n[s.material]?n[s.material][s.texture]:null;if(!b){const e=h[s.material.substring(s.material.lastIndexOf("/")+1)].params;1===e.transparency&&(e.transparency=0);const i=x&&x.alphaChannelUsage,r=e.transparency>0||"transparency"===i||"maskAndTransparency"===i,a=x?er(x.alphaChannelUsage):void 0,l={ambient:(0,d.d)(e.diffuse),diffuse:(0,d.d)(e.diffuse),opacity:1-(e.transparency||0),transparent:r,textureAlphaMode:a,textureAlphaCutoff:.33,textureId:_,initTextureTransparent:!0,doubleSided:!0,cullFace:M.Vr.None,colorMixMode:e.externalColorMixMode||"tint",textureAlphaPremultiplied:!!x&&!!x.params.preMultiplyAlpha};(0,o.pC)(t)&&t.materialParamsMixin&&Object.assign(l,t.materialParamsMixin),b=new Ii(l),n[s.material]||(n[s.material]={}),n[s.material][s.texture]=b}r.push(b);const A=new H(c,g);m+=g.position?g.position.length:0,i.push(A)}return{name:c,stageResources:{textures:a,materials:r,geometries:i},pivotOffset:s.model.pivotOffset,boundingBox:Zi(i),numberOfVertices:m,lodThreshold:null}}function Zi(e){const t=(0,u.cS)();return e.forEach((e=>{const i=e.boundingInfo;(0,o.pC)(i)&&((0,u.pp)(t,i.getBBMin()),(0,u.pp)(t,i.getBBMax()))})),t}async function Qi(e,t){const i=[];for(const a in e){const r=e[a],n=r.images[0].data;if(!n){qi.warn("Externally referenced texture data is not yet supported");continue}const s=r.encoding+";base64,"+n,l="/textureDefinitions/"+a,c="rgba"===r.channels?r.alphaChannelUsage||"transparency":"none",d={noUnpackFlip:!0,wrap:{s:J.e8.REPEAT,t:J.e8.REPEAT},preMultiplyAlpha:er(c)!==M.JJ.Opaque},u=(0,o.pC)(t)&&t.disableTextures?Promise.resolve(null):(0,O.t)(s,t);i.push(u.then((e=>({refId:l,image:e,params:d,alphaChannelUsage:c}))))}const r=await Promise.all(i),n={};for(const o of r)n[o.refId]=o;return n}function er(e){switch(e){case"mask":return M.JJ.Mask;case"maskAndTransparency":return M.JJ.MaskBlend;case"none":return M.JJ.Opaque;default:return M.JJ.Blend}}function tr(e){const t=e.params;return{id:1,material:t.material,texture:t.texture,region:t.texture}}const ir=new C.G(1,2,"wosr");var rr=i(14634),or=i(385),nr=i(32101),ar=i(14789);async function sr(e,t){const i=lr((0,r.pJ)(e));if("wosr"===i.fileType){const e=await(t.cache?t.cache.loadWOSR(i.url,t):$i(i.url,t)),r=Yi(e,t);return{lods:[r],referenceBoundingBox:r.boundingBox,isEsriSymbolResource:!1,isWosr:!0,remove:e.remove}}const n=await(t.cache?t.cache.loadGLTF(i.url,t,t.usePBR):(0,g.z)(new v.C(t.streamDataRequester),i.url,t,t.usePBR)),a=(0,o.U2)(n.model.meta,"ESRI_proxyEllipsoid");n.meta.isEsriSymbolResource&&(0,o.pC)(a)&&-1!==n.meta.uri.indexOf("/RealisticTrees/")&&hr(n,a);const s=n.meta.isEsriSymbolResource?{usePBR:t.usePBR,isSchematic:!1,treeRendering:n.customMeta.esriTreeRendering,mrrFactors:[0,1,.2]}:{usePBR:t.usePBR,isSchematic:!1,mrrFactors:[0,1,.5]},l={...t.materialParamsMixin,treeRendering:n.customMeta.esriTreeRendering};if(null!=i.specifiedLodIndex){const e=cr(n,s,l,i.specifiedLodIndex);let t=e[0].boundingBox;return 0!==i.specifiedLodIndex&&(t=cr(n,s,l,0)[0].boundingBox),{lods:e,referenceBoundingBox:t,isEsriSymbolResource:n.meta.isEsriSymbolResource,isWosr:!1,remove:n.remove}}const c=cr(n,s,l);return{lods:c,referenceBoundingBox:c[0].boundingBox,isEsriSymbolResource:n.meta.isEsriSymbolResource,isWosr:!1,remove:n.remove}}function lr(e){const t=e.match(/(.*\.(gltf|glb))(\?lod=([0-9]+))?$/);return t?{fileType:"gltf",url:t[1],specifiedLodIndex:null!=t[4]?Number(t[4]):null}:e.match(/(.*\.(json|json\.gz))$/)?{fileType:"wosr",url:e,specifiedLodIndex:null}:{fileType:"unknown",url:e,specifiedLodIndex:null}}function cr(e,t,i,r){const s=e.model,l=(0,a.c)(),c=new Array,d=new Map,v=new Map;return s.lods.forEach(((e,a)=>{if(void 0!==r&&a!==r)return;const g={name:e.name,stageResources:{textures:new Array,materials:new Array,geometries:new Array},lodThreshold:(0,o.pC)(e.lodThreshold)?e.lodThreshold:null,pivotOffset:[0,0,0],numberOfVertices:0,boundingBox:(0,u.cS)()};c.push(g),e.parts.forEach((e=>{const r=e.material+(e.attributes.normal?"_normal":"")+(e.attributes.color?"_color":"")+(e.attributes.texCoord0?"_texCoord0":"")+(e.attributes.tangent?"_tangent":""),a=s.materials.get(e.material),c=(0,o.pC)(e.attributes.texCoord0),x=(0,o.pC)(e.attributes.normal),T=dr(a.alphaMode);if(!d.has(r)){if(c){if((0,o.pC)(a.textureColor)&&!v.has(a.textureColor)){const e=s.textures.get(a.textureColor),t={...e.parameters,preMultiplyAlpha:T!==M.JJ.Opaque};v.set(a.textureColor,new Be(e.data,t))}if((0,o.pC)(a.textureNormal)&&!v.has(a.textureNormal)){const e=s.textures.get(a.textureNormal);v.set(a.textureNormal,new Be(e.data,e.parameters))}if((0,o.pC)(a.textureOcclusion)&&!v.has(a.textureOcclusion)){const e=s.textures.get(a.textureOcclusion);v.set(a.textureOcclusion,new Be(e.data,e.parameters))}if((0,o.pC)(a.textureEmissive)&&!v.has(a.textureEmissive)){const e=s.textures.get(a.textureEmissive);v.set(a.textureEmissive,new Be(e.data,e.parameters))}if((0,o.pC)(a.textureMetallicRoughness)&&!v.has(a.textureMetallicRoughness)){const e=s.textures.get(a.textureMetallicRoughness);v.set(a.textureMetallicRoughness,new Be(e.data,e.parameters))}}const n=a.color[0]**(1/rr.K),l=a.color[1]**(1/rr.K),u=a.color[2]**(1/rr.K),h=a.emissiveFactor[0]**(1/rr.K),f=a.emissiveFactor[1]**(1/rr.K),m=a.emissiveFactor[2]**(1/rr.K),p=(0,o.pC)(a.textureColor)&&c?v.get(a.textureColor):null;d.set(r,new Ii({...t,transparent:T===M.JJ.Blend,customDepthTest:M.Gv.Lequal,textureAlphaMode:T,textureAlphaCutoff:a.alphaCutoff,diffuse:[n,l,u],ambient:[n,l,u],opacity:a.opacity,doubleSided:a.doubleSided,doubleSidedType:"winding-order",cullFace:a.doubleSided?M.Vr.None:M.Vr.Back,vertexColors:!!e.attributes.color,vertexTangents:!!e.attributes.tangent,normals:x?"default":"screenDerivative",castShadows:!0,receiveSSAO:!0,fillLightsEnabled:!0,textureId:(0,o.pC)(p)?p.id:void 0,colorMixMode:a.colorMixMode,normalTextureId:(0,o.pC)(a.textureNormal)&&c?v.get(a.textureNormal).id:void 0,textureAlphaPremultiplied:(0,o.pC)(p)&&!!p.params.preMultiplyAlpha,occlusionTextureId:(0,o.pC)(a.textureOcclusion)&&c?v.get(a.textureOcclusion).id:void 0,emissiveTextureId:(0,o.pC)(a.textureEmissive)&&c?v.get(a.textureEmissive).id:void 0,metallicRoughnessTextureId:(0,o.pC)(a.textureMetallicRoughness)&&c?v.get(a.textureMetallicRoughness).id:void 0,emissiveFactor:[h,f,m],mrrFactors:[a.metallicFactor,a.roughnessFactor,t.mrrFactors[2]],isSchematic:!1,...i}))}const _=ur(e.indices||e.attributes.position.count,e.primitiveType),b=e.attributes.position.count,A=(0,p.gS)(h.ct,b);(0,f.t)(A,e.attributes.position,e.transform);const S=[[D.T.POSITION,{data:A.typedBuffer,size:A.elementCount,exclusive:!0}]],C=[[D.T.POSITION,_]];if((0,o.pC)(e.attributes.normal)){const t=(0,p.gS)(h.ct,b);(0,n.a)(l,e.transform),(0,f.a)(t,e.attributes.normal,l),S.push([D.T.NORMAL,{data:t.typedBuffer,size:t.elementCount,exclusive:!0}]),C.push([D.T.NORMAL,_])}if((0,o.pC)(e.attributes.tangent)){const t=(0,p.gS)(h.ek,b);(0,n.a)(l,e.transform),(0,m.t)(t,e.attributes.tangent,l),S.push([D.T.TANGENT,{data:t.typedBuffer,size:t.elementCount,exclusive:!0}]),C.push([D.T.TANGENT,_])}if((0,o.pC)(e.attributes.texCoord0)){const t=(0,p.gS)(h.Eu,b);(0,or.n)(t,e.attributes.texCoord0),S.push([D.T.UV0,{data:t.typedBuffer,size:t.elementCount,exclusive:!0}]),C.push([D.T.UV0,_])}if((0,o.pC)(e.attributes.color)){const t=(0,p.gS)(h.mc,b);if(4===e.attributes.color.elementCount)e.attributes.color instanceof h.ek?(0,m.s)(t,e.attributes.color,255):e.attributes.color instanceof h.mc?(0,nr.c)(t,e.attributes.color):e.attributes.color instanceof h.v6&&(0,m.s)(t,e.attributes.color,1/256);else{(0,nr.f)(t,255,255,255,255);const i=new h.ne(t.buffer,0,4);e.attributes.color instanceof h.ct?(0,f.s)(i,e.attributes.color,255):e.attributes.color instanceof h.ne?(0,ar.c)(i,e.attributes.color):e.attributes.color instanceof h.mw&&(0,f.s)(i,e.attributes.color,1/256)}S.push([D.T.COLOR,{data:t.typedBuffer,size:t.elementCount,exclusive:!0}]),C.push([D.T.COLOR,_])}const O=new H(S,C);g.stageResources.geometries.push(O),g.stageResources.materials.push(d.get(r)),c&&((0,o.pC)(a.textureColor)&&g.stageResources.textures.push(v.get(a.textureColor)),(0,o.pC)(a.textureNormal)&&g.stageResources.textures.push(v.get(a.textureNormal)),(0,o.pC)(a.textureOcclusion)&&g.stageResources.textures.push(v.get(a.textureOcclusion)),(0,o.pC)(a.textureEmissive)&&g.stageResources.textures.push(v.get(a.textureEmissive)),(0,o.pC)(a.textureMetallicRoughness)&&g.stageResources.textures.push(v.get(a.textureMetallicRoughness))),g.numberOfVertices+=b;const E=O.boundingInfo;(0,o.pC)(E)&&((0,u.pp)(g.boundingBox,E.getBBMin()),(0,u.pp)(g.boundingBox,E.getBBMax()))}))})),c}function dr(e){switch(e){case"BLEND":return M.JJ.Blend;case"MASK":return M.JJ.Mask;case"OPAQUE":case null:case void 0:return M.JJ.Opaque}}function ur(e,t){switch(t){case J.MX.TRIANGLES:return(0,x.nh)(e);case J.MX.TRIANGLE_STRIP:return(0,x.DA)(e);case J.MX.TRIANGLE_FAN:return(0,x.jX)(e)}}function hr(e,t){for(let i=0;i<e.model.lods.length;++i){const r=e.model.lods[i];e.customMeta.esriTreeRendering=!0;for(const n of r.parts){const r=n.attributes.normal;if((0,o.Wi)(r))return;const a=n.attributes.position,u=a.count,f=(0,d.c)(),m=(0,d.c)(),v=(0,d.c)(),g=(0,p.gS)(h.mc,u),x=(0,p.gS)(h.ct,u),T=(0,s.a)((0,l.c)(),n.transform);for(let o=0;o<u;o++){a.getVec(o,m),r.getVec(o,f),(0,c.m)(m,m,n.transform),(0,c.f)(v,m,t.center),(0,c.E)(v,v,t.radius);const s=v[2],l=(0,c.l)(v),d=Math.min(.45+.55*l*l,1);(0,c.E)(v,v,t.radius),(0,c.m)(v,v,T),(0,c.n)(v,v),i+1!==e.model.lods.length&&e.model.lods.length>1&&(0,c.e)(v,v,f,s>-1?.2:Math.min(-4*s-3.8,1)),x.setVec(o,v),g.set(o,0,255*d),g.set(o,1,255*d),g.set(o,2,255*d),g.set(o,3,255)}n.attributes.normal=x,n.attributes.color=g}}}},66352:function(e,t,i){i.d(t,{a9:function(){return r}});var r;i(19431);!function(e){e[e.Multiply=1]="Multiply",e[e.Ignore=2]="Ignore",e[e.Replace=3]="Replace",e[e.Tint=4]="Tint"}(r||(r={}))},44685:function(e,t,i){i.d(t,{U$:function(){return s}});var r=i(63338),o=i(90331);class n{constructor(e,t){this.layout=e,this.buffer="number"==typeof t?new ArrayBuffer(t*e.stride):t;for(const i of e.fieldNames){const t=e.fields.get(i);this[i]=new t.constructor(this.buffer,t.offset,this.stride)}}get stride(){return this.layout.stride}get count(){return this.buffer.byteLength/this.stride}get byteLength(){return this.buffer.byteLength}getField(e,t){const i=this[e];return i&&i.elementCount===t.ElementCount&&i.elementType===t.ElementType?i:null}slice(e,t){return new n(this.layout,this.buffer.slice(e*this.stride,t*this.stride))}copyFrom(e,t,i,r){const o=this.stride;if(o%4==0){const n=new Uint32Array(e.buffer,t*o,r*o/4);new Uint32Array(this.buffer,i*o,r*o/4).set(n)}else{const n=new Uint8Array(e.buffer,t*o,r*o);new Uint8Array(this.buffer,i*o,r*o).set(n)}}}class a{constructor(){this.stride=0,this.fields=new Map,this.fieldNames=[]}vec2f(e,t){return this._appendField(e,r.Eu,t),this}vec2f64(e,t){return this._appendField(e,r.q6,t),this}vec3f(e,t){return this._appendField(e,r.ct,t),this}vec3f64(e,t){return this._appendField(e,r.fP,t),this}vec4f(e,t){return this._appendField(e,r.ek,t),this}vec4f64(e,t){return this._appendField(e,r.Cd,t),this}mat3f(e,t){return this._appendField(e,r.gK,t),this}mat3f64(e,t){return this._appendField(e,r.ey,t),this}mat4f(e,t){return this._appendField(e,r.bj,t),this}mat4f64(e,t){return this._appendField(e,r.O1,t),this}vec4u8(e,t){return this._appendField(e,r.mc,t),this}f32(e,t){return this._appendField(e,r.ly,t),this}f64(e,t){return this._appendField(e,r.oS,t),this}u8(e,t){return this._appendField(e,r.D_,t),this}u16(e,t){return this._appendField(e,r.av,t),this}i8(e,t){return this._appendField(e,r.Hz,t),this}vec2i8(e,t){return this._appendField(e,r.Vs,t),this}vec2i16(e,t){return this._appendField(e,r.or,t),this}vec2u8(e,t){return this._appendField(e,r.xA,t),this}vec4u16(e,t){return this._appendField(e,r.v6,t),this}u32(e,t){return this._appendField(e,r.Nu,t),this}_appendField(e,t,i){const r=t.ElementCount*(0,o.n1)(t.ElementType),n=this.stride;this.fields.set(e,{size:r,constructor:t,offset:n,optional:i}),this.stride+=r,this.fieldNames.push(e)}alignTo(e){return this.stride=Math.floor((this.stride+e-1)/e)*e,this}hasField(e){return this.fieldNames.indexOf(e)>=0}createBuffer(e){return new n(this,e)}createView(e){return new n(this,e)}clone(){const e=new a;return e.stride=this.stride,e.fields=new Map,this.fields.forEach(((t,i)=>e.fields.set(i,t))),e.fieldNames=this.fieldNames.slice(),e.BufferType=this.BufferType,e}}function s(){return new a}},95650:function(e,t,i){i.d(t,{q:function(){return n}});var r=i(25714),o=i(23410);function n(e,t){t.output===r.H.Color&&t.receiveShadows?(e.varyings.add("linearDepth","float"),e.vertex.code.add(o.H`void forwardLinearDepth() { linearDepth = gl_Position.w; }`)):t.output===r.H.Depth||t.output===r.H.Shadow?(e.varyings.add("linearDepth","float"),e.vertex.uniforms.add("nearFar","vec2"),e.vertex.code.add(o.H`void forwardLinearDepth() {
linearDepth = (-position_view().z - nearFar[0]) / (nearFar[1] - nearFar[0]);
}`)):e.vertex.code.add(o.H`void forwardLinearDepth() {}`)}},57218:function(e,t,i){i.d(t,{w:function(){return o}});var r=i(23410);function o(e){e.vertex.code.add(r.H`vec4 offsetBackfacingClipPosition(vec4 posClip, vec3 posWorld, vec3 normalWorld, vec3 camPosWorld) {
vec3 camToVert = posWorld - camPosWorld;
bool isBackface = dot(camToVert, normalWorld) > 0.0;
if (isBackface) {
posClip.z += 0.0000003 * posClip.w;
}
return posClip;
}`)}},25714:function(e,t,i){var r;i.d(t,{H:function(){return r}}),function(e){e[e.Color=0]="Color",e[e.Depth=1]="Depth",e[e.Normal=2]="Normal",e[e.Shadow=3]="Shadow",e[e.Highlight=4]="Highlight",e[e.Draped=5]="Draped",e[e.Occlusion=6]="Occlusion",e[e.Alpha=7]="Alpha",e[e.COUNT=8]="COUNT"}(r||(r={}))},5885:function(e,t,i){i.d(t,{Vv:function(){return d},p2:function(){return c}});var r=i(61681),o=i(24455),n=i(39100),a=i(6766),s=i(8909),l=i(23410);function c(e,t){if(t.slicePlaneEnabled){e.extensions.add("GL_OES_standard_derivatives"),t.sliceEnabledForVertexPrograms&&(e.vertex.uniforms.add("slicePlaneOrigin","vec3"),e.vertex.uniforms.add("slicePlaneBasis1","vec3"),e.vertex.uniforms.add("slicePlaneBasis2","vec3")),e.fragment.uniforms.add("slicePlaneOrigin","vec3"),e.fragment.uniforms.add("slicePlaneBasis1","vec3"),e.fragment.uniforms.add("slicePlaneBasis2","vec3");const i=l.H`struct SliceFactors {
float front;
float side0;
float side1;
float side2;
float side3;
};
SliceFactors calculateSliceFactors(vec3 pos) {
vec3 rel = pos - slicePlaneOrigin;
vec3 slicePlaneNormal = -cross(slicePlaneBasis1, slicePlaneBasis2);
float slicePlaneW = -dot(slicePlaneNormal, slicePlaneOrigin);
float basis1Len2 = dot(slicePlaneBasis1, slicePlaneBasis1);
float basis2Len2 = dot(slicePlaneBasis2, slicePlaneBasis2);
float basis1Dot = dot(slicePlaneBasis1, rel);
float basis2Dot = dot(slicePlaneBasis2, rel);
return SliceFactors(
dot(slicePlaneNormal, pos) + slicePlaneW,
-basis1Dot - basis1Len2,
basis1Dot - basis1Len2,
-basis2Dot - basis2Len2,
basis2Dot - basis2Len2
);
}
bool sliceByFactors(SliceFactors factors) {
return factors.front < 0.0
&& factors.side0 < 0.0
&& factors.side1 < 0.0
&& factors.side2 < 0.0
&& factors.side3 < 0.0;
}
bool sliceEnabled() {
return dot(slicePlaneBasis1, slicePlaneBasis1) != 0.0;
}
bool sliceByPlane(vec3 pos) {
return sliceEnabled() && sliceByFactors(calculateSliceFactors(pos));
}
#define rejectBySlice(_pos_) sliceByPlane(_pos_)
#define discardBySlice(_pos_) { if (sliceByPlane(_pos_)) discard; }`,r=l.H`vec4 applySliceHighlight(vec4 color, vec3 pos) {
SliceFactors factors = calculateSliceFactors(pos);
const float HIGHLIGHT_WIDTH = 1.0;
const vec4 HIGHLIGHT_COLOR = vec4(0.0, 0.0, 0.0, 0.3);
factors.front /= (2.0 * HIGHLIGHT_WIDTH) * fwidth(factors.front);
factors.side0 /= (2.0 * HIGHLIGHT_WIDTH) * fwidth(factors.side0);
factors.side1 /= (2.0 * HIGHLIGHT_WIDTH) * fwidth(factors.side1);
factors.side2 /= (2.0 * HIGHLIGHT_WIDTH) * fwidth(factors.side2);
factors.side3 /= (2.0 * HIGHLIGHT_WIDTH) * fwidth(factors.side3);
if (sliceByFactors(factors)) {
return color;
}
float highlightFactor = (1.0 - step(0.5, factors.front))
* (1.0 - step(0.5, factors.side0))
* (1.0 - step(0.5, factors.side1))
* (1.0 - step(0.5, factors.side2))
* (1.0 - step(0.5, factors.side3));
return mix(color, vec4(HIGHLIGHT_COLOR.rgb, color.a), highlightFactor * HIGHLIGHT_COLOR.a);
}`,o=t.sliceHighlightDisabled?l.H`#define highlightSlice(_color_, _pos_) (_color_)`:l.H`
        ${r}
        #define highlightSlice(_color_, _pos_) (sliceEnabled() ? applySliceHighlight(_color_, _pos_) : (_color_))
      `;t.sliceEnabledForVertexPrograms&&e.vertex.code.add(i),e.fragment.code.add(i),e.fragment.code.add(o)}else{const i=l.H`#define rejectBySlice(_pos_) false
#define discardBySlice(_pos_) {}
#define highlightSlice(_color_, _pos_) (_color_)`;t.sliceEnabledForVertexPrograms&&e.vertex.code.add(i),e.fragment.code.add(i)}}function d(e,t,i,n){if(t.slicePlaneEnabled)if((0,r.pC)(i)){if((0,a.g)(u,i.origin),(0,a.g)(h,i.basis1),(0,a.g)(f,i.basis2),(0,r.pC)(n)&&(0,r.pC)(n.origin)&&(0,a.f)(u,i.origin,n.origin),(0,r.pC)(n)&&(0,r.pC)(n.view)){const e=(0,r.pC)(n.origin)?(0,o.j)(m,n.view,n.origin):n.view;(0,a.b)(h,h,u),(0,a.b)(f,f,u),(0,a.m)(u,u,e),(0,a.m)(h,h,e),(0,a.m)(f,f,e),(0,a.f)(h,h,u),(0,a.f)(f,f,u)}e.setUniform3fv("slicePlaneOrigin",u),e.setUniform3fv("slicePlaneBasis1",h),e.setUniform3fv("slicePlaneBasis2",f)}else e.setUniform3fv("slicePlaneBasis1",s.Z),e.setUniform3fv("slicePlaneBasis2",s.Z),e.setUniform3fv("slicePlaneOrigin",s.Z)}const u=(0,s.c)(),h=(0,s.c)(),f=(0,s.c)(),m=(0,n.c)()},4731:function(e,t,i){i.d(t,{w:function(){return o}});var r=i(23410);function o(e,t){const i={hasModelTransformation:!1,...t};if(i.hasModelTransformation)return i.linearDepth?void e.vertex.code.add(r.H`vec4 transformPositionWithDepth(mat4 proj, mat4 view, mat4 model, vec3 pos, vec2 nearFar, out float depth) {
vec4 eye = view * (model * vec4(pos, 1.0));
depth = (-eye.z - nearFar[0]) / (nearFar[1] - nearFar[0]) ;
return proj * eye;
}`):void e.vertex.code.add(r.H`vec4 transformPosition(mat4 proj, mat4 view, mat4 model, vec3 pos) {
return proj * (view * (model * vec4(pos, 1.0)));
}`);i.linearDepth?e.vertex.code.add(r.H`vec4 transformPositionWithDepth(mat4 proj, mat4 view, vec3 pos, vec2 nearFar, out float depth) {
vec4 eye = view * vec4(pos, 1.0);
depth = (-eye.z - nearFar[0]) / (nearFar[1] - nearFar[0]) ;
return proj * eye;
}`):e.vertex.code.add(r.H`vec4 transformPosition(mat4 proj, mat4 view, vec3 pos) {
return proj * (view * vec4(pos, 1.0));
}`)}},99163:function(e,t,i){i.d(t,{d3:function(){return d},fQ:function(){return c}});var r=i(8909),o=i(25714),n=i(5331),a=i(23410),s=i(21414),l=i(30560);function c(e,t){t.instanced&&t.instancedDoublePrecision&&(e.attributes.add(s.T.MODELORIGINHI,"vec3"),e.attributes.add(s.T.MODELORIGINLO,"vec3"),e.attributes.add(s.T.MODEL,"mat3"),e.attributes.add(s.T.MODELNORMAL,"mat3")),t.instancedDoublePrecision&&(e.vertex.include(n.$,t),e.vertex.uniforms.add("viewOriginHi","vec3"),e.vertex.uniforms.add("viewOriginLo","vec3"));const i=[a.H`
    vec3 calculateVPos() {
      ${t.instancedDoublePrecision?"return model * localPosition().xyz;":"return localPosition().xyz;"}
    }
    `,a.H`
    vec3 subtractOrigin(vec3 _pos) {
      ${t.instancedDoublePrecision?a.H`
          vec3 originDelta = dpAdd(viewOriginHi, viewOriginLo, -modelOriginHi, -modelOriginLo);
          return _pos - originDelta;`:"return vpos;"}
    }
    `,a.H`
    vec3 dpNormal(vec4 _normal) {
      ${t.instancedDoublePrecision?"return normalize(modelNormal * _normal.xyz);":"return normalize(_normal.xyz);"}
    }
    `,a.H`
    vec3 dpNormalView(vec4 _normal) {
      ${t.instancedDoublePrecision?"return normalize((viewNormal * vec4(modelNormal * _normal.xyz, 1.0)).xyz);":"return normalize((viewNormal * _normal).xyz);"}
    }
    `,t.vertexTangents?a.H`
    vec4 dpTransformVertexTangent(vec4 _tangent) {
      ${t.instancedDoublePrecision?"return vec4(modelNormal * _tangent.xyz, _tangent.w);":"return _tangent;"}

    }
    `:a.H``];e.vertex.code.add(i[0]),e.vertex.code.add(i[1]),e.vertex.code.add(i[2]),t.output===o.H.Normal&&e.vertex.code.add(i[3]),e.vertex.code.add(i[4])}function d(e,t){(0,l.po)(t,u,h,3),e.setUniform3fv("viewOriginHi",u),e.setUniform3fv("viewOriginLo",h)}const u=(0,r.c)(),h=(0,r.c)()},27125:function(e,t,i){i.d(t,{O:function(){return s},h:function(){return n}});var r=i(23410);function o(e){const t=r.H`vec3 decodeNormal(vec2 f) {
float z = 1.0 - abs(f.x) - abs(f.y);
return vec3(f + sign(f) * min(z, 0.0), z);
}`;e.fragment.code.add(t),e.vertex.code.add(t)}var n,a=i(21414);function s(e,t){t.normalType===n.Attribute&&(e.attributes.add(a.T.NORMAL,"vec3"),e.vertex.code.add(r.H`vec3 normalModel() {
return normal;
}`)),t.normalType===n.CompressedAttribute&&(e.include(o),e.attributes.add(a.T.NORMALCOMPRESSED,"vec2"),e.vertex.code.add(r.H`vec3 normalModel() {
return decodeNormal(normalCompressed);
}`)),t.normalType===n.ScreenDerivative&&(e.extensions.add("GL_OES_standard_derivatives"),e.fragment.code.add(r.H`vec3 screenDerivativeNormal(vec3 positionView) {
return normalize(cross(dFdx(positionView), dFdy(positionView)));
}`))}!function(e){e[e.Attribute=0]="Attribute",e[e.CompressedAttribute=1]="CompressedAttribute",e[e.Ground=2]="Ground",e[e.ScreenDerivative=3]="ScreenDerivative",e[e.COUNT=4]="COUNT"}(n||(n={}))},91636:function(e,t,i){i.d(t,{f:function(){return n}});var r=i(23410),o=i(21414);function n(e){e.attributes.add(o.T.POSITION,"vec3"),e.vertex.code.add(r.H`vec3 positionModel() { return position; }`)}},45658:function(e,t,i){i.d(t,{R:function(){return s}});var r=i(66352),o=i(23410);function n(e){e.vertex.code.add(o.H`
    vec4 decodeSymbolColor(vec4 symbolColor, out int colorMixMode) {
      float symbolAlpha = 0.0;

      const float maxTint = 85.0;
      const float maxReplace = 170.0;
      const float scaleAlpha = 3.0;

      if (symbolColor.a > maxReplace) {
        colorMixMode = ${o.H.int(r.a9.Multiply)};
        symbolAlpha = scaleAlpha * (symbolColor.a - maxReplace);
      } else if (symbolColor.a > maxTint) {
        colorMixMode = ${o.H.int(r.a9.Replace)};
        symbolAlpha = scaleAlpha * (symbolColor.a - maxTint);
      } else if (symbolColor.a > 0.0) {
        colorMixMode = ${o.H.int(r.a9.Tint)};
        symbolAlpha = scaleAlpha * symbolColor.a;
      } else {
        colorMixMode = ${o.H.int(r.a9.Multiply)};
        symbolAlpha = 0.0;
      }

      return vec4(symbolColor.r, symbolColor.g, symbolColor.b, symbolAlpha);
    }
  `)}var a=i(21414);function s(e,t){t.symbolColor?(e.include(n),e.attributes.add(a.T.SYMBOLCOLOR,"vec4"),e.varyings.add("colorMixMode","mediump float")):e.fragment.uniforms.add("colorMixMode","int"),t.symbolColor?e.vertex.code.add(o.H`int symbolColorMixMode;
vec4 getSymbolColor() {
return decodeSymbolColor(symbolColor, symbolColorMixMode) * 0.003921568627451;
}
void forwardColorMixMode() {
colorMixMode = float(symbolColorMixMode) + 0.5;
}`):e.vertex.code.add(o.H`vec4 getSymbolColor() { return vec4(1.0); }
void forwardColorMixMode() {}`)}},82082:function(e,t,i){i.d(t,{D:function(){return a},N:function(){return r}});var r,o=i(23410),n=i(21414);function a(e,t){t.attributeTextureCoordinates===r.Default&&(e.attributes.add(n.T.UV0,"vec2"),e.varyings.add("vuv0","vec2"),e.vertex.code.add(o.H`void forwardTextureCoordinates() {
vuv0 = uv0;
}`)),t.attributeTextureCoordinates===r.Atlas&&(e.attributes.add(n.T.UV0,"vec2"),e.varyings.add("vuv0","vec2"),e.attributes.add(n.T.UVREGION,"vec4"),e.varyings.add("vuvRegion","vec4"),e.vertex.code.add(o.H`void forwardTextureCoordinates() {
vuv0 = uv0;
vuvRegion = uvRegion;
}`)),t.attributeTextureCoordinates===r.None&&e.vertex.code.add(o.H`void forwardTextureCoordinates() {}`)}!function(e){e[e.None=0]="None",e[e.Default=1]="Default",e[e.Atlas=2]="Atlas",e[e.COUNT=3]="COUNT"}(r||(r={}))},6502:function(e,t,i){i.d(t,{c:function(){return n}});var r=i(23410),o=i(21414);function n(e,t){t.attributeColor?(e.attributes.add(o.T.COLOR,"vec4"),e.varyings.add("vColor","vec4"),e.vertex.code.add(r.H`void forwardVertexColor() { vColor = color; }`),e.vertex.code.add(r.H`void forwardNormalizedVertexColor() { vColor = color * 0.003921568627451; }`)):e.vertex.code.add(r.H`void forwardVertexColor() {}
void forwardNormalizedVertexColor() {}`)}},51592:function(e,t,i){i.d(t,{B:function(){return c}});var r=i(65684),o=i(27125),n=(i(34344),i(39100),i(8909),i(91636)),a=i(5331),s=i(23410);function l(e,t){e.include(n.f),e.vertex.include(a.$,t),e.varyings.add("vPositionWorldCameraRelative","vec3"),e.varyings.add("vPosition_view","vec3"),e.vertex.uniforms.add("transformWorldFromModelRS","mat3"),e.vertex.uniforms.add("transformWorldFromModelTH","vec3"),e.vertex.uniforms.add("transformWorldFromModelTL","vec3"),e.vertex.uniforms.add("transformWorldFromViewTH","vec3"),e.vertex.uniforms.add("transformWorldFromViewTL","vec3"),e.vertex.uniforms.add("transformViewFromCameraRelativeRS","mat3"),e.vertex.uniforms.add("transformProjFromView","mat4"),e.vertex.code.add(s.H`vec3 positionWorldCameraRelative() {
vec3 rotatedModelPosition = transformWorldFromModelRS * positionModel();
vec3 transform_CameraRelativeFromModel = dpAdd(
transformWorldFromModelTL,
transformWorldFromModelTH,
-transformWorldFromViewTL,
-transformWorldFromViewTH
);
return transform_CameraRelativeFromModel + rotatedModelPosition;
}
vec3 position_view() {
return transformViewFromCameraRelativeRS * positionWorldCameraRelative();
}
void forwardPosition() {
vPositionWorldCameraRelative = positionWorldCameraRelative();
vPosition_view = position_view();
gl_Position = transformProjFromView * vec4(vPosition_view, 1.0);
}
vec3 positionWorld() {
return transformWorldFromViewTL + vPositionWorldCameraRelative;
}`),e.fragment.uniforms.add("transformWorldFromViewTL","vec3"),e.fragment.code.add(s.H`vec3 positionWorld() {
return transformWorldFromViewTL + vPositionWorldCameraRelative;
}`)}function c(e,t){t.normalType===o.h.Attribute||t.normalType===o.h.CompressedAttribute?(e.include(o.O,t),e.varyings.add("vNormalWorld","vec3"),e.varyings.add("vNormalView","vec3"),e.vertex.uniforms.add("transformNormalGlobalFromModel","mat3"),e.vertex.uniforms.add("transformNormalViewFromGlobal","mat3"),e.vertex.code.add(s.H`void forwardNormal() {
vNormalWorld = transformNormalGlobalFromModel * normalModel();
vNormalView = transformNormalViewFromGlobal * vNormalWorld;
}`)):t.normalType===o.h.Ground?(e.include(l,t),e.varyings.add("vNormalWorld","vec3"),e.vertex.code.add(s.H`
    void forwardNormal() {
      vNormalWorld = ${t.viewingMode===r.JY.Global?s.H`normalize(vPositionWorldCameraRelative);`:s.H`vec3(0.0, 0.0, 1.0);`}
    }
    `)):e.vertex.code.add(s.H`void forwardNormal() {}`)}},512:function(e,t,i){i.d(t,{i:function(){return a}});var r=i(82082),o=i(23410);function n(e){e.extensions.add("GL_EXT_shader_texture_lod"),e.extensions.add("GL_OES_standard_derivatives"),e.fragment.code.add(o.H`#ifndef GL_EXT_shader_texture_lod
float calcMipMapLevel(const vec2 ddx, const vec2 ddy) {
float deltaMaxSqr = max(dot(ddx, ddx), dot(ddy, ddy));
return max(0.0, 0.5 * log2(deltaMaxSqr));
}
#endif
vec4 textureAtlasLookup(sampler2D texture, vec2 textureSize, vec2 textureCoordinates, vec4 atlasRegion) {
vec2 atlasScale = atlasRegion.zw - atlasRegion.xy;
vec2 uvAtlas = fract(textureCoordinates) * atlasScale + atlasRegion.xy;
float maxdUV = 0.125;
vec2 dUVdx = clamp(dFdx(textureCoordinates), -maxdUV, maxdUV) * atlasScale;
vec2 dUVdy = clamp(dFdy(textureCoordinates), -maxdUV, maxdUV) * atlasScale;
#ifdef GL_EXT_shader_texture_lod
return texture2DGradEXT(texture, uvAtlas, dUVdx, dUVdy);
#else
vec2 dUVdxAuto = dFdx(uvAtlas);
vec2 dUVdyAuto = dFdy(uvAtlas);
float mipMapLevel = calcMipMapLevel(dUVdx * textureSize, dUVdy * textureSize);
float autoMipMapLevel = calcMipMapLevel(dUVdxAuto * textureSize, dUVdyAuto * textureSize);
return texture2D(texture, uvAtlas, mipMapLevel - autoMipMapLevel);
#endif
}`)}function a(e,t){e.include(r.D,t),e.fragment.code.add(o.H`
  struct TextureLookupParameter {
    vec2 uv;
    ${t.supportsTextureAtlas?"vec2 size;":""}
  } vtc;
  `),t.attributeTextureCoordinates===r.N.Default&&e.fragment.code.add(o.H`vec4 textureLookup(sampler2D tex, TextureLookupParameter params) {
return texture2D(tex, params.uv);
}`),t.attributeTextureCoordinates===r.N.Atlas&&(e.include(n),e.fragment.code.add(o.H`vec4 textureLookup(sampler2D tex, TextureLookupParameter params) {
return textureAtlasLookup(tex, params.size, params.uv, vuvRegion);
}`))}},78183:function(e,t,i){i.d(t,{LC:function(){return a},Mo:function(){return s}});var r=i(65684),o=i(23410);i(16318);function n(e){e.vertex.code.add(o.H`float screenSizePerspectiveMinSize(float size, vec4 factor) {
float nonZeroSize = 1.0 - step(size, 0.0);
return (
factor.z * (
1.0 +
nonZeroSize *
2.0 * factor.w / (
size + (1.0 - nonZeroSize)
)
)
);
}`),e.vertex.code.add(o.H`float screenSizePerspectiveViewAngleDependentFactor(float absCosAngle) {
return absCosAngle * absCosAngle * absCosAngle;
}`),e.vertex.code.add(o.H`vec4 screenSizePerspectiveScaleFactor(float absCosAngle, float distanceToCamera, vec4 params) {
return vec4(
min(params.x / (distanceToCamera - params.y), 1.0),
screenSizePerspectiveViewAngleDependentFactor(absCosAngle),
params.z,
params.w
);
}`),e.vertex.code.add(o.H`float applyScreenSizePerspectiveScaleFactorFloat(float size, vec4 factor) {
return max(mix(size * factor.x, size, factor.y), screenSizePerspectiveMinSize(size, factor));
}`),e.vertex.code.add(o.H`float screenSizePerspectiveScaleFloat(float size, float absCosAngle, float distanceToCamera, vec4 params) {
return applyScreenSizePerspectiveScaleFactorFloat(
size,
screenSizePerspectiveScaleFactor(absCosAngle, distanceToCamera, params)
);
}`),e.vertex.code.add(o.H`vec2 applyScreenSizePerspectiveScaleFactorVec2(vec2 size, vec4 factor) {
return mix(size * clamp(factor.x, screenSizePerspectiveMinSize(size.y, factor) / max(1e-5, size.y), 1.0), size, factor.y);
}`),e.vertex.code.add(o.H`vec2 screenSizePerspectiveScaleVec2(vec2 size, float absCosAngle, float distanceToCamera, vec4 params) {
return applyScreenSizePerspectiveScaleFactorVec2(size, screenSizePerspectiveScaleFactor(absCosAngle, distanceToCamera, params));
}`)}function a(e,t){const i=e.vertex.code;t.verticalOffsetEnabled?(e.vertex.uniforms.add("verticalOffset","vec4"),t.screenSizePerspectiveEnabled&&(e.include(n),e.vertex.uniforms.add("screenSizePerspectiveAlignment","vec4")),i.add(o.H`
    vec3 calculateVerticalOffset(vec3 worldPos, vec3 localOrigin) {
      float viewDistance = length((view * vec4(worldPos, 1.0)).xyz);
      ${t.viewingMode===r.JY.Global?o.H`vec3 worldNormal = normalize(worldPos + localOrigin);`:o.H`vec3 worldNormal = vec3(0.0, 0.0, 1.0);`}
      ${t.screenSizePerspectiveEnabled?o.H`
          float cosAngle = dot(worldNormal, normalize(worldPos - cameraPosition));
          float verticalOffsetScreenHeight = screenSizePerspectiveScaleFloat(verticalOffset.x, abs(cosAngle), viewDistance, screenSizePerspectiveAlignment);`:o.H`
          float verticalOffsetScreenHeight = verticalOffset.x;`}
      // Screen sized offset in world space, used for example for line callouts
      float worldOffset = clamp(verticalOffsetScreenHeight * verticalOffset.y * viewDistance, verticalOffset.z, verticalOffset.w);
      return worldNormal * worldOffset;
    }

    vec3 addVerticalOffset(vec3 worldPos, vec3 localOrigin) {
      return worldPos + calculateVerticalOffset(worldPos, localOrigin);
    }
    `)):i.add(o.H`vec3 addVerticalOffset(vec3 worldPos, vec3 localOrigin) { return worldPos; }`)}function s(e,t,i){if(!t.verticalOffset)return;const r=l(t.verticalOffset,i.camera.fovY,i.camera.fullViewport[3]),o=i.camera.pixelRatio||1;e.setUniform4f("verticalOffset",r.screenLength*o,r.perDistance,r.minWorldLength,r.maxWorldLength)}function l(e,t,i,r=c){return r.screenLength=e.screenLength,r.perDistance=Math.tan(.5*t)/(.5*i),r.minWorldLength=e.minWorldLength,r.maxWorldLength=e.maxWorldLength,r}const c={screenLength:0,perDistance:0,minWorldLength:0,maxWorldLength:0}},52372:function(e,t,i){i.d(t,{s:function(){return p}});var r=i(25714),o=i(5885),n=i(4731),a=i(27125),s=i(82082),l=i(51592),c=i(9794),d=i(23410);function u(e,t){e.fragment.include(c.n),t.output===r.H.Shadow?(e.extensions.add("GL_OES_standard_derivatives"),e.fragment.code.add(d.H`float _calculateFragDepth(const in float depth) {
const float SLOPE_SCALE = 2.0;
const float BIAS = 2.0 * .000015259;
float m = max(abs(dFdx(depth)), abs(dFdy(depth)));
float result = depth + SLOPE_SCALE * m + BIAS;
return clamp(result, .0, .999999);
}
void outputDepth(float _linearDepth) {
gl_FragColor = float2rgba(_calculateFragDepth(_linearDepth));
}`)):t.output===r.H.Depth&&e.fragment.code.add(d.H`void outputDepth(float _linearDepth) {
gl_FragColor = float2rgba(_linearDepth);
}`)}var h=i(55994),f=i(12664),m=i(41272);function p(e,t){const i=e.vertex.code,c=e.fragment.code,p=t.hasModelTransformation;t.output!==r.H.Depth&&t.output!==r.H.Shadow||(e.include(n.w,{linearDepth:!0,hasModelTransformation:p}),e.include(s.D,t),e.include(f.kl,t),e.include(u,t),e.include(o.p2,t),e.vertex.uniforms.add("nearFar","vec2"),e.varyings.add("depth","float"),t.hasColorTexture&&e.fragment.uniforms.add("tex","sampler2D"),i.add(d.H`
      void main(void) {
        vpos = calculateVPos();
        vpos = subtractOrigin(vpos);
        vpos = addVerticalOffset(vpos, localOrigin);
        gl_Position = transformPositionWithDepth(proj, view, ${p?"model,":""} vpos, nearFar, depth);
        forwardTextureCoordinates();
      }
    `),e.include(m.sj,t),c.add(d.H`
      void main(void) {
        discardBySlice(vpos);
        ${t.hasColorTexture?d.H`
        vec4 texColor = texture2D(tex, vuv0);
        discardOrAdjustAlpha(texColor);`:""}
        outputDepth(depth);
      }
    `)),t.output===r.H.Normal&&(e.include(n.w,{linearDepth:!1,hasModelTransformation:p}),e.include(a.O,t),e.include(l.B,t),e.include(s.D,t),e.include(f.kl,t),t.hasColorTexture&&e.fragment.uniforms.add("tex","sampler2D"),e.vertex.uniforms.add("viewNormal","mat4"),e.varyings.add("vPositionView","vec3"),i.add(d.H`
      void main(void) {
        vpos = calculateVPos();
        vpos = subtractOrigin(vpos);
        ${t.normalType===a.h.Attribute?d.H`
        vNormalWorld = dpNormalView(vvLocalNormal(normalModel()));`:""}
        vpos = addVerticalOffset(vpos, localOrigin);
        gl_Position = transformPosition(proj, view, ${p?"model,":""} vpos);
        forwardTextureCoordinates();
      }
    `),e.include(o.p2,t),e.include(m.sj,t),c.add(d.H`
      void main() {
        discardBySlice(vpos);
        ${t.hasColorTexture?d.H`
        vec4 texColor = texture2D(tex, vuv0);
        discardOrAdjustAlpha(texColor);`:""}

        ${t.normalType===a.h.ScreenDerivative?d.H`
            vec3 normal = screenDerivativeNormal(vPositionView);`:d.H`
            vec3 normal = normalize(vNormalWorld);
            if (gl_FrontFacing == false) normal = -normal;`}
        gl_FragColor = vec4(vec3(0.5) + 0.5 * normal, 1.0);
      }
    `)),t.output===r.H.Highlight&&(e.include(n.w,{linearDepth:!1,hasModelTransformation:p}),e.include(s.D,t),e.include(f.kl,t),t.hasColorTexture&&e.fragment.uniforms.add("tex","sampler2D"),i.add(d.H`
      void main(void) {
        vpos = calculateVPos();
        vpos = subtractOrigin(vpos);
        vpos = addVerticalOffset(vpos, localOrigin);
        gl_Position = transformPosition(proj, view, ${p?"model,":""} vpos);
        forwardTextureCoordinates();
      }
    `),e.include(o.p2,t),e.include(m.sj,t),e.include(h.bA),c.add(d.H`
      void main() {
        discardBySlice(vpos);
        ${t.hasColorTexture?d.H`
        vec4 texColor = texture2D(tex, vuv0);
        discardOrAdjustAlpha(texColor);`:""}
        outputHighlight();
      }
    `))}},55994:function(e,t,i){i.d(t,{bA:function(){return s},wW:function(){return l}});var r=i(1983),o=i(23410);const n=(0,r.f)(1,1,0,1),a=(0,r.f)(1,0,1,1);function s(e){e.fragment.uniforms.add("depthTex","sampler2D"),e.fragment.uniforms.add("highlightViewportPixelSz","vec4"),e.fragment.constants.add("occludedHighlightFlag","vec4",n).add("unoccludedHighlightFlag","vec4",a),e.fragment.code.add(o.H`void outputHighlight() {
vec4 fragCoord = gl_FragCoord;
float sceneDepth = texture2D(depthTex, (fragCoord.xy - highlightViewportPixelSz.xy) * highlightViewportPixelSz.zw).r;
if (fragCoord.z > sceneDepth + 5e-7) {
gl_FragColor = occludedHighlightFlag;
}
else {
gl_FragColor = unoccludedHighlightFlag;
}
}`)}function l(e,t){e.bindTexture(t.highlightDepthTexture,"depthTex"),e.setUniform4f("highlightViewportPixelSz",0,0,t.inverseViewport[0],t.inverseViewport[1])}},6665:function(e,t,i){i.d(t,{S:function(){return n}});var r=i(9794),o=i(23410);function n(e){e.include(r.n),e.code.add(o.H`float linearDepthFromFloat(float depth, vec2 nearFar) {
return -(depth * (nearFar[1] - nearFar[0]) + nearFar[0]);
}
float linearDepthFromTexture(sampler2D depthTex, vec2 uv, vec2 nearFar) {
return linearDepthFromFloat(rgba2float(texture2D(depthTex, uv)), nearFar);
}`)}},3417:function(e,t,i){i.d(t,{Q:function(){return l}});var r=i(82082),o=i(512),n=i(2833),a=i(23410),s=i(21414);function l(e,t){const i=e.fragment;t.vertexTangents?(e.attributes.add(s.T.TANGENT,"vec4"),e.varyings.add("vTangent","vec4"),t.doubleSidedMode===n.q.WindingOrder?i.code.add(a.H`mat3 computeTangentSpace(vec3 normal) {
float tangentHeadedness = gl_FrontFacing ? vTangent.w : -vTangent.w;
vec3 tangent = normalize(gl_FrontFacing ? vTangent.xyz : -vTangent.xyz);
vec3 bitangent = cross(normal, tangent) * tangentHeadedness;
return mat3(tangent, bitangent, normal);
}`):i.code.add(a.H`mat3 computeTangentSpace(vec3 normal) {
float tangentHeadedness = vTangent.w;
vec3 tangent = normalize(vTangent.xyz);
vec3 bitangent = cross(normal, tangent) * tangentHeadedness;
return mat3(tangent, bitangent, normal);
}`)):(e.extensions.add("GL_OES_standard_derivatives"),i.code.add(a.H`mat3 computeTangentSpace(vec3 normal, vec3 pos, vec2 st) {
vec3 Q1 = dFdx(pos);
vec3 Q2 = dFdy(pos);
vec2 stx = dFdx(st);
vec2 sty = dFdy(st);
float det = stx.t * sty.s - sty.t * stx.s;
vec3 T = stx.t * Q2 - sty.t * Q1;
T = T - normal * dot(normal, T);
T *= inversesqrt(max(dot(T,T), 1.e-10));
vec3 B = sign(det) * cross(normal, T);
return mat3(T, B, normal);
}`)),t.attributeTextureCoordinates!==r.N.None&&(e.include(o.i,t),i.uniforms.add("normalTexture","sampler2D"),i.uniforms.add("normalTextureSize","vec2"),i.code.add(a.H`
    vec3 computeTextureNormal(mat3 tangentSpace, vec2 uv) {
      vtc.uv = uv;
      ${t.supportsTextureAtlas?"vtc.size = normalTextureSize;":""}
      vec3 rawNormal = textureLookup(normalTexture, vtc).rgb * 2.0 - 1.0;
      return tangentSpace * rawNormal;
    }
  `))}},30786:function(e,t,i){i.d(t,{K:function(){return o}});var r=i(23410);function o(e,t){const i=e.fragment;t.receiveAmbientOcclusion?(i.uniforms.add("ssaoTex","sampler2D"),i.uniforms.add("viewportPixelSz","vec4"),i.code.add(r.H`float evaluateAmbientOcclusion() {
return 1.0 - texture2D(ssaoTex, (gl_FragCoord.xy - viewportPixelSz.xy) * viewportPixelSz.zw).a;
}
float evaluateAmbientOcclusionInverse() {
float ssao = texture2D(ssaoTex, (gl_FragCoord.xy - viewportPixelSz.xy) * viewportPixelSz.zw).a;
return viewportPixelSz.z < 0.0 ? 1.0 : ssao;
}`)):i.code.add(r.H`float evaluateAmbientOcclusion() { return 0.0; }
float evaluateAmbientOcclusionInverse() { return 1.0; }`)}},40566:function(e,t,i){i.d(t,{X:function(){return h}});var r=i(65684),o=i(3864),n=i(23410);function a(e,t){const i=e.fragment,r=void 0!==t.lightingSphericalHarmonicsOrder?t.lightingSphericalHarmonicsOrder:2;0===r?(i.uniforms.add("lightingAmbientSH0","vec3"),i.code.add(n.H`vec3 calculateAmbientIrradiance(vec3 normal, float ambientOcclusion) {
vec3 ambientLight = 0.282095 * lightingAmbientSH0;
return ambientLight * (1.0 - ambientOcclusion);
}`)):1===r?(i.uniforms.add("lightingAmbientSH_R","vec4"),i.uniforms.add("lightingAmbientSH_G","vec4"),i.uniforms.add("lightingAmbientSH_B","vec4"),i.code.add(n.H`vec3 calculateAmbientIrradiance(vec3 normal, float ambientOcclusion) {
vec4 sh0 = vec4(
0.282095,
0.488603 * normal.x,
0.488603 * normal.z,
0.488603 * normal.y
);
vec3 ambientLight = vec3(
dot(lightingAmbientSH_R, sh0),
dot(lightingAmbientSH_G, sh0),
dot(lightingAmbientSH_B, sh0)
);
return ambientLight * (1.0 - ambientOcclusion);
}`)):2===r&&(i.uniforms.add("lightingAmbientSH0","vec3"),i.uniforms.add("lightingAmbientSH_R1","vec4"),i.uniforms.add("lightingAmbientSH_G1","vec4"),i.uniforms.add("lightingAmbientSH_B1","vec4"),i.uniforms.add("lightingAmbientSH_R2","vec4"),i.uniforms.add("lightingAmbientSH_G2","vec4"),i.uniforms.add("lightingAmbientSH_B2","vec4"),i.code.add(n.H`vec3 calculateAmbientIrradiance(vec3 normal, float ambientOcclusion) {
vec3 ambientLight = 0.282095 * lightingAmbientSH0;
vec4 sh1 = vec4(
0.488603 * normal.x,
0.488603 * normal.z,
0.488603 * normal.y,
1.092548 * normal.x * normal.y
);
vec4 sh2 = vec4(
1.092548 * normal.y * normal.z,
0.315392 * (3.0 * normal.z * normal.z - 1.0),
1.092548 * normal.x * normal.z,
0.546274 * (normal.x * normal.x - normal.y * normal.y)
);
ambientLight += vec3(
dot(lightingAmbientSH_R1, sh1),
dot(lightingAmbientSH_G1, sh1),
dot(lightingAmbientSH_B1, sh1)
);
ambientLight += vec3(
dot(lightingAmbientSH_R2, sh2),
dot(lightingAmbientSH_G2, sh2),
dot(lightingAmbientSH_B2, sh2)
);
return ambientLight * (1.0 - ambientOcclusion);
}`),t.pbrMode!==o.f7.Normal&&t.pbrMode!==o.f7.Schematic||i.code.add(n.H`const vec3 skyTransmittance = vec3(0.9, 0.9, 1.0);
vec3 calculateAmbientRadiance(float ambientOcclusion)
{
vec3 ambientLight = 1.2 * (0.282095 * lightingAmbientSH0) - 0.2;
return ambientLight *= (1.0 - ambientOcclusion) * skyTransmittance;
}`))}var s=i(30786);function l(e){const t=e.fragment;t.uniforms.add("lightingMainDirection","vec3"),t.uniforms.add("lightingMainIntensity","vec3"),t.uniforms.add("lightingFixedFactor","float"),t.uniforms.add("lightingSpecularStrength","float"),t.uniforms.add("lightingEnvironmentStrength","float"),t.code.add(n.H`vec3 evaluateMainLighting(vec3 normal_global, float shadowing) {
float dotVal = clamp(dot(normal_global, lightingMainDirection), 0.0, 1.0);
dotVal = mix(dotVal, 1.0, lightingFixedFactor);
return lightingMainIntensity * ((1.0 - shadowing) * dotVal);
}`)}var c=i(74839),d=i(95509),u=i(20105);function h(e,t){const i=e.fragment;e.include(l),e.include(s.K,t),t.pbrMode!==o.f7.Disabled&&e.include(c.T,t),e.include(a,t),t.receiveShadows&&e.include(u.hX,t),i.uniforms.add("lightingGlobalFactor","float"),i.uniforms.add("ambientBoostFactor","float"),i.uniforms.add("hasFillLights","bool"),e.include(d.e),i.code.add(n.H`
    const float GAMMA_SRGB = 2.1;
    const float INV_GAMMA_SRGB = 0.4761904;
    ${t.pbrMode===o.f7.Disabled?"":"const vec3 GROUND_REFLECTANCE = vec3(0.2);"}
  `),i.code.add(n.H`
    float additionalDirectedAmbientLight(vec3 vPosWorld) {
      float vndl = dot(${t.viewingMode===r.JY.Global?n.H`normalize(vPosWorld)`:n.H`vec3(0.0, 0.0, 1.0)`}, lightingMainDirection);
      return smoothstep(0.0, 1.0, clamp(vndl * 2.5, 0.0, 1.0));
    }
  `),i.code.add(n.H`vec3 evaluateAdditionalLighting(float ambientOcclusion, vec3 vPosWorld) {
float additionalAmbientScale = additionalDirectedAmbientLight(vPosWorld);
return (1.0 - ambientOcclusion) * additionalAmbientScale * ambientBoostFactor * lightingGlobalFactor * lightingMainIntensity;
}`),t.pbrMode===o.f7.Disabled||t.pbrMode===o.f7.WaterOnIntegratedMesh?i.code.add(n.H`vec3 evaluateSceneLighting(vec3 normalWorld, vec3 albedo, float shadow, float ssao, vec3 additionalLight)
{
vec3 mainLighting = evaluateMainLighting(normalWorld, shadow);
vec3 ambientLighting = calculateAmbientIrradiance(normalWorld, ssao);
vec3 albedoLinear = pow(albedo, vec3(GAMMA_SRGB));
vec3 totalLight = mainLighting + ambientLighting + additionalLight;
totalLight = min(totalLight, vec3(PI));
vec3 outColor = vec3((albedoLinear / PI) * totalLight);
return pow(outColor, vec3(INV_GAMMA_SRGB));
}`):t.pbrMode!==o.f7.Normal&&t.pbrMode!==o.f7.Schematic||(i.code.add(n.H`const float fillLightIntensity = 0.25;
const float horizonLightDiffusion = 0.4;
const float additionalAmbientIrradianceFactor = 0.02;
vec3 evaluateSceneLightingPBR(vec3 normal, vec3 albedo, float shadow, float ssao, vec3 additionalLight, vec3 viewDir, vec3 normalGround, vec3 mrr, vec3 _emission, float additionalAmbientIrradiance)
{
vec3 viewDirection = -viewDir;
vec3 mainLightDirection = lightingMainDirection;
vec3 h = normalize(viewDirection + mainLightDirection);
PBRShadingInfo inputs;
inputs.NdotL = clamp(dot(normal, mainLightDirection), 0.001, 1.0);
inputs.NdotV = clamp(abs(dot(normal, viewDirection)), 0.001, 1.0);
inputs.NdotH = clamp(dot(normal, h), 0.0, 1.0);
inputs.VdotH = clamp(dot(viewDirection, h), 0.0, 1.0);
inputs.NdotNG = clamp(dot(normal, normalGround), -1.0, 1.0);
vec3 reflectedView = normalize(reflect(viewDirection, normal));
inputs.RdotNG = clamp(dot(reflectedView, normalGround), -1.0, 1.0);
inputs.albedoLinear = pow(albedo, vec3(GAMMA_SRGB));
inputs.ssao = ssao;
inputs.metalness = mrr[0];
inputs.roughness = clamp(mrr[1] * mrr[1], 0.001, 0.99);`),i.code.add(n.H`inputs.f0 = (0.16 * mrr[2] * mrr[2]) * (1.0 - inputs.metalness) + inputs.albedoLinear * inputs.metalness;
inputs.f90 = vec3(clamp(dot(inputs.f0, vec3(50.0 * 0.33)), 0.0, 1.0));
inputs.diffuseColor = inputs.albedoLinear * (vec3(1.0) - inputs.f0) * (1.0 - inputs.metalness);`),i.code.add(n.H`vec3 ambientDir = vec3(5.0 * normalGround[1] - normalGround[0] * normalGround[2], - 5.0 * normalGround[0] - normalGround[2] * normalGround[1], normalGround[1] * normalGround[1] + normalGround[0] * normalGround[0]);
ambientDir = ambientDir != vec3(0.0)? normalize(ambientDir) : normalize(vec3(5.0, -1.0, 0.0));
inputs.NdotAmbDir = hasFillLights ? abs(dot(normal, ambientDir)) : 1.0;
vec3 mainLightIrradianceComponent = inputs.NdotL * (1.0 - shadow) * lightingMainIntensity;
vec3 fillLightsIrradianceComponent = inputs.NdotAmbDir * lightingMainIntensity * fillLightIntensity;
vec3 ambientLightIrradianceComponent = calculateAmbientIrradiance(normal, ssao) + additionalLight;
inputs.skyIrradianceToSurface = ambientLightIrradianceComponent + mainLightIrradianceComponent + fillLightsIrradianceComponent ;
inputs.groundIrradianceToSurface = GROUND_REFLECTANCE * ambientLightIrradianceComponent + mainLightIrradianceComponent + fillLightsIrradianceComponent ;`),i.code.add(n.H`vec3 horizonRingDir = inputs.RdotNG * normalGround - reflectedView;
vec3 horizonRingH = normalize(viewDirection + horizonRingDir);
inputs.NdotH_Horizon = dot(normal, horizonRingH);
vec3 mainLightRadianceComponent = lightingSpecularStrength * normalDistribution(inputs.NdotH, inputs.roughness) * lightingMainIntensity * (1.0 - shadow);
vec3 horizonLightRadianceComponent = lightingEnvironmentStrength * normalDistribution(inputs.NdotH_Horizon, min(inputs.roughness + horizonLightDiffusion, 1.0)) * lightingMainIntensity * fillLightIntensity;
vec3 ambientLightRadianceComponent = lightingEnvironmentStrength * calculateAmbientRadiance(ssao) + additionalLight;
inputs.skyRadianceToSurface = ambientLightRadianceComponent + mainLightRadianceComponent + horizonLightRadianceComponent;
inputs.groundRadianceToSurface = GROUND_REFLECTANCE * (ambientLightRadianceComponent + horizonLightRadianceComponent) + mainLightRadianceComponent;
inputs.averageAmbientRadiance = ambientLightIrradianceComponent[1] * (1.0 + GROUND_REFLECTANCE[1]);`),i.code.add(n.H`
        vec3 reflectedColorComponent = evaluateEnvironmentIllumination(inputs);
        vec3 additionalMaterialReflectanceComponent = inputs.albedoLinear * additionalAmbientIrradiance;
        vec3 emissionComponent = pow(_emission, vec3(GAMMA_SRGB));
        vec3 outColorLinear = reflectedColorComponent + additionalMaterialReflectanceComponent + emissionComponent;
        ${t.pbrMode===o.f7.Schematic?n.H`vec3 outColor = pow(max(vec3(0.0), outColorLinear - 0.005 * inputs.averageAmbientRadiance), vec3(INV_GAMMA_SRGB));`:n.H`vec3 outColor = pow(blackLevelSoftCompression(outColorLinear, inputs), vec3(INV_GAMMA_SRGB));`}
        return outColor;
      }
    `))}},73393:function(e,t,i){i.d(t,{l:function(){return o},p:function(){return n}});var r=i(23410);function o(e,t){e.fragment.uniforms.add("terrainDepthTexture","sampler2D"),e.fragment.uniforms.add("nearFar","vec2"),e.fragment.uniforms.add("inverseViewport","vec2"),e.fragment.code.add(r.H`
    // Compare the linearized depths of fragment and terrain. Discard fragments on the wrong side of the terrain.
    void terrainDepthTest(vec4 fragCoord, float fragmentDepth){

      float terrainDepth = linearDepthFromTexture(terrainDepthTexture, fragCoord.xy * inverseViewport, nearFar);
      if(fragmentDepth ${t.cullAboveGround?">":"<="} terrainDepth){
        discard;
      }
    }
  `)}function n(e,t){t.multipassTerrainEnabled&&t.terrainLinearDepthTexture&&e.bindTexture(t.terrainLinearDepthTexture,"terrainDepthTexture")}},2833:function(e,t,i){i.d(t,{k:function(){return a},q:function(){return r}});var r,o=i(27755),n=i(23410);function a(e,t){const i=e.fragment;switch(i.code.add(n.H`struct ShadingNormalParameters {
vec3 normalView;
vec3 viewDirection;
} shadingParams;`),t.doubleSidedMode){case r.None:i.code.add(n.H`vec3 shadingNormal(ShadingNormalParameters params) {
return normalize(params.normalView);
}`);break;case r.View:i.code.add(n.H`vec3 shadingNormal(ShadingNormalParameters params) {
return dot(params.normalView, params.viewDirection) > 0.0 ? normalize(-params.normalView) : normalize(params.normalView);
}`);break;case r.WindingOrder:i.code.add(n.H`vec3 shadingNormal(ShadingNormalParameters params) {
return gl_FrontFacing ? normalize(params.normalView) : normalize(-params.normalView);
}`);break;default:(0,o.Bg)(t.doubleSidedMode);case r.COUNT:}}!function(e){e[e.None=0]="None",e[e.View=1]="View",e[e.WindingOrder=2]="WindingOrder",e[e.COUNT=3]="COUNT"}(r||(r={}))},74839:function(e,t,i){i.d(t,{T:function(){return s}});var r=i(23410);function o(e){const t=e.fragment.code;t.add(r.H`vec3 evaluateDiffuseIlluminationHemisphere(vec3 ambientGround, vec3 ambientSky, float NdotNG)
{
return ((1.0 - NdotNG) * ambientGround + (1.0 + NdotNG) * ambientSky) * 0.5;
}`),t.add(r.H`float integratedRadiance(float cosTheta2, float roughness)
{
return (cosTheta2 - 1.0) / (cosTheta2 * (1.0 - roughness * roughness) - 1.0);
}`),t.add(r.H`vec3 evaluateSpecularIlluminationHemisphere(vec3 ambientGround, vec3 ambientSky, float RdotNG, float roughness)
{
float cosTheta2 = 1.0 - RdotNG * RdotNG;
float intRadTheta = integratedRadiance(cosTheta2, roughness);
float ground = RdotNG < 0.0 ? 1.0 - intRadTheta : 1.0 + intRadTheta;
float sky = 2.0 - ground;
return (ground * ambientGround + sky * ambientSky) * 0.5;
}`)}var n=i(3864),a=i(95509);function s(e,t){const i=e.fragment.code;e.include(a.e),t.pbrMode===n.f7.Water||t.pbrMode===n.f7.WaterOnIntegratedMesh?(i.add(r.H`
    struct PBRShadingWater
    {
        float NdotL;   // cos angle between normal and light direction
        float NdotV;   // cos angle between normal and view direction
        float NdotH;   // cos angle between normal and half vector
        float VdotH;   // cos angle between view direction and half vector
        float LdotH;   // cos angle between light direction and half vector
        float VdotN;   // cos angle between view direction and normal vector
    };

    float dtrExponent = ${t.useCustomDTRExponentForWater?"2.2":"2.0"};
    `),i.add(r.H`vec3 fresnelReflection(float angle, vec3 f0, float f90) {
return f0 + (f90 - f0) * pow(1.0 - angle, 5.0);
}`),i.add(r.H`float normalDistributionWater(float NdotH, float roughness)
{
float r2 = roughness * roughness;
float NdotH2 = NdotH * NdotH;
float denom = pow((NdotH2 * (r2 - 1.0) + 1.0), dtrExponent) * PI;
return r2 / denom;
}`),i.add(r.H`float geometricOcclusionKelemen(float LoH)
{
return 0.25 / (LoH * LoH);
}`),i.add(r.H`vec3 brdfSpecularWater(in PBRShadingWater props, float roughness, vec3 F0, float F0Max)
{
vec3  F = fresnelReflection(props.VdotH, F0, F0Max);
float dSun = normalDistributionWater(props.NdotH, roughness);
float V = geometricOcclusionKelemen(props.LdotH);
float diffusionSunHaze = mix(roughness + 0.045, roughness + 0.385, 1.0 - props.VdotH);
float strengthSunHaze  = 1.2;
float dSunHaze = normalDistributionWater(props.NdotH, diffusionSunHaze)*strengthSunHaze;
return ((dSun + dSunHaze) * V) * F;
}
vec3 tonemapACES(const vec3 x) {
return (x * (2.51 * x + 0.03)) / (x * (2.43 * x + 0.59) + 0.14);
}`)):t.pbrMode!==n.f7.Normal&&t.pbrMode!==n.f7.Schematic||(e.include(o),i.add(r.H`struct PBRShadingInfo
{
float NdotL;
float NdotV;
float NdotH;
float VdotH;
float LdotH;
float NdotNG;
float RdotNG;
float NdotAmbDir;
float NdotH_Horizon;
vec3 skyRadianceToSurface;
vec3 groundRadianceToSurface;
vec3 skyIrradianceToSurface;
vec3 groundIrradianceToSurface;
float averageAmbientRadiance;
float ssao;
vec3 albedoLinear;
vec3 f0;
vec3 f90;
vec3 diffuseColor;
float metalness;
float roughness;
};`),i.add(r.H`float normalDistribution(float NdotH, float roughness)
{
float a = NdotH * roughness;
float b = roughness / (1.0 - NdotH * NdotH + a * a);
return b * b * INV_PI;
}`),i.add(r.H`const vec4 c0 = vec4(-1.0, -0.0275, -0.572,  0.022);
const vec4 c1 = vec4( 1.0,  0.0425,  1.040, -0.040);
const vec2 c2 = vec2(-1.04, 1.04);
vec2 prefilteredDFGAnalytical(float roughness, float NdotV) {
vec4 r = roughness * c0 + c1;
float a004 = min(r.x * r.x, exp2(-9.28 * NdotV)) * r.x + r.y;
return c2 * a004 + r.zw;
}`),i.add(r.H`vec3 evaluateEnvironmentIllumination(PBRShadingInfo inputs) {
vec3 indirectDiffuse = evaluateDiffuseIlluminationHemisphere(inputs.groundIrradianceToSurface, inputs.skyIrradianceToSurface, inputs.NdotNG);
vec3 indirectSpecular = evaluateSpecularIlluminationHemisphere(inputs.groundRadianceToSurface, inputs.skyRadianceToSurface, inputs.RdotNG, inputs.roughness);
vec3 diffuseComponent = inputs.diffuseColor * indirectDiffuse * INV_PI;
vec2 dfg = prefilteredDFGAnalytical(inputs.roughness, inputs.NdotV);
vec3 specularColor = inputs.f0 * dfg.x + inputs.f90 * dfg.y;
vec3 specularComponent = specularColor * indirectSpecular;
return (diffuseComponent + specularComponent);
}`),i.add(r.H`float gamutMapChanel(float x, vec2 p){
return (x < p.x) ? mix(0.0, p.y, x/p.x) : mix(p.y, 1.0, (x - p.x) / (1.0 - p.x) );
}`),i.add(r.H`vec3 blackLevelSoftCompression(vec3 inColor, PBRShadingInfo inputs){
vec3 outColor;
vec2 p = vec2(0.02 * (inputs.averageAmbientRadiance), 0.0075 * (inputs.averageAmbientRadiance));
outColor.x = gamutMapChanel(inColor.x, p) ;
outColor.y = gamutMapChanel(inColor.y, p) ;
outColor.z = gamutMapChanel(inColor.z, p) ;
return outColor;
}`))}},3864:function(e,t,i){i.d(t,{f7:function(){return a},jV:function(){return s},nW:function(){return l}});var r=i(79912),o=i(512),n=i(23410);(0,r.f)(0,.6,.2);var a;function s(e,t){const i=e.fragment,r=t.hasMetalnessAndRoughnessTexture||t.hasEmissionTexture||t.hasOcclusionTexture;t.pbrMode===a.Normal&&r&&e.include(o.i,t),t.pbrMode!==a.Schematic?(t.pbrMode===a.Disabled&&i.code.add(n.H`float getBakedOcclusion() { return 1.0; }`),t.pbrMode===a.Normal&&(i.uniforms.add("emissionFactor","vec3"),i.uniforms.add("mrrFactors","vec3"),i.code.add(n.H`vec3 mrr;
vec3 emission;
float occlusion;`),t.hasMetalnessAndRoughnessTexture&&(i.uniforms.add("texMetallicRoughness","sampler2D"),t.supportsTextureAtlas&&i.uniforms.add("texMetallicRoughnessSize","vec2"),i.code.add(n.H`void applyMetallnessAndRoughness(TextureLookupParameter params) {
vec3 metallicRoughness = textureLookup(texMetallicRoughness, params).rgb;
mrr[0] *= metallicRoughness.b;
mrr[1] *= metallicRoughness.g;
}`)),t.hasEmissionTexture&&(i.uniforms.add("texEmission","sampler2D"),t.supportsTextureAtlas&&i.uniforms.add("texEmissionSize","vec2"),i.code.add(n.H`void applyEmission(TextureLookupParameter params) {
emission *= textureLookup(texEmission, params).rgb;
}`)),t.hasOcclusionTexture?(i.uniforms.add("texOcclusion","sampler2D"),t.supportsTextureAtlas&&i.uniforms.add("texOcclusionSize","vec2"),i.code.add(n.H`void applyOcclusion(TextureLookupParameter params) {
occlusion *= textureLookup(texOcclusion, params).r;
}
float getBakedOcclusion() {
return occlusion;
}`)):i.code.add(n.H`float getBakedOcclusion() { return 1.0; }`),i.code.add(n.H`
    void applyPBRFactors() {
      mrr = mrrFactors;
      emission = emissionFactor;
      occlusion = 1.0;
      ${r?"vtc.uv = vuv0;":""}
      ${t.hasMetalnessAndRoughnessTexture?t.supportsTextureAtlas?"vtc.size = texMetallicRoughnessSize; applyMetallnessAndRoughness(vtc);":"applyMetallnessAndRoughness(vtc);":""}
      ${t.hasEmissionTexture?t.supportsTextureAtlas?"vtc.size = texEmissionSize; applyEmission(vtc);":"applyEmission(vtc);":""}
      ${t.hasOcclusionTexture?t.supportsTextureAtlas?"vtc.size = texOcclusionSize; applyOcclusion(vtc);":"applyOcclusion(vtc);":""}
    }
  `))):i.code.add(n.H`const vec3 mrr = vec3(0.0, 0.6, 0.2);
const vec3 emission = vec3(0.0);
float occlusion = 1.0;
void applyPBRFactors() {}
float getBakedOcclusion() { return 1.0; }`)}function l(e,t,i=!1){i||(e.setUniform3fv("mrrFactors",t.mrrFactors),e.setUniform3fv("emissionFactor",t.emissiveFactor))}!function(e){e[e.Disabled=0]="Disabled",e[e.Normal=1]="Normal",e[e.Schematic=2]="Schematic",e[e.Water=3]="Water",e[e.WaterOnIntegratedMesh=4]="WaterOnIntegratedMesh",e[e.COUNT=5]="COUNT"}(a||(a={}))},95509:function(e,t,i){i.d(t,{e:function(){return o}});var r=i(23410);function o(e){e.vertex.code.add(r.H`const float PI = 3.141592653589793;`),e.fragment.code.add(r.H`const float PI = 3.141592653589793;
const float LIGHT_NORMALIZATION = 1.0 / PI;
const float INV_PI = 0.3183098861837907;
const float HALF_PI = 1.570796326794897;`)}},20105:function(e,t,i){i.d(t,{hX:function(){return n},vL:function(){return a}});var r=i(9794),o=i(23410);function n(e){e.fragment.include(r.n),e.fragment.uniforms.add("shadowMapTex","sampler2D"),e.fragment.uniforms.add("numCascades","int"),e.fragment.uniforms.add("cascadeDistances","vec4"),e.fragment.uniforms.add("shadowMapMatrix","mat4",4),e.fragment.uniforms.add("depthHalfPixelSz","float"),e.fragment.code.add(o.H`int chooseCascade(float _linearDepth, out mat4 mat) {
vec4 distance = cascadeDistances;
float depth = _linearDepth;
int i = depth < distance[1] ? 0 : depth < distance[2] ? 1 : depth < distance[3] ? 2 : 3;
mat = i == 0 ? shadowMapMatrix[0] : i == 1 ? shadowMapMatrix[1] : i == 2 ? shadowMapMatrix[2] : shadowMapMatrix[3];
return i;
}
vec3 lightSpacePosition(vec3 _vpos, mat4 mat) {
vec4 lv = mat * vec4(_vpos, 1.0);
lv.xy /= lv.w;
return 0.5 * lv.xyz + vec3(0.5);
}
vec2 cascadeCoordinates(int i, vec3 lvpos) {
return vec2(float(i - 2 * (i / 2)) * 0.5, float(i / 2) * 0.5) + 0.5 * lvpos.xy;
}
float readShadowMapDepth(vec2 uv, sampler2D _depthTex) {
return rgba2float(texture2D(_depthTex, uv));
}
float posIsInShadow(vec2 uv, vec3 lvpos, sampler2D _depthTex) {
return readShadowMapDepth(uv, _depthTex) < lvpos.z ? 1.0 : 0.0;
}
float filterShadow(vec2 uv, vec3 lvpos, float halfPixelSize, sampler2D _depthTex) {
float texSize = 0.5 / halfPixelSize;
vec2 st = fract((vec2(halfPixelSize) + uv) * texSize);
float s00 = posIsInShadow(uv + vec2(-halfPixelSize, -halfPixelSize), lvpos, _depthTex);
float s10 = posIsInShadow(uv + vec2(halfPixelSize, -halfPixelSize), lvpos, _depthTex);
float s11 = posIsInShadow(uv + vec2(halfPixelSize, halfPixelSize), lvpos, _depthTex);
float s01 = posIsInShadow(uv + vec2(-halfPixelSize, halfPixelSize), lvpos, _depthTex);
return mix(mix(s00, s10, st.x), mix(s01, s11, st.x), st.y);
}
float readShadowMap(const in vec3 _vpos, float _linearDepth) {
mat4 mat;
int i = chooseCascade(_linearDepth, mat);
if (i >= numCascades) { return 0.0; }
vec3 lvpos = lightSpacePosition(_vpos, mat);
if (lvpos.z >= 1.0) { return 0.0; }
if (lvpos.x < 0.0 || lvpos.x > 1.0 || lvpos.y < 0.0 || lvpos.y > 1.0) { return 0.0; }
vec2 uv = cascadeCoordinates(i, lvpos);
return filterShadow(uv, lvpos, depthHalfPixelSz, shadowMapTex);
}`)}function a(e,t,i){t.shadowMappingEnabled&&t.shadowMap.bindView(e,i)}},12664:function(e,t,i){i.d(t,{kl:function(){return n},uj:function(){return s}});var r=i(23410),o=i(21414);function n(e,t){t.vvInstancingEnabled&&(t.vvSize||t.vvColor)&&e.attributes.add(o.T.INSTANCEFEATUREATTRIBUTE,"vec4"),t.vvSize?(e.vertex.uniforms.add("vvSizeMinSize","vec3"),e.vertex.uniforms.add("vvSizeMaxSize","vec3"),e.vertex.uniforms.add("vvSizeOffset","vec3"),e.vertex.uniforms.add("vvSizeFactor","vec3"),e.vertex.uniforms.add("vvSymbolRotationMatrix","mat3"),e.vertex.uniforms.add("vvSymbolAnchor","vec3"),e.vertex.code.add(r.H`vec3 vvScale(vec4 _featureAttribute) {
return clamp(vvSizeOffset + _featureAttribute.x * vvSizeFactor, vvSizeMinSize, vvSizeMaxSize);
}
vec4 vvTransformPosition(vec3 position, vec4 _featureAttribute) {
return vec4(vvSymbolRotationMatrix * ( vvScale(_featureAttribute) * (position + vvSymbolAnchor)), 1.0);
}`),e.vertex.code.add(r.H`
      const float eps = 1.192092896e-07;
      vec4 vvTransformNormal(vec3 _normal, vec4 _featureAttribute) {
        vec3 vvScale = clamp(vvSizeOffset + _featureAttribute.x * vvSizeFactor, vvSizeMinSize + eps, vvSizeMaxSize);
        return vec4(vvSymbolRotationMatrix * _normal / vvScale, 1.0);
      }

      ${t.vvInstancingEnabled?r.H`
      vec4 vvLocalNormal(vec3 _normal) {
        return vvTransformNormal(_normal, instanceFeatureAttribute);
      }

      vec4 localPosition() {
        return vvTransformPosition(position, instanceFeatureAttribute);
      }`:""}
    `)):e.vertex.code.add(r.H`vec4 localPosition() { return vec4(position, 1.0); }
vec4 vvLocalNormal(vec3 _normal) { return vec4(_normal, 1.0); }`),t.vvColor?(e.vertex.constants.add("vvColorNumber","int",8),e.vertex.code.add(r.H`
      uniform float vvColorValues[vvColorNumber];
      uniform vec4 vvColorColors[vvColorNumber];

      vec4 vvGetColor(vec4 featureAttribute, float values[vvColorNumber], vec4 colors[vvColorNumber]) {
        float value = featureAttribute.y;
        if (value <= values[0]) {
          return colors[0];
        }

        for (int i = 1; i < vvColorNumber; ++i) {
          if (values[i] >= value) {
            float f = (value - values[i-1]) / (values[i] - values[i-1]);
            return mix(colors[i-1], colors[i], f);
          }
        }
        return colors[vvColorNumber - 1];
      }

      ${t.vvInstancingEnabled?r.H`
      vec4 vvColor() {
        return vvGetColor(instanceFeatureAttribute, vvColorValues, vvColorColors);
      }`:""}
    `)):e.vertex.code.add(r.H`vec4 vvColor() { return vec4(1.0); }`)}function a(e,t){t.vvSizeEnabled&&(e.setUniform3fv("vvSizeMinSize",t.vvSizeMinSize),e.setUniform3fv("vvSizeMaxSize",t.vvSizeMaxSize),e.setUniform3fv("vvSizeOffset",t.vvSizeOffset),e.setUniform3fv("vvSizeFactor",t.vvSizeFactor)),t.vvColorEnabled&&(e.setUniform1fv("vvColorValues",t.vvColorValues),e.setUniform4fv("vvColorColors",t.vvColorColors))}function s(e,t){a(e,t),t.vvSizeEnabled&&(e.setUniform3fv("vvSymbolAnchor",t.vvSymbolAnchor),e.setUniformMatrix3fv("vvSymbolRotationMatrix",t.vvSymbolRotationMatrix))}},41272:function(e,t,i){i.d(t,{F:function(){return n},bf:function(){return a},sj:function(){return s}});var r=i(23410),o=i(70984);const n=.1,a=.001;function s(e,t){const i=e.fragment;switch(t.alphaDiscardMode){case o.JJ.Blend:i.code.add(r.H`
        #define discardOrAdjustAlpha(color) { if (color.a < ${r.H.float(a)}) { discard; } }
      `);break;case o.JJ.Opaque:i.code.add(r.H`void discardOrAdjustAlpha(inout vec4 color) {
color.a = 1.0;
}`);break;case o.JJ.Mask:i.uniforms.add("textureAlphaCutoff","float"),i.code.add(r.H`#define discardOrAdjustAlpha(color) { if (color.a < textureAlphaCutoff) { discard; } else { color.a = 1.0; } }`);break;case o.JJ.MaskBlend:e.fragment.uniforms.add("textureAlphaCutoff","float"),e.fragment.code.add(r.H`#define discardOrAdjustAlpha(color) { if (color.a < textureAlphaCutoff) { discard; } }`)}}},5331:function(e,t,i){i.d(t,{$:function(){return n},I:function(){return a}});var r=i(39994),o=i(23410);function n({code:e},t){t.doublePrecisionRequiresObfuscation?e.add(o.H`vec3 dpPlusFrc(vec3 a, vec3 b) {
return mix(a, a + b, vec3(notEqual(b, vec3(0))));
}
vec3 dpMinusFrc(vec3 a, vec3 b) {
return mix(vec3(0), a - b, vec3(notEqual(a, b)));
}
vec3 dpAdd(vec3 hiA, vec3 loA, vec3 hiB, vec3 loB) {
vec3 t1 = dpPlusFrc(hiA, hiB);
vec3 e = dpMinusFrc(t1, hiA);
vec3 t2 = dpMinusFrc(hiB, e) + dpMinusFrc(hiA, dpMinusFrc(t1, e)) + loA + loB;
return t1 + t2;
}`):e.add(o.H`vec3 dpAdd(vec3 hiA, vec3 loA, vec3 hiB, vec3 loB) {
vec3 t1 = hiA + hiB;
vec3 e = t1 - hiA;
vec3 t2 = ((hiB - e) + (hiA - (t1 - e))) + loA + loB;
return t1 + t2;
}`)}function a(e){return!!(0,r.Z)("force-double-precision-obfuscation")||e.driverTest.doublePrecisionRequiresObfuscation}},78115:function(e,t,i){i.d(t,{a:function(){return a}});var r=i(25714),o=i(23410),n=i(6174);function a(e,t){const i=o.H`
  /*
  *  ${t.name}
  *  ${t.output===r.H.Color?"RenderOutput: Color":t.output===r.H.Depth?"RenderOutput: Depth":t.output===r.H.Shadow?"RenderOutput: Shadow":t.output===r.H.Normal?"RenderOutput: Normal":t.output===r.H.Highlight?"RenderOutput: Highlight":""}
  */
  `;(0,n.CG)()&&(e.fragment.code.add(i),e.vertex.code.add(i))}},42727:function(e,t,i){i.d(t,{y:function(){return a}});var r=i(66352),o=i(23410);function n(e){e.code.add(o.H`vec4 premultiplyAlpha(vec4 v) {
return vec4(v.rgb * v.a, v.a);
}
vec3 rgb2hsv(vec3 c) {
vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);
vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);
float d = q.x - min(q.w, q.y);
float e = 1.0e-10;
return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), min(d / (q.x + e), 1.0), q.x);
}
vec3 hsv2rgb(vec3 c) {
vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}
float rgb2v(vec3 c) {
return max(c.x, max(c.y, c.z));
}`)}function a(e){e.include(n),e.code.add(o.H`
    vec3 mixExternalColor(vec3 internalColor, vec3 textureColor, vec3 externalColor, int mode) {
      // workaround for artifacts in OSX using Intel Iris Pro
      // see: https://devtopia.esri.com/WebGIS/arcgis-js-api/issues/10475
      vec3 internalMixed = internalColor * textureColor;
      vec3 allMixed = internalMixed * externalColor;

      if (mode == ${o.H.int(r.a9.Multiply)}) {
        return allMixed;
      }
      else if (mode == ${o.H.int(r.a9.Ignore)}) {
        return internalMixed;
      }
      else if (mode == ${o.H.int(r.a9.Replace)}) {
        return externalColor;
      }
      else {
        // tint (or something invalid)
        float vIn = rgb2v(internalMixed);
        vec3 hsvTint = rgb2hsv(externalColor);
        vec3 hsvOut = vec3(hsvTint.x, hsvTint.y, vIn * hsvTint.z);
        return hsv2rgb(hsvOut);
      }
    }

    float mixExternalOpacity(float internalOpacity, float textureOpacity, float externalOpacity, int mode) {
      // workaround for artifacts in OSX using Intel Iris Pro
      // see: https://devtopia.esri.com/WebGIS/arcgis-js-api/issues/10475
      float internalMixed = internalOpacity * textureOpacity;
      float allMixed = internalMixed * externalOpacity;

      if (mode == ${o.H.int(r.a9.Ignore)}) {
        return internalMixed;
      }
      else if (mode == ${o.H.int(r.a9.Replace)}) {
        return externalOpacity;
      }
      else {
        // multiply or tint (or something invalid)
        return allMixed;
      }
    }
  `)}},9794:function(e,t,i){i.d(t,{n:function(){return o}});var r=i(23410);function o(e){e.code.add(r.H`const float MAX_RGBA_FLOAT =
255.0 / 256.0 +
255.0 / 256.0 / 256.0 +
255.0 / 256.0 / 256.0 / 256.0 +
255.0 / 256.0 / 256.0 / 256.0 / 256.0;
const vec4 FIXED_POINT_FACTORS = vec4(1.0, 256.0, 256.0 * 256.0, 256.0 * 256.0 * 256.0);
vec4 float2rgba(const float value) {
float valueInValidDomain = clamp(value, 0.0, MAX_RGBA_FLOAT);
vec4 fixedPointU8 = floor(fract(valueInValidDomain * FIXED_POINT_FACTORS) * 256.0);
const float toU8AsFloat = 1.0 / 255.0;
return fixedPointU8 * toU8AsFloat;
}
const vec4 RGBA_2_FLOAT_FACTORS = vec4(
255.0 / (256.0),
255.0 / (256.0 * 256.0),
255.0 / (256.0 * 256.0 * 256.0),
255.0 / (256.0 * 256.0 * 256.0 * 256.0)
);
float rgba2float(vec4 rgba) {
return dot(rgba, RGBA_2_FLOAT_FACTORS);
}`)}},3961:function(e,t,i){i.d(t,{kG:function(){return a}});var r=i(13802);const o=r.Z.getLogger("esri.views.3d.webgl-engine.core.shaderModules.shaderBuilder");class n{constructor(){this.includedModules=new Map}include(e,t){this.includedModules.has(e)?this.includedModules.get(e)!==t&&o.error("Trying to include shader module multiple times with different sets of options."):(this.includedModules.set(e,t),e(this.builder,t))}}class a extends n{constructor(){super(...arguments),this.vertex=new c,this.fragment=new c,this.attributes=new d,this.varyings=new u,this.extensions=new h,this.constants=new f}get fragmentUniforms(){return this.fragment.uniforms}get builder(){return this}generateSource(e){const t=this.extensions.generateSource(e),i=this.attributes.generateSource(e),r=this.varyings.generateSource(),o="vertex"===e?this.vertex:this.fragment,n=o.uniforms.generateSource(),a=o.code.generateSource(),s="vertex"===e?p:m,l=this.constants.generateSource().concat(o.constants.generateSource());return`\n${t.join("\n")}\n\n${s}\n\n${l.join("\n")}\n\n${n.join("\n")}\n\n${i.join("\n")}\n\n${r.join("\n")}\n\n${a.join("\n")}`}}class s{constructor(){this._entries=new Map}add(e,t,i){const r=`${e}_${t}_${i}`;return this._entries.set(r,{name:e,type:t,arraySize:i}),this}generateSource(){const e=e=>e?`[${e}]`:"";return Array.from(this._entries.values()).map((t=>`uniform ${t.type} ${t.name}${e(t.arraySize)};`))}get entries(){return Array.from(this._entries.values())}}class l{constructor(){this._entries=new Array}add(e){this._entries.push(e)}generateSource(){return this._entries}}class c extends n{constructor(){super(...arguments),this.uniforms=new s,this.code=new l,this.constants=new f}get builder(){return this}}class d{constructor(){this._entries=new Array}add(e,t){this._entries.push([e,t])}generateSource(e){return"fragment"===e?[]:this._entries.map((e=>`attribute ${e[1]} ${e[0]};`))}}class u{constructor(){this._entries=new Array}add(e,t){this._entries.push([e,t])}generateSource(){return this._entries.map((e=>`varying ${e[1]} ${e[0]};`))}}class h{constructor(){this._entries=new Set}add(e){this._entries.add(e)}generateSource(e){const t="vertex"===e?h.ALLOWLIST_VERTEX:h.ALLOWLIST_FRAGMENT;return Array.from(this._entries).filter((e=>t.includes(e))).map((e=>`#extension ${e} : enable`))}}h.ALLOWLIST_FRAGMENT=["GL_EXT_shader_texture_lod","GL_OES_standard_derivatives"],h.ALLOWLIST_VERTEX=[];class f{constructor(){this._entries=[]}add(e,t,i){let r="ERROR_CONSTRUCTOR_STRING";switch(t){case"float":r=f._numberToFloatStr(i);break;case"int":r=f._numberToIntStr(i);break;case"bool":r=i.toString();break;case"vec2":r=`vec2(${f._numberToFloatStr(i[0])},                            ${f._numberToFloatStr(i[1])})`;break;case"vec3":r=`vec3(${f._numberToFloatStr(i[0])},                            ${f._numberToFloatStr(i[1])},                            ${f._numberToFloatStr(i[2])})`;break;case"vec4":r=`vec4(${f._numberToFloatStr(i[0])},                            ${f._numberToFloatStr(i[1])},                            ${f._numberToFloatStr(i[2])},                            ${f._numberToFloatStr(i[3])})`;break;case"ivec2":r=`ivec2(${f._numberToIntStr(i[0])},                             ${f._numberToIntStr(i[1])})`;break;case"ivec3":r=`ivec3(${f._numberToIntStr(i[0])},                             ${f._numberToIntStr(i[1])},                             ${f._numberToIntStr(i[2])})`;break;case"ivec4":r=`ivec4(${f._numberToIntStr(i[0])},                             ${f._numberToIntStr(i[1])},                             ${f._numberToIntStr(i[2])},                             ${f._numberToIntStr(i[3])})`;break;case"mat2":case"mat3":case"mat4":r=`${t}(${Array.prototype.map.call(i,(e=>f._numberToFloatStr(e))).join(", ")})`}return this._entries.push(`const ${t} ${e} = ${r};`),this}static _numberToIntStr(e){return e.toFixed(0)}static _numberToFloatStr(e){return Number.isInteger(e)?e.toFixed(1):e.toString()}generateSource(){return Array.from(this._entries)}}const m="#ifdef GL_FRAGMENT_PRECISION_HIGH\n  precision highp float;\n  precision highp sampler2D;\n#else\n  precision mediump float;\n  precision mediump sampler2D;\n#endif",p="precision highp float;\nprecision highp sampler2D;"},23410:function(e,t,i){function r(e,...t){let i="";for(let r=0;r<t.length;r++)i+=e[r]+t[r];return i+=e[e.length-1],i}i.d(t,{H:function(){return r}}),function(e){function t(e){return Math.round(e).toString()}function i(e){return e.toPrecision(8)}e.int=t,e.float=i}(r||(r={}))},12045:function(e,t,i){i.d(t,{Bh:function(){return f},IB:function(){return l},j7:function(){return c},je:function(){return h},ve:function(){return d},wu:function(){return a}});var r=i(70984),o=i(91907),n=i(17346);const a=(0,n.wK)(o.zi.SRC_ALPHA,o.zi.ONE,o.zi.ONE_MINUS_SRC_ALPHA,o.zi.ONE_MINUS_SRC_ALPHA),s=(0,n["if"])(o.zi.ONE,o.zi.ONE),l=(0,n["if"])(o.zi.ZERO,o.zi.ONE_MINUS_SRC_ALPHA);function c(e){return e===r.Am.FrontFace?null:e===r.Am.Alpha?l:s}const d=5e5,u={factor:-1,units:-2};function h(e){return e?u:null}function f(e,t=o.wb.LESS){return e===r.Am.NONE||e===r.Am.FrontFace?t:o.wb.LEQUAL}},38208:function(e,t,i){var r;i.d(t,{C:function(){return r}}),function(e){e[e.MATERIAL=0]="MATERIAL",e[e.MATERIAL_ALPHA=1]="MATERIAL_ALPHA",e[e.MATERIAL_DEPTH=2]="MATERIAL_DEPTH",e[e.MATERIAL_NORMAL=3]="MATERIAL_NORMAL",e[e.MATERIAL_DEPTH_SHADOWMAP_ALL=4]="MATERIAL_DEPTH_SHADOWMAP_ALL",e[e.MATERIAL_HIGHLIGHT=5]="MATERIAL_HIGHLIGHT",e[e.MATERIAL_DEPTH_SHADOWMAP_DEFAULT=6]="MATERIAL_DEPTH_SHADOWMAP_DEFAULT",e[e.MATERIAL_DEPTH_SHADOWMAP_HIGHLIGHT=7]="MATERIAL_DEPTH_SHADOWMAP_HIGHLIGHT",e[e.MAX_PASS=8]="MAX_PASS"}(r||(r={}))},46378:function(e,t,i){var r;i.d(t,{r:function(){return r}}),function(e){e[e.INTEGRATED_MESH=0]="INTEGRATED_MESH",e[e.OPAQUE_TERRAIN=1]="OPAQUE_TERRAIN",e[e.OPAQUE_MATERIAL=2]="OPAQUE_MATERIAL",e[e.OPAQUE_PLUGIN=3]="OPAQUE_PLUGIN",e[e.TRANSPARENT_MATERIAL=4]="TRANSPARENT_MATERIAL",e[e.TRANSPARENT_PLUGIN=5]="TRANSPARENT_PLUGIN",e[e.TRANSPARENT_TERRAIN=6]="TRANSPARENT_TERRAIN",e[e.TRANSPARENT_DEPTH_WRITE_DISABLED_MATERIAL=7]="TRANSPARENT_DEPTH_WRITE_DISABLED_MATERIAL",e[e.OCCLUDED_TERRAIN=8]="OCCLUDED_TERRAIN",e[e.OCCLUDER_MATERIAL=9]="OCCLUDER_MATERIAL",e[e.TRANSPARENT_OCCLUDER_MATERIAL=10]="TRANSPARENT_OCCLUDER_MATERIAL",e[e.OCCLUSION_PIXELS=11]="OCCLUSION_PIXELS",e[e.POSTPROCESSING_ENVIRONMENT_OPAQUE=12]="POSTPROCESSING_ENVIRONMENT_OPAQUE",e[e.POSTPROCESSING_ENVIRONMENT_TRANSPARENT=13]="POSTPROCESSING_ENVIRONMENT_TRANSPARENT",e[e.LASERLINES=14]="LASERLINES",e[e.LASERLINES_CONTRAST_CONTROL=15]="LASERLINES_CONTRAST_CONTROL",e[e.HUD_MATERIAL=16]="HUD_MATERIAL",e[e.LABEL_MATERIAL=17]="LABEL_MATERIAL",e[e.LINE_CALLOUTS=18]="LINE_CALLOUTS",e[e.LINE_CALLOUTS_HUD_DEPTH=19]="LINE_CALLOUTS_HUD_DEPTH",e[e.DRAPED_MATERIAL=20]="DRAPED_MATERIAL",e[e.DRAPED_WATER=21]="DRAPED_WATER",e[e.VOXEL=22]="VOXEL",e[e.MAX_SLOTS=23]="MAX_SLOTS"}(r||(r={}))},21414:function(e,t,i){var r;i.d(t,{T:function(){return r}}),function(e){e.POSITION="position",e.NORMAL="normal",e.UV0="uv0",e.AUXPOS1="auxpos1",e.AUXPOS2="auxpos2",e.MAPPOS="mapPos",e.COLOR="color",e.SYMBOLCOLOR="symbolColor",e.SIZE="size",e.TANGENT="tangent",e.OFFSET="offset",e.SUBDIVISIONFACTOR="subdivisionFactor",e.COLORFEATUREATTRIBUTE="colorFeatureAttribute",e.SIZEFEATUREATTRIBUTE="sizeFeatureAttribute",e.OPACITYFEATUREATTRIBUTE="opacityFeatureAttribute",e.DISTANCETOSTART="distanceToStart",e.UVMAPSPACE="uvMapSpace",e.BOUNDINGRECT="boundingRect",e.UVREGION="uvRegion",e.NORMALCOMPRESSED="normalCompressed",e.PROFILERIGHT="profileRight",e.PROFILEUP="profileUp",e.PROFILEVERTEXANDNORMAL="profileVertexAndNormal",e.FEATUREVALUE="featureValue",e.MODELORIGINHI="modelOriginHi",e.MODELORIGINLO="modelOriginLo",e.MODEL="model",e.MODELNORMAL="modelNormal",e.INSTANCECOLOR="instanceColor",e.INSTANCEFEATUREATTRIBUTE="instanceFeatureAttribute",e.LOCALTRANSFORM="localTransform",e.GLOBALTRANSFORM="globalTransform",e.BOUNDINGSPHERE="boundingSphere",e.MODELORIGIN="modelOrigin",e.MODELSCALEFACTORS="modelScaleFactors",e.FEATUREATTRIBUTE="featureAttribute",e.STATE="state",e.LODLEVEL="lodLevel",e.POSITION0="position0",e.POSITION1="position1",e.NORMALA="normalA",e.NORMALB="normalB",e.COMPONENTINDEX="componentIndex",e.VARIANTOFFSET="variantOffset",e.VARIANTSTROKE="variantStroke",e.VARIANTEXTENSION="variantExtension",e.U8PADDING="u8padding",e.U16PADDING="u16padding",e.SIDENESS="sideness",e.START="start",e.END="end",e.UP="up",e.EXTRUDE="extrude"}(r||(r={}))},70984:function(e,t,i){var r,o,n,a,s,l,c,d,u,h,f,m,p,v;i.d(t,{Am:function(){return a},CE:function(){return f},Gv:function(){return o},JJ:function(){return p},MX:function(){return h},Rw:function(){return l},Vr:function(){return r},hU:function(){return c}}),function(e){e[e.None=0]="None",e[e.Front=1]="Front",e[e.Back=2]="Back",e[e.COUNT=3]="COUNT"}(r||(r={})),function(e){e[e.Less=0]="Less",e[e.Lequal=1]="Lequal",e[e.COUNT=2]="COUNT"}(o||(o={})),function(e){e[e.NONE=0]="NONE",e[e.SMAA=1]="SMAA"}(n||(n={})),function(e){e[e.Color=0]="Color",e[e.Alpha=1]="Alpha",e[e.FrontFace=2]="FrontFace",e[e.NONE=3]="NONE",e[e.COUNT=4]="COUNT"}(a||(a={})),function(e){e[e.BACKGROUND=0]="BACKGROUND",e[e.UPDATE=1]="UPDATE"}(s||(s={})),function(e){e[e.NOT_LOADED=0]="NOT_LOADED",e[e.LOADING=1]="LOADING",e[e.LOADED=2]="LOADED"}(l||(l={})),function(e){e[e.IntegratedMeshMaskExcluded=1]="IntegratedMeshMaskExcluded",e[e.OutlineVisualElementMask=2]="OutlineVisualElementMask"}(c||(c={})),function(e){e[e.ASYNC=0]="ASYNC",e[e.SYNC=1]="SYNC"}(d||(d={})),function(e){e[e.Highlight=0]="Highlight",e[e.MaskOccludee=1]="MaskOccludee",e[e.COUNT=2]="COUNT"}(u||(u={})),function(e){e[e.Triangle=0]="Triangle",e[e.Point=1]="Point",e[e.Line=2]="Line"}(h||(h={})),function(e){e[e.STRETCH=1]="STRETCH",e[e.PAD=2]="PAD"}(f||(f={})),function(e){e[e.CHANGED=0]="CHANGED",e[e.UNCHANGED=1]="UNCHANGED"}(m||(m={})),function(e){e[e.Blend=0]="Blend",e[e.Opaque=1]="Opaque",e[e.Mask=2]="Mask",e[e.MaskBlend=3]="MaskBlend",e[e.COUNT=4]="COUNT"}(p||(p={})),function(e){e[e.OFF=0]="OFF",e[e.ON=1]="ON"}(v||(v={}))},16318:function(e,t,i){i.d(t,{bj:function(){return N},FZ:function(){return F},Uf:function(){return L},Bw:function(){return _},LO:function(){return D},Hx:function(){return I}});var r=i(19431),o=i(61681),n=i(6766),a=i(8909),s=i(37116),l=i(70984);i(65684);function c(e){return Math.abs(e*e*e)}function d(e,t,i){const r=i.parameters,o=i.paddingPixelsOverride;return m.scale=Math.min(r.divisor/(t-r.offset),1),m.factor=c(e),m.minPixelSize=r.minPixelSize,m.paddingPixels=o,m}function u(e,t){return 0===e?t.minPixelSize:t.minPixelSize*(1+2*t.paddingPixels/e)}function h(e,t){return Math.max((0,r.t7)(e*t.scale,e,t.factor),u(e,t))}function f(e,t,i,r){return h(e,d(t,i,r))}(0,r.Vl)(10),(0,r.Vl)(12),(0,r.Vl)(70),(0,r.Vl)(40);const m={scale:0,factor:0,minPixelSize:0,paddingPixels:0};var p=i(15095),v=i(21414),g=(i(24455),i(39100));i(30560);function x(e){return!!(0,o.pC)(e)&&!e.visible}new Float64Array(3),new Float32Array(6),(0,g.c)();const T=(0,s.Ue)();function _(e,t,i,r,o,n,a){if(!x(t))if(e.boundingInfo){(0,p.hu)(e.primitiveType===l.MX.Triangle);const t=i.tolerance;A(e.boundingInfo,r,o,t,n,a)}else{const t=e.indices.get(v.T.POSITION),i=e.vertexAttributes.get(v.T.POSITION);C(r,o,0,t.length/3,t,i,void 0,n,a)}}const b=(0,a.c)();function A(e,t,i,r,n,a){if((0,o.Wi)(e))return;const l=w(t,i,b);if((0,s.op)(T,e.getBBMin()),(0,s.Tn)(T,e.getBBMax()),(0,o.pC)(n)&&n.applyToAabb(T),R(T,t,l,r)){const{primitiveIndices:o,indices:s,position:l}=e,c=o?o.length:s.length/3;if(c>z){const o=e.getChildren();if(void 0!==o){for(let e=0;e<8;++e)void 0!==o[e]&&A(o[e],t,i,r,n,a);return}}C(t,i,0,c,s,l,o,n,a)}}const S=(0,a.c)();function C(e,t,i,r,n,a,s,l,c){if(s)return O(e,t,i,r,n,a,s,l,c);const d=a.data,u=a.stride||a.size,h=e[0],f=e[1],m=e[2],p=t[0]-h,v=t[1]-f,g=t[2]-m;for(let x=i,T=3*i;x<r;++x){let e=u*n[T++],t=d[e++],i=d[e++],r=d[e];e=u*n[T++];let a=d[e++],s=d[e++],_=d[e];e=u*n[T++];let b=d[e++],A=d[e++],C=d[e];(0,o.pC)(l)&&([t,i,r]=l.applyToVertex(t,i,r,x),[a,s,_]=l.applyToVertex(a,s,_,x),[b,A,C]=l.applyToVertex(b,A,C,x));const O=a-t,M=s-i,E=_-r,w=b-t,R=A-i,P=C-r,I=v*P-R*g,N=g*w-P*p,L=p*R-w*v,D=O*I+M*N+E*L;if(Math.abs(D)<=Number.EPSILON)continue;const H=h-t,F=f-i,z=m-r,B=H*I+F*N+z*L;if(D>0){if(B<0||B>D)continue}else if(B>0||B<D)continue;const U=F*E-M*z,G=z*O-E*H,V=H*M-O*F,W=p*U+v*G+g*V;if(D>0){if(W<0||B+W>D)continue}else if(W>0||B+W<D)continue;const k=(w*U+R*G+P*V)/D;k>=0&&c(k,y(O,M,E,w,R,P,S),x,!1)}}function O(e,t,i,r,n,a,s,l,c){const d=a.data,u=a.stride||a.size,h=e[0],f=e[1],m=e[2],p=t[0]-h,v=t[1]-f,g=t[2]-m;for(let x=i;x<r;++x){const e=s[x];let t=3*e,i=u*n[t++],r=d[i++],a=d[i++],T=d[i];i=u*n[t++];let _=d[i++],b=d[i++],A=d[i];i=u*n[t];let C=d[i++],O=d[i++],M=d[i];(0,o.pC)(l)&&([r,a,T]=l.applyToVertex(r,a,T,x),[_,b,A]=l.applyToVertex(_,b,A,x),[C,O,M]=l.applyToVertex(C,O,M,x));const E=_-r,w=b-a,R=A-T,P=C-r,I=O-a,N=M-T,L=v*N-I*g,D=g*P-N*p,H=p*I-P*v,F=E*L+w*D+R*H;if(Math.abs(F)<=Number.EPSILON)continue;const z=h-r,B=f-a,U=m-T,G=z*L+B*D+U*H;if(F>0){if(G<0||G>F)continue}else if(G>0||G<F)continue;const V=B*R-w*U,W=U*E-R*z,k=z*w-E*B,q=p*V+v*W+g*k;if(F>0){if(q<0||G+q>F)continue}else if(q>0||G+q<F)continue;const $=(P*V+I*W+N*k)/F;$>=0&&c($,y(E,w,R,P,I,N,S),e,!1)}}const M=(0,a.c)(),E=(0,a.c)();function y(e,t,i,r,o,a,s){return(0,n.s)(M,e,t,i),(0,n.s)(E,r,o,a),(0,n.c)(s,M,E),(0,n.n)(s,s),s}function w(e,t,i){return(0,n.s)(i,1/(t[0]-e[0]),1/(t[1]-e[1]),1/(t[2]-e[2]))}function R(e,t,i,r){return P(e,t,i,r,1/0)}function P(e,t,i,r,o){const n=(e[0]-r-t[0])*i[0],a=(e[3]+r-t[0])*i[0];let s=Math.min(n,a),l=Math.max(n,a);const c=(e[1]-r-t[1])*i[1],d=(e[4]+r-t[1])*i[1];if(l=Math.min(l,Math.max(c,d)),l<0)return!1;if(s=Math.max(s,Math.min(c,d)),s>l)return!1;const u=(e[2]-r-t[2])*i[2],h=(e[5]+r-t[2])*i[2];return l=Math.min(l,Math.max(u,h)),!(l<0)&&(s=Math.max(s,Math.min(u,h)),!(s>l)&&s<o)}function I(e,t,i,o,n){let a=(i.screenLength||0)*e.pixelRatio;n&&(a=f(a,o,t,n));const s=a*Math.tan(.5*e.fovY)/(.5*e.fullHeight);return(0,r.uZ)(s*t,i.minWorldLength||0,null!=i.maxWorldLength?i.maxWorldLength:1/0)}function N(e,t,i){if(!e)return;const r=e.parameters,o=e.paddingPixelsOverride;t.setUniform4f(i,r.divisor,r.offset,r.minPixelSize,o)}function L(e,t){const i=t?L(t):{};for(const r in e){let t=e[r];t&&t.forEach&&(t=H(t)),null==t&&r in i||(i[r]=t)}return i}function D(e,t){let i=!1;for(const r in t){const o=t[r];void 0!==o&&(i=!0,Array.isArray(o)?e[r]=o.slice():e[r]=o)}return i}function H(e){const t=[];return e.forEach((e=>t.push(e))),t}const F={multiply:1,ignore:2,replace:3,tint:4},z=1e3},41163:function(e,t,i){i.d(t,{G:function(){return r}});class r{constructor(e,t,i,r,o,n=!1,a=0){this.name=e,this.count=t,this.type=i,this.offset=r,this.stride=o,this.normalized=n,this.divisor=a}}},30560:function(e,t,i){function r(e,t,i){for(let r=0;r<i;++r)t[2*r]=e[r],t[2*r+1]=e[r]-t[2*r]}function o(e,t,i,o){for(let s=0;s<o;++s)n[0]=e[s],r(n,a,1),t[s]=a[0],i[s]=a[1]}i.d(t,{LF:function(){return r},po:function(){return o}});const n=new Float64Array(1),a=new Float32Array(2)},17346:function(e,t,i){i.d(t,{BK:function(){return u},LZ:function(){return d},if:function(){return n},jp:function(){return G},sm:function(){return _},wK:function(){return a},zp:function(){return c}});var r=i(70984),o=i(91907);function n(e,t,i=o.db.ADD,r=[0,0,0,0]){return{srcRgb:e,srcAlpha:e,dstRgb:t,dstAlpha:t,opRgb:i,opAlpha:i,color:{r:r[0],g:r[1],b:r[2],a:r[3]}}}function a(e,t,i,r,n=o.db.ADD,a=o.db.ADD,s=[0,0,0,0]){return{srcRgb:e,srcAlpha:t,dstRgb:i,dstAlpha:r,opRgb:n,opAlpha:a,color:{r:s[0],g:s[1],b:s[2],a:s[3]}}}const s={face:o.LR.BACK,mode:o.Wf.CCW},l={face:o.LR.FRONT,mode:o.Wf.CCW},c=e=>e===r.Vr.Back?s:e===r.Vr.Front?l:null,d={zNear:0,zFar:1},u={r:!0,g:!0,b:!0,a:!0};function h(e){return S.intern(e)}function f(e){return O.intern(e)}function m(e){return E.intern(e)}function p(e){return w.intern(e)}function v(e){return P.intern(e)}function g(e){return N.intern(e)}function x(e){return D.intern(e)}function T(e){return F.intern(e)}function _(e){return B.intern(e)}class b{constructor(e,t){this.makeKey=e,this.makeRef=t,this.interns=new Map}intern(e){if(!e)return null;const t=this.makeKey(e),i=this.interns;return i.has(t)||i.set(t,this.makeRef(e)),i.get(t)}}function A(e){return"["+e.join(",")+"]"}const S=new b(C,(e=>({__tag:"Blending",...e})));function C(e){return e?A([e.srcRgb,e.srcAlpha,e.dstRgb,e.dstAlpha,e.opRgb,e.opAlpha,e.color.r,e.color.g,e.color.b,e.color.a]):null}const O=new b(M,(e=>({__tag:"Culling",...e})));function M(e){return e?A([e.face,e.mode]):null}const E=new b(y,(e=>({__tag:"PolygonOffset",...e})));function y(e){return e?A([e.factor,e.units]):null}const w=new b(R,(e=>({__tag:"DepthTest",...e})));function R(e){return e?A([e.func]):null}const P=new b(I,(e=>({__tag:"StencilTest",...e})));function I(e){return e?A([e.function.func,e.function.ref,e.function.mask,e.operation.fail,e.operation.zFail,e.operation.zPass]):null}const N=new b(L,(e=>({__tag:"DepthWrite",...e})));function L(e){return e?A([e.zNear,e.zFar]):null}const D=new b(H,(e=>({__tag:"ColorWrite",...e})));function H(e){return e?A([e.r,e.g,e.b,e.a]):null}const F=new b(z,(e=>({__tag:"StencilWrite",...e})));function z(e){return e?A([e.mask]):null}const B=new b(U,(e=>({blending:h(e.blending),culling:f(e.culling),polygonOffset:m(e.polygonOffset),depthTest:p(e.depthTest),stencilTest:v(e.stencilTest),depthWrite:g(e.depthWrite),colorWrite:x(e.colorWrite),stencilWrite:T(e.stencilWrite)})));function U(e){return e?A([C(e.blending),M(e.culling),y(e.polygonOffset),R(e.depthTest),I(e.stencilTest),L(e.depthWrite),H(e.colorWrite),z(e.stencilWrite)]):null}class G{constructor(e){this._pipelineInvalid=!0,this._blendingInvalid=!0,this._cullingInvalid=!0,this._polygonOffsetInvalid=!0,this._depthTestInvalid=!0,this._stencilTestInvalid=!0,this._depthWriteInvalid=!0,this._colorWriteInvalid=!0,this._stencilWriteInvalid=!0,this._stateSetters=e}setPipeline(e){(this._pipelineInvalid||e!==this._pipeline)&&(this._setBlending(e.blending),this._setCulling(e.culling),this._setPolygonOffset(e.polygonOffset),this._setDepthTest(e.depthTest),this._setStencilTest(e.stencilTest),this._setDepthWrite(e.depthWrite),this._setColorWrite(e.colorWrite),this._setStencilWrite(e.stencilWrite),this._pipeline=e),this._pipelineInvalid=!1}invalidateBlending(){this._blendingInvalid=!0,this._pipelineInvalid=!0}invalidateCulling(){this._cullingInvalid=!0,this._pipelineInvalid=!0}invalidatePolygonOffset(){this._polygonOffsetInvalid=!0,this._pipelineInvalid=!0}invalidateDepthTest(){this._depthTestInvalid=!0,this._pipelineInvalid=!0}invalidateStencilTest(){this._stencilTestInvalid=!0,this._pipelineInvalid=!0}invalidateDepthWrite(){this._depthWriteInvalid=!0,this._pipelineInvalid=!0}invalidateColorWrite(){this._colorWriteInvalid=!0,this._pipelineInvalid=!0}invalidateStencilWrite(){this._stencilTestInvalid=!0,this._pipelineInvalid=!0}_setBlending(e){this._blending=this._setSubState(e,this._blending,this._blendingInvalid,this._stateSetters.setBlending),this._blendingInvalid=!1}_setCulling(e){this._culling=this._setSubState(e,this._culling,this._cullingInvalid,this._stateSetters.setCulling),this._cullingInvalid=!1}_setPolygonOffset(e){this._polygonOffset=this._setSubState(e,this._polygonOffset,this._polygonOffsetInvalid,this._stateSetters.setPolygonOffset),this._polygonOffsetInvalid=!1}_setDepthTest(e){this._depthTest=this._setSubState(e,this._depthTest,this._depthTestInvalid,this._stateSetters.setDepthTest),this._depthTestInvalid=!1}_setStencilTest(e){this._stencilTest=this._setSubState(e,this._stencilTest,this._stencilTestInvalid,this._stateSetters.setStencilTest),this._stencilTestInvalid=!1}_setDepthWrite(e){this._depthWrite=this._setSubState(e,this._depthWrite,this._depthWriteInvalid,this._stateSetters.setDepthWrite),this._depthWriteInvalid=!1}_setColorWrite(e){this._colorWrite=this._setSubState(e,this._colorWrite,this._colorWriteInvalid,this._stateSetters.setColorWrite),this._colorWriteInvalid=!1}_setStencilWrite(e){this._stencilWrite=this._setSubState(e,this._stencilWrite,this._stencilWriteInvalid,this._stateSetters.setStencilWrite),this._stencilTestInvalid=!1}_setSubState(e,t,i,r){return(i||e!==t)&&(r(e),this._pipelineInvalid=!0),e}}}}]);
//# sourceMappingURL=160.04236f8d.js.map